package com.infinum.dbinspector.data;

public class Data {
    private Data() {
    }

    private static Data INSTANCE = new Data();

    public static Data getInstance() {
        return INSTANCE;
    }


    public static class Constants {
        public static class Limits {
            public static final int PAGE_SIZE = 100;
            public static final int INITIAL_PAGE = 1;
        }

        public static class Settings {
            public static final int LINES_LIMIT_MAXIMUM = 100;
        }
    }

    public static class Qualifiers {
        public static class Name {
            public static String DATASTORE_SETTINGS = "data.qualifiers.name.datastore.settings";
        }

        public static class Schema {
            public static String TABLES = "data.qualifiers.tables";
            public static String TABLE_BY_NAME = "data.qualifiers.table_by_name";
            public static String DROP_TABLE_CONTENT = "data.qualifiers.drop_table_content";

            public static String VIEWS = "data.qualifiers.views";
            public static String VIEW_BY_NAME = "data.qualifiers.view_by_name";
            public static String DROP_VIEW = "data.qualifiers.drop_view";

            public static String TRIGGERS = "data.qualifiers.triggers";
            public static String TRIGGER_BY_NAME = "data.qualifiers.trigger_by_name";
            public static String DROP_TRIGGER = "data.qualifiers.drop_trigger";

            public static String RAW_QUERY = "data.qualifiers.raw_query";
        }

        public static class Pragma {
            public static String TABLE_INFO = "data.qualifiers.table_info";
            public static String FOREIGN_KEYS = "data.qualifiers.foreign_keys";
            public static String INDEXES = "data.qualifiers.indexes";
        }
    }


}
