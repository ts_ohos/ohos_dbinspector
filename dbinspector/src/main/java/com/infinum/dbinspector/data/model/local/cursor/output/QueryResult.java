package com.infinum.dbinspector.data.model.local.cursor.output;

import java.util.List;

public class QueryResult {
    private List<Row> rows;
    private int nextPage = Integer.parseInt(null);
    private int beforeCount = 0;
    private int afterCount = 0;
}
