/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.data.source.raw;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.domain.database.models.Operation;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OhosDatabasesSource implements Sources.Raw {

    private static final String TAG = "OhosDatabasesSource";
    private List<File> database = new ArrayList<>();
    private static List<String> allowed = new ArrayList<>();

    private static int count = 1;

    @Override
    public List<File> getDatabases(Operation operation) {

        LogUtil.error(TAG, "调用来 查询数据库文件的个数");

        List<String> ignored = new ArrayList<>();
        try {
            allowed = Arrays.asList(operation.getContext()
                    .getResourceManager()
                    .getElement(ResourceTable.Strarray_dbinspector_allowed)
                    .getStringArray());
            ignored = Arrays.asList(operation.getContext()
                    .getResourceManager()
                    .getElement(ResourceTable.Strarray_dbinspector_ignored)
                    .getStringArray());

            File file = new File(operation.getContext().getDatabaseDir() + "/db");
            if (!file.exists()) {
                file.mkdir();
            }
            File[] files = file.listFiles();
            for (File f : files) {
                //遍历path 下文件
                if (FileInAllowed(f)) {
                    //判断是否有正在搜索,搜索数据为空就全添加
                    if (operation.getArgument().equals("") || operation.getArgument() == null) {
                        database.add(f);
                    } else if ((f.getName().substring(0, f.getName().indexOf("."))).contains(operation.getArgument())) {
                        LogUtil.error("name", f.getName().substring(0, f.getName().indexOf(".")));
                        database.add(f);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        LogUtil.error(TAG, "getDatabases:" + database.size() + ":" + String.valueOf(operation.getContext().getDatabaseDir()));
        return database;
    }

    //判断是否存在数据库文件，有的话就加入到database中
    public Boolean FileInAllowed(File f) {
        Boolean in = false;
        for (String a : allowed) {
            if (a.equals(f.getName().substring(f.getName().lastIndexOf(".") + 1))) {
                in = true;
            }
        }
        return (f.isFile() && f.canRead() && in);
    }


    @Override
    public List<File> importDatabases(Operation operation) {
        return null;
    }

    @Override
    public List<File> removeDatabase(Operation operation) {
        Boolean ok = deleteFile(operation.getDatabaseDescriptor().getAbsolutePath());
        LogUtil.error(TAG, operation.getContext().getDatabaseDir() + "");

        //添加数据
        File file = new File(operation.getContext().getDatabaseDir() + "/db");
        if (!file.exists()) {
            file.mkdir();
        }
        File[] files = file.listFiles();
        if (ok) {
            for (File f : files) {
                //遍历path 下文件
                LogUtil.error(TAG, f.getName());
                if (FileInAllowed(f)) {
                    database.add(f);
                }
            }
        } else {
            LogUtil.error(TAG, ok + "");
        }
        LogUtil.error(TAG, "Operation:" + database.size() + ":" + operation.getContext() + ":" + operation.getDatabaseDescriptor().getAbsolutePath());
        return database;
    }

    @Override
    public List<File> renameDatabase(Operation operation) {
        LogUtil.error(TAG, "operation getExtension = " + operation.getDatabaseDescriptor().getExtension());
        String filePath = operation.getDatabaseDescriptor().getAbsolutePath();
        LogUtil.error(TAG, "filePath =" + filePath);
        ///data/data/com.infinum.dbinspector/databases/db/blog.db
        String newName = operation.getArgument() + operation.getDatabaseDescriptor().getExtension(); //blog1.db
        LogUtil.error(TAG, "newName =" + newName);
           /*     operation.getDatabaseDescriptor().getParentPath()
                        + "/" + operation.getArgument()
                        + operation.getDatabaseDescriptor().getExtension();*/
        chageFileName(filePath, operation.getDatabaseDescriptor().getParentPath()
                + "/" + operation.getArgument()
                + operation.getDatabaseDescriptor().getExtension());


        File file = new File(operation.getContext().getDatabaseDir() + "/db");
        File[] files = file.listFiles();
        for (File f : files) {
            //遍历path 下文件
            if (FileInAllowed(f)) {
                database.add(f);
            }
        }
        LogUtil.error(TAG, "renameDatabase:" + database.size() + ":");
        return database;
    }

    public void chageFileName(String oldName, String reName) {
        /*File file = new File(filePath);
        //前面路径必须一样才能修改成功
        String path = filePath.substring(0, filePath.lastIndexOf("/") + 1) + reName;
        File newFile = new File(path);
        file.renameTo(newFile);*/
        if (!oldName.equals(reName)) {
            copyFile(oldName, reName);
            deleteFile(oldName);
        }
    }

    @Override
    public List<File> copyDatabase(Operation operation) {
        //旧的文件夹名
        String oldName = operation.getDatabaseDescriptor().getAbsolutePath();
        LogUtil.error(TAG, "1" + oldName);
        File targetFile = new File(oldName);

        //创建新的文件夹
        if (targetFile.exists()) {
            String newName = operation.getDatabaseDescriptor().getParentPath()
                    + "/"
                    + operation.getDatabaseDescriptor().getName()
                    + "_"
                    + count
                    + operation.getDatabaseDescriptor().getExtension();
            LogUtil.error(TAG, "2" + newName);
            targetFile = new File(newName);
            try {
                targetFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            count++;
            copyFile(oldName, newName);
        }

        //遍历文件获取到database集合
        File file = new File(operation.getContext().getDatabaseDir() + "/db");
        File[] files = file.listFiles();
        for (File f : files) {
            //遍历path 下文件
            if (FileInAllowed(f)) {
                database.add(f);
            }
        }

        LogUtil.error(TAG, "复制的 copyDatabase:" + operation.getDatabaseDescriptor().getAbsolutePath() + ":");
        return database;
    }


    /**
     * 删除单个文件
     *
     * @param fileName 要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean copyFile(String oldPathName, String newPathName) {
        try {
            File oldFile = new File(oldPathName);
            if (!oldFile.exists()) {
                LogUtil.error("--Method--", "copyFile:  oldFile not exist.");
                return false;
            } else if (!oldFile.isFile()) {
                LogUtil.error("--Method--", "copyFile:  oldFile not file.");
                return false;
            } else if (!oldFile.canRead()) {
                LogUtil.error("--Method--", "copyFile:  oldFile cannot read.");
                return false;
            }

            /* 如果不需要打log，可以使用下面的语句
            if (!oldFile.exists() || !oldFile.isFile() || !oldFile.canRead()) {
                return false;
            }
            */

            FileInputStream fileInputStream = new FileInputStream(oldPathName);
            FileOutputStream fileOutputStream = new FileOutputStream(newPathName);
            byte[] buffer = new byte[1024];
            int byteRead;
            while (-1 != (byteRead = fileInputStream.read(buffer))) {
                fileOutputStream.write(buffer, 0, byteRead);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}
