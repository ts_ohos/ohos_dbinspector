/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.data;

import com.infinum.dbinspector.Utils.LogUtil;
import ohos.app.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 资源文件操作
 */
public class RawfileDatabaseProvider implements DatabaseProvider {

    private static final String TAG = "AssetsDatabaseProvider";

    private Context context;
    private String databasesDir;
    private List<String> list;



    public RawfileDatabaseProvider(Context context) {
        this.context = context;
        databasesDir = context.getFilesDir().getPath();
        databasesDir = databasesDir.substring(0, databasesDir.lastIndexOf("/")) + "/databases/db";
        File file = new File(databasesDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        LogUtil.error(TAG, "databasesDir：" + databasesDir);
    }

    @Override
    public List<String> names(String[] s) {
        list = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            list.add(s[i]);
        }
      /*  list.add("blog.db");
        list.add("chinook.db");
        list.add("northwind.sqlite");*/
        return list;
    }

    @Override
    public void copy() {
        LogUtil.error(TAG,"copy方法 调用");
        InputStream input = null;
        FileOutputStream output = null;
        for (String s : list) {
            File file = new File(databasesDir, s);
            if (file.exists()) {
                LogUtil.error(TAG, " file 已存在");
            } else {
                try {
                    file.createNewFile();
                    input = context.getResourceManager().getRawFileEntry("resources/rawfile/databases/" + s).openRawFile();
                    output = new FileOutputStream(file);
                    byte[] buffer = new byte[1024];
                    int len = -1;
                    while ((len = input.read(buffer)) != -1) {
                        output.write(buffer, 0, len);
                    }
                    input.close();
                    output.flush();
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
