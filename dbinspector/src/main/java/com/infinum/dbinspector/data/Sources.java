package com.infinum.dbinspector.data;

import com.infinum.dbinspector.data.model.local.cursor.input.Query;
import com.infinum.dbinspector.data.model.local.cursor.output.QueryResult;
import com.infinum.dbinspector.domain.database.models.Operation;
import ohos.data.rdb.RdbStore;

import java.io.File;
import java.util.List;

public interface Sources {
    interface Raw {
        List<File> getDatabases(Operation operation);

        List<File> importDatabases(Operation operation);

        List<File> removeDatabase(Operation operation);

        List<File> renameDatabase(Operation operation);

        List<File> copyDatabase(Operation operation);
    }

    interface Memory {
        RdbStore openConnection(String path);

        void closeConnection(String path);
    }

    interface Local {
        interface Store {
        }

        interface Schema {
            QueryResult getTables(Query query);

            QueryResult getTableByName(Query query);

            QueryResult dropTableContentByName(Query query);

            QueryResult getViews(Query query);

            QueryResult getViewByName(Query query);

            QueryResult dropViewByName(Query query);

            QueryResult getTriggers(Query query);

            QueryResult getTriggerByName(Query query);

            QueryResult dropTriggerByName(Query query);
        }

        interface Pragma {
            QueryResult getUserVersion(Query query);

            QueryResult getTableInfo(Query query);

            QueryResult getForeignKeys(Query query);

            QueryResult getIndexes(Query query);
        }

        interface RawQuery {
            QueryResult rawQueryHeaders(Query query);

            QueryResult rawQuery(Query query);

            QueryResult affectedRows(Query query);
        }
    }
}
