package com.infinum.dbinspector.data.model.local.cursor.input;

import com.infinum.dbinspector.data.Data;
import ohos.data.rdb.RdbStore;

public class Query {
    private String databasePath = "";
    private RdbStore database = null;
    private String statement;
    private Order order = Order.ASCENDING;
    private int pageSize = Data.Constants.Limits.PAGE_SIZE;
    private int page = Data.Constants.Limits.INITIAL_PAGE;

    public Query(String statement) {
        this.statement = statement;
    }

    public Query(String databasePath, RdbStore database, String statement, Order order, int pageSize, int page) {
        this.databasePath = databasePath;
        this.database = database;
        this.statement = statement;
        this.order = order;
        this.pageSize = pageSize;
        this.page = page;
    }

    public String getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath = databasePath;
    }

    public RdbStore getDatabase() {
        return database;
    }

    public void setDatabase(RdbStore database) {
        this.database = database;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
