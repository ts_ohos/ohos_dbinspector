/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.data.source.memory.connection;

import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.ui.Presentation;
import ohos.data.DatabaseHelper;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

import java.util.HashMap;

public class OhosConnectionSource implements Sources.Memory {

    private HashMap<String, RdbStore> connectionPool = new HashMap<>();

    private DatabaseHelper helper = new DatabaseHelper(Presentation.getInstance().getApplication());

    @Override
    public RdbStore openConnection(String path) {
        String rdbName = path.substring(path.lastIndexOf("/") + 1);


        LogUtil.error("OhosConnectionSource", rdbName);
        StoreConfig config = StoreConfig.newDefaultConfig(rdbName);
        RdbStore rdbStore = helper.getRdbStore(config, 1, callback, null);
        return rdbStore;
    }

    private static final RdbOpenCallback callback = new RdbOpenCallback() {
        @Override
        public void onCreate(RdbStore store) {
            //store.executeSql("CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, age INTEGER, salary REAL, blobType BLOB)");
        }

        @Override
        public void onUpgrade(RdbStore store, int oldVersion, int newVersion) {
        }
    };

    @Override
    public void closeConnection(String path) {
      /*  String rdbName = path.substring(path.lastIndexOf("/") + 1);
        helper.deleteRdbStore(rdbName);*/
    }
}
