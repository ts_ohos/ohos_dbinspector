package com.infinum.dbinspector.data.model.local.cursor.input;

import com.infinum.dbinspector.ResourceTable;

public enum  Order {
    ASCENDING("ASC", ResourceTable.Media_ASC),
    DESCENDING("DESC", ResourceTable.Media_DESC);

    private String rawValue;
    private int icon;

    Order(String rawValue, int icon) {
        this.rawValue = rawValue;
        this.icon = icon;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

   /* private Void invoke(String value){

    }*/

}
