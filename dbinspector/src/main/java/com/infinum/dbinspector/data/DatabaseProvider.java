package com.infinum.dbinspector.data;

import java.util.List;

public interface DatabaseProvider {
    List<String> names(String[] strings);
    void copy();
}
