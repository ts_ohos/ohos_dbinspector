/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector;

import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;

public class DbInspector {
    private final String TAG = "DbInspector";

    private DbInspector() {
    }

    private static DbInspector INSTANCE = new DbInspector();

    public static DbInspector getInstance(){
        return INSTANCE;
    }

    public void show() {
        LogUtil.error(TAG,"show");
        Context context = Presentation.getInstance().getApplication();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.infinum.dbinspector")
                .withAbilityName("com.infinum.dbinspector.NewAbility")
                .build();
        intent.setOperation(operation);
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        context.startAbility(intent, 0);
    }
}
