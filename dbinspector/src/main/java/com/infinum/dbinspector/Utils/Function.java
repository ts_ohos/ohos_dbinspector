package com.infinum.dbinspector.Utils;

import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;

public interface Function<T>  {
    public void invoke(T  t);
}
