package com.infinum.dbinspector.ui.databases;

import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.List;


public class DatabasesAdapter extends BaseItemProvider {

    private static final String TAG = "DatabasesAdapter";

    private Function<DatabaseDescriptor> onClick;

    private DatabaseInteractions interactions;

    private List<DatabaseDescriptor> databases;

    private DatabaseViewHolder databaseViewHolder;


    public DatabasesAdapter(DatabaseInteractions interactions, Function<DatabaseDescriptor> onClick, List<DatabaseDescriptor> list) {
        this.interactions = interactions;
        this.onClick = onClick;
        this.databases = list;
        LogUtil.error(TAG, "list.size()：" + list.size());
    }


    @Override
    public int getCount() {
        return databases.size();
    }

    @Override
    public Object getItem(int i) {
        return databases.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        databaseViewHolder = new DatabaseViewHolder();
        component = databaseViewHolder.bind((DatabaseDescriptor) getItem(i), onClick, interactions);
        return component;
    }

    public void removeObject(DatabaseDescriptor databaseDescriptor) {
        databases.remove(databaseDescriptor);
        notifyDataChanged();
    }

    public void update(String oldName, String newName) {
        DatabaseDescriptor databaseDescriptor = null;
        for (int i = 0; i < databases.size(); i++) {
            if (databases.get(i).getName().equals(oldName)) {
                databases.get(i).setName(newName);
                databaseDescriptor = databases.get(i);
                databases.remove(i);
                databases.add(databaseDescriptor);
            }
        }
        notifyDataChanged();
    }

    public void add(List<DatabaseDescriptor> databases) {
        this.databases = databases;
        notifyDataChanged();
    }
}
