package com.infinum.dbinspector.ui.databases.edit;

import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.data.source.raw.OhosDatabasesSource;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.DatabaseRepository;
import com.infinum.dbinspector.domain.database.control.DatabaseControl;
import com.infinum.dbinspector.domain.database.control.converters.DatabaseConverter;
import com.infinum.dbinspector.domain.database.control.mappers.DatabaseDescriptorMapper;
import com.infinum.dbinspector.domain.database.interactors.RenameDatabaseInteractor;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.database.usecases.RenameDatabaseUseCase;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.shared.base.BaseViewModel;

import java.util.List;

public class EditDatabaseViewModel extends BaseViewModel {

    private UseCases.RenameDatabase renameDatabase;


    public void rename(DatabaseDescriptor database, String newName, Function<DatabaseDescriptor> action) {

        OhosDatabasesSource ohosDatabasesSource = new OhosDatabasesSource();

        DatabaseDescriptorMapper databaseDescriptorMapper = new DatabaseDescriptorMapper();

        DatabaseConverter databaseConverter = new DatabaseConverter();

        RenameDatabaseInteractor renameDatabaseInteractor = new RenameDatabaseInteractor(ohosDatabasesSource);

        DatabaseControl databaseControl = new DatabaseControl(databaseDescriptorMapper, databaseConverter);

        DatabaseRepository databaseRepository = new DatabaseRepository(renameDatabaseInteractor, databaseControl);

        RenameDatabaseUseCase renameDatabaseUseCase = new RenameDatabaseUseCase(databaseRepository);

        DatabaseParameters.Rename parametersRename = new DatabaseParameters.Rename(Presentation.getInstance().getApplication(), database, newName);

        renameDatabase = renameDatabaseUseCase;
        List<DatabaseDescriptor> list ;
        try {
            list = renameDatabase.invoke(parametersRename);
            if (list.size() != 0){
               action.invoke(list.get(0));
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
