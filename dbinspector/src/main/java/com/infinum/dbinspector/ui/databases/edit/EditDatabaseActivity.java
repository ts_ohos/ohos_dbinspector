package com.infinum.dbinspector.ui.databases.edit;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditDatabaseActivity extends BaseActivity {

    private static final String TAG = "EditDatabaseActivity";

    private DependentLayout edit_bar;

    private Image edit_return;

    private Text edit_bar_message;

    private TextField nameInput;

    private Text error_tip_text;

    private Image error_tip_image;


    //数据操控类
    private EditDatabaseViewModel editDatabaseViewModel;

    private String databasePath;
    private String databaseFilepath;
    private String databaseName;
    private String databaseExtension;

    private Context context = this;

    private Intent intent;

    private String newName;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        databasePath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_PATH);
        databaseFilepath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_FILEPATH);
        databaseName = intent.getStringParam(Presentation.Constants.Keys.DATABASE_NAME);
        databaseExtension = intent.getStringParam(Presentation.Constants.Keys.DATABASE_EXTENSION);
        LogUtil.error(TAG, databasePath + ":" + databaseFilepath + ":" + databaseName + ":" + databaseExtension + "");

    }

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_edit_database;
    }

    @Override
    public void initView() {
        edit_bar = (DependentLayout) findComponentById(ResourceTable.Id_editDatabase_bar);
        DependentLayout dependentLayout_bar = (DependentLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_edit_bar, null, false);
        edit_bar.addComponent(dependentLayout_bar);
        edit_return = (Image) dependentLayout_bar.findComponentById(ResourceTable.Id_edit_return);
        edit_bar_message = (Text) dependentLayout_bar.findComponentById(ResourceTable.Id_edit_bar_message);

        nameInput = (TextField) findComponentById(ResourceTable.Id_nameInput);
        error_tip_text = (Text) findComponentById(ResourceTable.Id_error_tip_text);
        error_tip_image = (Image) findComponentById(ResourceTable.Id_error_tip_image);
        setupUi();
    }


    private void setupUi() {
        edit_bar_message.setText(databaseName);
        nameInput.setText(databaseName);
        intent = new Intent();
        intent.setParam("oldName", databaseName);
        editDatabaseViewModel = new EditDatabaseViewModel();
    }

    /**
     * 正则替换所有特殊字符
     *
     * @param orgStr
     * @return
     */
    public static String replaceSpecStr(String orgStr) {
        String regEx = "[\\s~·`!！@#￥$%^……&*（()）\\-——\\-_=+【\\[\\]】｛{}｝\\|、\\\\；;：:‘'“”\"，,《<。.》>、/？?]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(orgStr);
        return m.replaceAll("").trim();
    }


    @Override
    public void clickLinstener() {
        edit_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!nameInput.getText().equals("")) {
                    intent.setParam("newName", replaceSpecStr(nameInput.getText()));
                    rename(replaceSpecStr(nameInput.getText()));
                    setResult(intent);
                }
                terminate();
            }
        });
        nameInput.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int start, int before, int count) {
                if (s.equals("")) {
                    error_tip_text.setVisibility(Component.VISIBLE);
                    error_tip_image.setVisibility(Component.VISIBLE);
                    // 显示TextField错误状态下的样式
                    ShapeElement errorElement = new ShapeElement(context, ResourceTable.Graphic_ele_error_textFields);
                    nameInput.setBackground(errorElement);
                } else {
                    error_tip_text.setVisibility(Component.HIDE);
                    error_tip_image.setVisibility(Component.HIDE);
                    ShapeElement normalElement = new ShapeElement(context, ResourceTable.Graphic_ele_name_textFields);
                    nameInput.setBackground(normalElement);
                }
            }
        });

    }

    private void rename(String newName) {
        getGlobalTaskDispatcher(TaskPriority.DEFAULT).syncDispatch(new Runnable() {
            @Override
            public void run() {
                editDatabaseViewModel.rename(
                        new DatabaseDescriptor(
                                true, databaseFilepath, databaseName, databaseExtension), newName, new Function<DatabaseDescriptor>() {
                            @Override
                            public void invoke(DatabaseDescriptor databaseDescriptor) {
                                databasePath = databaseDescriptor.getAbsolutePath();
                                databaseName = databaseDescriptor.getName();
                                setResult(intent.setParam(Presentation.Constants.Keys.SHOULD_REFRESH, true));
                            }
                        });
            }
        });
    }

    @Override
    protected void onBackPressed() {
        if (!nameInput.getText().equals("")) {
            intent.setParam("newName", replaceSpecStr(nameInput.getText()));
            rename(replaceSpecStr(nameInput.getText()));
            setResult(intent);
        }
        super.onBackPressed();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }
}
