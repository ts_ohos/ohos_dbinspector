/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.ui.edit;

import com.infinum.dbinspector.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class ToastDialogAdapter extends BaseItemProvider {

    private Context context;
    private List<String> sqlKeywords;
    private List<String> sqlFunctions;
    private List<String> sqlTypes;
    private List<String> tableName;
    private List<String> viewNmae;
    private List<String> triggerName;
    private List<String> tableInfo;

    public ToastDialogAdapter(Context context, List<String> sqlKeywords, List<String> sqlFunctions,
                              List<String> sqlTypes, List<String> tableName, List<String> viewNmae,
                              List<String> triggerName, List<String> tableInfo) {
        this.context = context;
        this.sqlKeywords = sqlKeywords;
        this.sqlFunctions = sqlFunctions;
        this.sqlTypes = sqlTypes;
        this.tableName = tableName;
        this.viewNmae = viewNmae;
        this.triggerName = triggerName;
        this.tableInfo = tableInfo;
    }

    @Override
    public int getCount() {
        return sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size() + triggerName.size() + tableInfo.size();
    }

    @Override
    public Object getItem(int i) {
        if (i < sqlKeywords.size()) {
            return sqlKeywords.get(i);
        } else if (i < (sqlKeywords.size() + sqlFunctions.size())) {
            return sqlFunctions.get(i - sqlKeywords.size());
        } else if (i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size())) {
            return sqlTypes.get(i - (sqlKeywords.size() + sqlFunctions.size()));
        } else if (i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size())) {
            return tableName.get(i - (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size()));
        } else if (i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size())) {
            return viewNmae.get(i - (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size()));
        } else if (i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size() + triggerName.size())) {
            return triggerName.get(i - (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size()));
        } else {
            return tableInfo.get(i - (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size() + triggerName.size()));
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_toastDialog_item_layout, null, false);
        Button button = (Button) component.findComponentById(ResourceTable.Id_toast_dialog_button);
        button.setClickable(false);
        button.setText((String) getItem(i));
        if (i < sqlKeywords.size()) {
            button.setTextColor(Color.WHITE);
        } else if (i < sqlKeywords.size() + sqlFunctions.size()) {
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#EB8258")));
        } else if (i < sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size()) {
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#CC2E28")));
        } else if (i < sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size()) {
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#7b71c2")));
        }else if (i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size())){
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#8E23A9")));
        }else if(i < (sqlKeywords.size() + sqlFunctions.size() + sqlTypes.size() + tableName.size() + viewNmae.size() + triggerName.size())){
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#BB3A8C")));
        }else {
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_functions);
            button.setBackground(shapeElement);
            button.setTextColor(new Color(Color.getIntColor("#abc96a")));
        }
        return component;
    }


}
