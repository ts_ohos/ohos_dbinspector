package com.infinum.dbinspector.ui.schema.shared;

public abstract class SchemaSourceViewModel {

    protected abstract String schemaStatement(String query);

    protected abstract void query(String databasePath, String query);
}
