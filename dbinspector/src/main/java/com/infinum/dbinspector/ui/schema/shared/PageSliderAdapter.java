package com.infinum.dbinspector.ui.schema.shared;

import com.infinum.dbinspector.Utils.LogUtil;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.ArrayList;
import java.util.List;

public class PageSliderAdapter extends PageSliderProvider {

    private final static String TAG = "PageSliderAdapter";
    private List<Component> pages;


    public PageSliderAdapter(ArrayList<Component> pages) {
        this.pages = pages;
        LogUtil.error(TAG, "pages:" + pages.size());
    }

    @Override
    public int getCount() {
        return pages == null ? 0 : pages.size();
    }


    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        componentContainer.addComponent(pages.get(i));
        LogUtil.error(TAG, "createPageInContainer" + i + "");
        return componentContainer;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent(pages.get(i));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

}
