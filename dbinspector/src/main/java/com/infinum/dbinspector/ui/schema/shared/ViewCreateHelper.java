package com.infinum.dbinspector.ui.schema.shared;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.content.table.TableActivity;
import com.infinum.dbinspector.ui.databases.edit.EditDatabaseActivity;
import com.infinum.dbinspector.ui.schema.tables.SchemaTableAdapter;
import com.infinum.dbinspector.ui.schema.triggers.SchemaTriggerAdapter;
import com.infinum.dbinspector.ui.schema.views.SchemaViewAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class ViewCreateHelper {

    //适配器
    private SchemaTableAdapter schemaTableAdapter;
    private SchemaViewAdapter schemaViewAdapter;
    private SchemaTriggerAdapter schemaTriggerAdapter;

    private AbilitySlice slice;
    private RdbStore rdbStore;
    private String databasePath;

    public ViewCreateHelper(AbilitySlice abilitySlice) {
        this.slice = abilitySlice;
        listContainers.clear();
        directionalLayouts.clear();
    }

    public Component createView(String databaseName,RdbStore rdbStore, String title, String databasePath) {
        this.rdbStore = rdbStore;
        this.databasePath = databasePath;
        Component mainComponent =
                LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_pager_item, null, false);
        if (!(mainComponent instanceof ComponentContainer)) {
            return null;
        }
        ComponentContainer rootLayout = (ComponentContainer) mainComponent;
        initView(databaseName,mainComponent, title);
        return rootLayout;
    }

    List<ListContainer> listContainers = new ArrayList<>();
    ListContainer listContainer;
    DirectionalLayout refresh_layout;
    List<DirectionalLayout> directionalLayouts = new ArrayList<>();

    public void initView(String databaseName,Component mainComponent, String title) {
        // ListView
        listContainer = (ListContainer) mainComponent.findComponentById(ResourceTable.Id_list_main);
        refresh_layout = (DirectionalLayout) mainComponent.findComponentById(ResourceTable.Id_refresh_layout);
        switch (title) {
            case "TABLES":
                Function<String> onClick = new Function<String>() {
                    @Override
                    public void invoke(String s) {
                        Intent intent = new Intent();
                        intent.setParam(Presentation.Constants.Keys.SCHEMA_NAME, "Table");
                        intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, databaseName);
                        intent.setParam(Presentation.Constants.Keys.TABLE_NAME,s);
                        intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, databasePath);
                        slice.present(new TableActivity(), intent);
                    }
                };
                schemaTableAdapter = new SchemaTableAdapter(rdbStore, onClick, slice);
                listContainer.setItemProvider(schemaTableAdapter);
                noFind(listContainer, refresh_layout);
                break;
            case "VIEWS":
                Function<String> onClick1 = new Function<String>() {
                    @Override
                    public void invoke(String s) {
                        Intent intent = new Intent();
                        intent.setParam(Presentation.Constants.Keys.SCHEMA_NAME, "VIEWS");
                        intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, databaseName);
                        intent.setParam(Presentation.Constants.Keys.TABLE_NAME,s);
                        intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, databasePath);
                        slice.present(new TableActivity(), intent);
                    }
                };
                schemaViewAdapter = new SchemaViewAdapter(rdbStore, onClick1);
                listContainer.setItemProvider(schemaViewAdapter);
                noFind(listContainer, refresh_layout);
                break;
            case "TRIGGERS":
                Function<String> onClick2 = new Function<String>() {
                    @Override
                    public void invoke(String s) {
                        Intent intent = new Intent();
                        intent.setParam(Presentation.Constants.Keys.SCHEMA_NAME, "Trigger");
                        intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, databaseName);
                        intent.setParam(Presentation.Constants.Keys.TABLE_NAME,s);
                        intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, databasePath);
                        slice.present(new TableActivity(), intent);
                    }
                };
                schemaTriggerAdapter = new SchemaTriggerAdapter(rdbStore, onClick2);
                listContainer.setItemProvider(schemaTriggerAdapter);
                noFind(listContainer, refresh_layout);
                break;
        }
        listContainers.add(listContainer);
        directionalLayouts.add(refresh_layout);
    }

    private static final String TAG = ViewCreateHelper.class.getSimpleName();

    //判断是否有数据
    public void noFind(ListContainer listContainer, DirectionalLayout directionalLayout) {
        DirectionalLayout  noFind = (DirectionalLayout) LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_dbinspector_layout_empty, null, false);
        //如果没有数据
        if (listContainer.getItemProvider().getCount() == 0) {
            LogUtil.error(TAG, " count = " + 0);
            listContainer.setVisibility(Component.HIDE);
            directionalLayout.addComponent(noFind);
        } else {
            //如果有数据了
            noFind.setVisibility(Component.HIDE);
            listContainer.setVisibility(Component.VISIBLE);
        }
    }

    public void noFind(int position) {
        ListContainer tempList = listContainers.get(position);
        DirectionalLayout tempDire = directionalLayouts.get(position);
        noFind(tempList, tempDire);
    }

    //搜索方法
    public void search(String query) {
        schemaTableAdapter.search(rdbStore, query);
        noFind(listContainer, refresh_layout);
    }


    public SchemaTableAdapter getSchemaTableAdapter() {
        return schemaTableAdapter;
    }

    public SchemaViewAdapter getSchemaViewAdapter() {
        return schemaViewAdapter;
    }

    public SchemaTriggerAdapter getSchemaTriggerAdapter() {
        return schemaTriggerAdapter;
    }
}