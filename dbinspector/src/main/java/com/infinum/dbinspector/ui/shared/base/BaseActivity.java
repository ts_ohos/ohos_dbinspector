package com.infinum.dbinspector.ui.shared.base;

import com.infinum.dbinspector.Utils.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public abstract class BaseActivity extends AbilitySlice {
    protected int layoutId;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setLayoutId();
        setUIContent(layoutId);
    }




    public abstract void setLayoutId();
    public abstract void initView();
    public abstract void clickLinstener();
    @Override
    protected void onActive() {
        super.onActive();
        initView();
        clickLinstener();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }
}
