package com.infinum.dbinspector.ui.content.table;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.content.shared.ContentActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.text.Layout;
import ohos.data.resultset.ResultSet;
import ohos.ivihardware.adas.InfoAssistManager;

import java.util.ArrayList;
import java.util.List;


public class TableActivity extends ContentActivity {

    private final static String TAG = "TableActivity";

    @Override
    protected void onStart(Intent intent) {
        drop = ResourceTable.String_dbinspector_clear_table_confirm;
        super.onStart(intent);
    }
    @Override
    protected void onActive() {
        super.onActive();
    }

/*
    @Override
    protected ResultSet getResultSet() {
        return  rdbStore.querySql("select * from " + databaseName, null);
    }

*/

    @Override
    protected void onBackground() {
        super.onBackground();
    }
}
