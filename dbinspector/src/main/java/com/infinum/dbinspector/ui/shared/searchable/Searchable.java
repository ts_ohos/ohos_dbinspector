package com.infinum.dbinspector.ui.shared.searchable;

public interface Searchable {
   void onSearchOpened();
   void search(String query);
   String searchQuery();
   void onSearchClosed();
}
