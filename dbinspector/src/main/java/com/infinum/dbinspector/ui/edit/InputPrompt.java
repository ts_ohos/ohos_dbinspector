/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.ui.edit;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.shared.models.Cell;
import com.infinum.dbinspector.ui.schema.shared.ViewCreateHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 输入提示框
 */
public class InputPrompt {
    public static InputPrompt INSTANCE = new InputPrompt();

    private InputPrompt() {
    }

    public static InputPrompt getINSTANCE() {
        return INSTANCE;
    }


    private static final String TAG = "InputPrompt";

    private ListContainer listContainer;
    private Context context;
    private TextField textField;

    public ListContainer getListContainer() {
        return listContainer;
    }

    public void setListContainer(ListContainer listContainer) {
        this.listContainer = listContainer;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public TextField getTextField() {
        return textField;
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    private String[] sqlKeywords;
    private String[] sqlFunctions;
    private String[] sqlTypes;
    private List<String> tableNames;
    private List<String> viewNames;
    private List<String> triggersNames;
    private List<String> tableInfos;

    private String result;
    private String value;
    private boolean isListContainerShow;


    private int length;
    private int row = 1;

    public void setRow(int row) {
        this.row = row;
    }

//获取用户table view trigger 页面的内容


    private RdbStore rdbStore;

    public void setRdbStore(RdbStore rdbStore) {
        this.rdbStore = rdbStore;
    }

    public void setResult(String result) {
        this.result = result;
    }

    //初始化操作
    public void initString() {

        tableNames = new ArrayList<>();
        viewNames = new ArrayList<>();
        triggersNames = new ArrayList<>();
        tableInfos = new ArrayList<>();

        //获取tableName
        getTableName();

        //获取View
        ResultSet resultSetView = rdbStore.querySql("select name from sqlite_master where type='view' order by name", null);
        while (resultSetView.goToNextRow()) {
            viewNames.add(resultSetView.getString(0));
        }

        //获取triggersName
        ResultSet resultSetTrigger = rdbStore.querySql("select name from sqlite_master where type='trigger' order by name", null);
        while (resultSetTrigger.goToNextRow()) {
            triggersNames.add(resultSetTrigger.getString(0));
        }
        //获取table Info
        for (int i = 0; i < tableNames.size(); i++) {
            ResultSet resultSetTableInfo = rdbStore.querySql("PRAGMA TABLE_INFO ('" + tableNames.get(i) + "')", null);
            if (resultSetTableInfo != null) {
                int ROWS = resultSetTableInfo.getRowCount();
                int COLS = resultSetTableInfo.getColumnCount();
                ROWS++;

                resultSetTableInfo.goToNextRow();
                for (int row = 0; row < ROWS; row++) {
                    for (int col = 0; col < COLS; col++) {
                        if (row == 0) {
                            //第一行字段数据
                        } else {
                            //当name 字段的数据
                            if (col == 1) {
                                tableInfos.add(resultSetTableInfo.getString(col));
                            }
                        }
                    }
                    if (row != 0) {
                        resultSetTableInfo.goToNextRow();
                    }
                }
            }
        }

        try {
            sqlKeywords = context.getResourceManager().getElement(ResourceTable.Strarray_dbinspector_sqlite_keywords).getStringArray();
            sqlFunctions = context.getResourceManager().getElement(ResourceTable.Strarray_dbinspector_sqlite_functions).getStringArray();
            sqlTypes = context.getResourceManager().getElement(ResourceTable.Strarray_dbinspector_sqlite_types).getStringArray();
        } catch (
                IOException e) {
            e.printStackTrace();
        } catch (
                NotExistException e) {
            e.printStackTrace();
        } catch (
                WrongTypeException e) {
            e.printStackTrace();
        }

    }

    //获取tableName;
    public void getTableName() {
        getSearchFilter();
        String filter = "";
        if (search_filter != null && search_filter.size() > 0) {
            Object[] strs = search_filter.toArray();
            filter += "and ";
            for (int i = 0; i < strs.length; i++) {
                if (i == strs.length - 1) {
                    filter += "name not like '%" + strs[i] + "%'";
                } else {
                    filter += "name not like '%" + strs[i] + "%' and ";
                }
            }
            LogUtil.error(TAG, "filter = " + filter);
        }
        ResultSet resultSet = rdbStore.querySql("select name from sqlite_master where type='table'" + filter + " order by name", null);
        while (resultSet.goToNextRow()) {
            tableNames.add(resultSet.getString(0));
        }
    }

    private Set<String> search_filter;

    //判断过滤
    public void getSearchFilter() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences preferences = databaseHelper.getPreferences("setting_filter");
        if (preferences != null) search_filter = preferences.getStringSet("filter", new HashSet<>());
    }

    //显示ListContainer
    public void show(String s) {
        LogUtil.error(TAG, "show 调用了");
        LogUtil.error(TAG, "table [0]" + tableNames.get(0));
        LogUtil.error(TAG, "tableInfo  [0]" + tableInfos.get(0));

        List<String> data1 = new ArrayList<>();
        List<String> data2 = new ArrayList<>();
        List<String> data3 = new ArrayList<>();
        List<String> data4 = new ArrayList<>();
        List<String> data5 = new ArrayList<>();
        List<String> data6 = new ArrayList<>();
        List<String> data7 = new ArrayList<>();

        for (String keyword : sqlKeywords) {
            if (keyword.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data1.add(keyword);
            }
        }
        for (String Function : sqlFunctions) {
            if (Function.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data2.add(Function);
            }
        }
        for (String type : sqlTypes) {
            if (type.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data3.add(type);
            }
        }
        for (String tableName : tableNames) {
            if (tableName.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data4.add(tableName);
            }
        }
        for (String view : viewNames) {
            if (view.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data5.add(view);
            }
        }
        for (String trigger : triggersNames) {
            if (trigger.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                data6.add(trigger);
            }
        }
        for (String tableInfo : tableInfos) {
            if (tableInfo.toUpperCase().indexOf(s.toUpperCase()) == 0) {
                if (!data7.contains(tableInfo)) {
                    data7.add(tableInfo);
                }
            }
        }


        LogUtil.error(TAG, data1.size() + ":" + data2.size() + ":" + data3.size());
        ToastDialogAdapter toastDialogAdapter = new ToastDialogAdapter(context, data1, data2, data3, data4, data5, data6, data7);
        listContainer.setVisibility(Component.VISIBLE);
        isListContainerShow = true;
        listContainer.setItemProvider(toastDialogAdapter);
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                value = listContainer.getItemProvider().getItem(i) + " ";
                listContainer.setVisibility(Component.HIDE);
                //textField.setText(result);
                change(s, value);
                LogUtil.error(TAG, "s =" + s);
                LogUtil.error(TAG, "Value =" + value);
                isListContainerShow = false;
            }
        });


    }

    //进行替换值
    private void change(String s, String value) {

        textField.setText(result.replaceAll("[^\\s]+$", value));
    }

    //listContainer位置控制
    public void changeListContainer(int i1) {
        length = i1;
        row = length / 28 + 1;


        LogUtil.error(TAG, "length / row * 30 = " + length / row * 20 + " 80* row =" + 80 * row);
        listContainer.setMarginLeft(length / row * 10);
        listContainer.setMarginTop(100 * row);
    }

    //销毁数据
    public void DestroyData() {
        sqlKeywords = null;
        sqlFunctions = null;
        sqlTypes = null;
        tableNames = null;
        viewNames = null;
        triggersNames = null;
        tableInfos = null;
    }

    public String getResult() {
        return result;
    }

    public boolean isListContainerShow() {
        return isListContainerShow;
    }
}
