package com.infinum.dbinspector.ui.schema.shared;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.shared.models.Cell;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.databases.DatabaseInteractions;
import ohos.agp.components.*;
import ohos.app.Context;

public class SchemaViewHolder {

    private DependentLayout content;
    private Text name;
    private DependentLayout item_layout;

    private Context context;

    private Cell item;
    private Function<String> onClick;

    public Component bind(Cell item, Function<String> onClick) {
        this.item = item;
        this.onClick = onClick;
        initView();
        ClickedListener();
        return content;
    }

    public void unbind() {
        item_layout.setClickedListener(null);
    }

    public SchemaViewHolder() {
        this.context = Presentation.context;
    }

    private void ClickedListener() {
        item_layout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onClick.invoke(item.getText());
            }
        });
    }

    private void initView() {
        content = (DependentLayout) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dbinspector_schema_table_item, null, false);
        item_layout = (DependentLayout) content.findComponentById(ResourceTable.Id_item_layout);
        name = (Text) content.findComponentById(ResourceTable.Id_item_name);
        name.setText(item.getText());
    }
}
