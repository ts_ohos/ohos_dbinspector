package com.infinum.dbinspector.ui.schema.pragma;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import ohos.event.notification.NotificationUserInput;

import java.util.ArrayList;
import java.util.List;


public class PragmaCreateHelper {

    private Context mContext;
    private RdbStore rdbStore;
    private String databasePath;
    private String tableName;

    private static final String TAG = PragmaCreateHelper.class.getSimpleName();


    public PragmaCreateHelper(Context slice) {
        this.mContext = slice;
    }


    public ResultSet createResultSet(RdbStore rdbStore, String title, String databasePath, String tableName) {
        this.rdbStore = rdbStore;
        this.databasePath = databasePath;
        this.tableName = tableName;
        ResultSet resultSet = null;
        LogUtil.error(TAG, "title = " + title);

        switch (title) {
            case "TABLE INFO":
                resultSet = rdbStore.querySql("PRAGMA TABLE_INFO ('" + tableName + "')" , null);
                LogUtil.error(TAG, "row1 = " + resultSet.getRowCount() + " col1 = " + resultSet.getColumnCount());
                //createTableLayout(resultSet);
                break;
            case "FOREIGN KEY":
                resultSet = rdbStore.querySql("PRAGMA FOREIGN_KEY_LIST('"+ tableName +"')", null);
                LogUtil.error(TAG, "row2 = " + resultSet.getRowCount() + " col2 = " + resultSet.getColumnCount());
                //createTableLayout(resultSet);
                break;
            case "INDEXES":
                resultSet = rdbStore.querySql("PRAGMA INDEX_LIST ('"+ tableName +"')", null);
                LogUtil.error(TAG, "row3 = " + resultSet.getRowCount() + " col3 = " + resultSet.getColumnCount());
                //createTableLayout(indexResultSet);
                break;
        }
        return resultSet;
    }


}
