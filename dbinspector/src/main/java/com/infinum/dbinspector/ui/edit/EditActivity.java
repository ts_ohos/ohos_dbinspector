/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.ui.edit;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.content.shared.ContentViewModel;
import com.infinum.dbinspector.ui.databases.DatabasesActivity;
import com.infinum.dbinspector.ui.databases.edit.EditDatabaseActivity;
import com.infinum.dbinspector.ui.databases.edit.EditDatabaseContract;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.*;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.*;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.miscservices.pasteboard.PasteData;
import ohos.miscservices.pasteboard.SystemPasteboard;
import ohos.multimodalinput.event.KeyEvent;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.PacMap;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditActivity extends BaseActivity {

    private static final String TAG = "EditActivity";

    private String databaseName;
    private String databasePath;

    //主界面放置 Bar
    private DirectionalLayout Bar;

    //具体的Bar页面
    private Component edit_Bar;

    //标题栏上的内容
    private Text search_message;
    private Image clear;
    private Image run;
    private Image search_return;

    //数据库操作
    private ContentViewModel contentViewModel;
    private RdbStore rdbStore;


    private int stop = 201;

    //页面输入框
    private TextField textField;

    private ShapeElement shapeElement;

    //tableLayout
    private TableLayout tableLayout;

    private ResultSet resultSet;

    private String warnMessage = "";

    private DependentLayout warningLayout;
    private Text warnText;

    private StackLayout stackLayout;

    private int oldX;
    private int oldY;

    private int moveX;
    private int moveY;

    //向上滑动
    private boolean isHalfUp = false;
    //向下滑动
    private boolean isHalfDown = false;

    private ScrollView search_scrollView;

    private SystemPasteboard pasteboard;


    private ListContainer listContainer;

    private String StartResult;

    private byte[] bytes;
    private Preferences preferences;
    private int click_pos;
    private String blob;
    String count = "";
    private AbilitySlice slice;

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_edit;
    }

    @Override
    protected void onStart(Intent intent) {
        databaseName = intent.getStringParam(Presentation.Constants.Keys.DATABASE_NAME);
        databasePath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_PATH);
        LogUtil.error(TAG, "databaseName = " + databaseName + "databasePath =" + databasePath);
        super.onStart(intent);
        contentViewModel = new ContentViewModel();
        contentViewModel.setDatabasePath(databasePath);
        contentViewModel.open();
        rdbStore = contentViewModel.getRdbStore();

        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        preferences = databaseHelper.getPreferences("setting_filter");
        click_pos = preferences.getInt("radio_click", -1);
        slice = this;
    }

    @Override
    public void initView() {
        Bar = (DirectionalLayout) findComponentById(ResourceTable.Id_serach_edit_bar);
        edit_Bar = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_search_edit_bar, null, false);
        Bar.addComponent(edit_Bar);
        search_message = (Text) edit_Bar.findComponentById(ResourceTable.Id_search_message);
        search_message.setText(databaseName);
        search_return = (Image) edit_Bar.findComponentById(ResourceTable.Id_search_edit_return);
        clear = (Image) edit_Bar.findComponentById(ResourceTable.Id_clear);
        run = (Image) edit_Bar.findComponentById(ResourceTable.Id_run);

        textField = (TextField) findComponentById(ResourceTable.Id_edit_textField);
        tableLayout = (TableLayout) findComponentById(ResourceTable.Id_search_table);


        shapeElement = new ShapeElement(this, ResourceTable.Graphic_background_grey);

        warningLayout = (DependentLayout) findComponentById(ResourceTable.Id_waring_layout);
        warnText = (Text) findComponentById(ResourceTable.Id_waring_text);

        search_scrollView = (ScrollView) findComponentById(ResourceTable.Id_search_scrollView);
        stackLayout = (StackLayout) findComponentById(ResourceTable.Id_search_stackLayout);

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_InputPrompt_ListContainer);

        if (preferences.getBoolean("HOME", false)) {
            canClick();
        } else {
            notCanClick();
        }

        preferences.putBoolean("HOME", true);

        InputPrompt.getINSTANCE().setContext(getContext());
        InputPrompt.getINSTANCE().setListContainer(listContainer);
        InputPrompt.getINSTANCE().setTextField(textField);
        InputPrompt.getINSTANCE().setRdbStore(rdbStore);
        InputPrompt.getINSTANCE().initString();

    }

    @Override
    public void clickLinstener() {
        //返回按钮
        search_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                textField.setText("");
                textField.setHeight(990);
                warningLayout.setVisibility(Component.HIDE);
                search_scrollView.setVisibility(Component.HIDE);
                tableLayout.removeAllComponents();
                InputPrompt.getINSTANCE().setResult("");
                InputPrompt.getINSTANCE().DestroyData();
                preferences.putBoolean("HOME", false);
                terminate();
            }
        });

        clear.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                textField.setText("");
                textField.clearFocus();
                tableLayout.removeAllComponents();
                warningLayout.setVisibility(Component.HIDE);
                search_scrollView.setVisibility(Component.HIDE);
            }
        });

        run.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //如果输入不为空
                tableLayout.removeAllComponents();

                getTableInfo(textField.getText());
            }
        });
        textField.setEditorActionListener(new Text.EditorActionListener() {
            @Override
            public boolean onTextEditorAction(int i) {
                return false;
            }
        });
        textField.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                LogUtil.error(TAG, "onTextUpdated = " + s + "i = " + i + "i1= " + i1 + "i2= " + i2);

                //每次更新的时候都把值 传到处理类
                InputPrompt.getINSTANCE().setResult(s);

                //输入值是空的不做处理，不显示出ListContainer
                if (s.isEmpty()) {
                    notCanClick();
                    InputPrompt.getINSTANCE().getListContainer().setVisibility(Component.HIDE);
                    InputPrompt.getINSTANCE().setRow(1);
                } else {
                    //如果字符串包含有空格，表示已经添加过了
                    if (s.contains(" ")) {
                        canClick();
                        StartResult = s.substring(s.lastIndexOf(" ") + 1);
                        LogUtil.error(TAG, "截取的值 =" + StartResult);
                        if (StartResult.isEmpty()) {
                            InputPrompt.getINSTANCE().getListContainer().setVisibility(Component.HIDE);
                            //InputPrompt.getINSTANCE().setRow(1);
                        } else {
                            InputPrompt.getINSTANCE().show(StartResult);
                        }
                    } else {
                        canClick();
                        InputPrompt.getINSTANCE().show(s);
                    }
                }

                LogUtil.error(TAG, "输入字符的长度 = " + s.length());
                //控制弹窗位置
                InputPrompt.getINSTANCE().changeListContainer(i1);
            }
        });


        textField.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (InputPrompt.getINSTANCE().isListContainerShow()) {
                    InputPrompt.getINSTANCE().getListContainer().setVisibility(Component.HIDE);
                }
            }
        });

        stackLayout.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        LogUtil.error(TAG, "按下");
                        //按下
                        oldX = (int) touchEvent.getPointerScreenPosition(touchEvent.getPointerId(0)).getX();
                        oldY = (int) touchEvent.getPointerScreenPosition(touchEvent.getPointerId(0)).getY();
                        break;
                    case TouchEvent.POINT_MOVE:
                        LogUtil.error(TAG, "移动");
                        moveX = (int) touchEvent.getPointerScreenPosition(touchEvent.getPointerId(0)).getX();
                        moveY = (int) touchEvent.getPointerScreenPosition(touchEvent.getPointerId(0)).getY();
                        LogUtil.error(TAG, "moveY = " + moveY);
                        // 计算距离，以及方向，
                        int distanceX = moveX - oldX;
                        int distanceY = moveY - oldY;
                        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig();
                        if (distanceY > 0) {
                            isHalfDown = true;
                            isHalfUp = false;
                        } else {
                            isHalfUp = true;
                            isHalfDown = false;
                        }
                        //向下滑动时
                        if (isHalfDown) {
                            textField.setHeight(990 + distanceY);
                            /*layoutConfig.setMarginTop(distanceY);
                            component.setLayoutConfig(layoutConfig);*/
                        }
                        //向上滑动时
                        if (isHalfUp) {
                            textField.setHeight(990 + distanceY);
                            /*layoutConfig.setMarginTop(990+distanceY);
                            component.setLayoutConfig(layoutConfig);*/
                        }
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        LogUtil.error(TAG,"抬起");
                        break;
                }
                return true;
            }
        });
    }


    //输入提示弹框
    private void canClick() {
        clear.setAlpha(1f);
        run.setAlpha(1f);
        clear.setClickable(true);
        run.setClickable(true);
    }

    private void notCanClick() {
        clear.setAlpha(0.5f);
        run.setAlpha(0.5f);
        clear.setClickable(false);
        run.setClickable(false);
    }


    public void getTableInfo(String str) {

        String s1 = str.replaceAll(";", " ");
        String s = s1.replaceAll("=", " ");

        if (rdbStore.isOpen()) {
            rdbStore.querySql("PRAGMA foreign_keys = 0", null);
            if (!s.contains("CREATE") && s.contains("SELECT") || s.contains("select")) {
                if (!s.contains("LIMIT") || s.contains("limit")) {
                    try {
                        resultSet = rdbStore.querySql(s + " LIMIT " + stop, null);
                        LogUtil.error(TAG, "result length = " + resultSet.getAllColumnNames().length);
                        warnMessage = "";
                    } catch (Throwable e) {
                        LogUtil.error(TAG, "e.getMessage = " + e.getMessage());
                        warnMessage = e.getMessage();
                        //sql 语句错误了
                        if (warnMessage.contains("LIMIT")) {
                            error(s);
                        } else {
                            error(warnMessage);
                        }
                    }
                    if (warnMessage == "") {
                        showTable(s);
                    }
                } else {
                    try {
                        resultSet = rdbStore.querySql(s, null);
                        LogUtil.error(TAG, "result length = " + resultSet.getAllColumnNames().length);
                        warnMessage = "";
                    } catch (Throwable e) {
                        LogUtil.error(TAG, "e.getMessage = " + e.getMessage());
                        warnMessage = e.getMessage();
                        error(warnMessage);
                    }
                    if (warnMessage == "") {
                        showTable(s);
                    }
                }
            } else {
                try {
                    rdbStore.executeSql(s);
                    search_scrollView.setVisibility(Component.HIDE);
                    warningLayout.setVisibility(Component.VISIBLE);
                    warnText.setText("sql 语句执行成功");
                } catch (RdbException e) {
                    warnMessage = e.getMessage();
                    error(warnMessage);
                } catch (Throwable e) {
                    warnMessage = e.getMessage();
                    error(warnMessage);
                }
            }
        }

    }

    /**
     * 提示语句存在错误
     *
     * @param s
     */
    private void error(String s) {
        //sql 语句错误了
        tableLayout.removeAllComponents();
        warningLayout.setVisibility(Component.VISIBLE);
        warnText.setText("该语句存在错误:" + s);
        search_scrollView.setVisibility(Component.HIDE);
    }


    //显示数据表
    private void showTable(String s) {
        warningLayout.setVisibility(Component.HIDE);
        search_scrollView.setVisibility(Component.VISIBLE);
        String[] columnNames = resultSet.getAllColumnNames();
        LogUtil.error(TAG, "columNames len = " + columnNames.length);
        int ROWS = resultSet.getRowCount();
        int COLS = resultSet.getColumnCount();
        ROWS++;

        tableLayout.setRowCount(ROWS);
        tableLayout.setColumnCount(COLS);

        List<ResultSet.ColumnType> columnTypes = new ArrayList<>();

        TableLayout.LayoutConfig params = new TableLayout.LayoutConfig();
        params.width = 400;
        params.height = 300;

        //但ROWS == 1是表示数据库数据已经被删除 否则正常查询
        if (ROWS != 1) {
            resultSet.goToNextRow();
            for (int i = 0; i < columnNames.length; i++) {
                columnTypes.add(resultSet.getColumnTypeForIndex(i));
            }
            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    Component item = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_table_info_item, null, false);
                    Image showImage = (Image) item.findComponentById(ResourceTable.Id_image_id);
                    Text text = (Text) item.findComponentById(ResourceTable.Id_item_info);
                    showImage.setVisibility(Component.HIDE);
                    text.setVisibility(Component.VISIBLE);
                    if (row == 0) {
                        item.setBackground(shapeElement);
                        text.setText(columnNames[col]);
                        tableLayout.addComponent(item, params);
                    } else {
                        showBlob(columnTypes, col, text, showImage, tableLayout, item, s, params);
                    }
                    item.setClickable(true);
                    item.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            if (showImage.getTag() != null) {
                                Object object = showImage.getTag();
                                byte[] bytes = (byte[]) object;
                                InputStream inputStream = new ByteArrayInputStream(bytes);
                                ImageSource.SourceOptions scrOpts = new ImageSource.SourceOptions();
                                scrOpts.formatHint = "image/jpg";
                                ImageSource imageSource = ImageSource.create(inputStream, scrOpts);
                                // 设置图片参数
                                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                                PixelMapElement pixelMapElement = new PixelMapElement(pixelMap);
                                show(getContext(), pixelMapElement, bytes);
                            } else show(getContext(), text.getText());
                        }
                    });
                }
                if (row != 0) {
                    resultSet.goToNextRow();
                }
            }
        } else {
            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    Component item1 = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_table_info_item, null, false);
                    Text text1 = (Text) item1.findComponentById(ResourceTable.Id_item_info);
                    text1.setText(columnNames[col]);
                    item1.setBackground(shapeElement);
                    tableLayout.addComponent(item1, params);
                    item1.setClickable(true);
                    item1.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            show(getContext(), text1.getText());
                        }
                    });
                }
            }
        }
    }

    private void showBlob(List<ResultSet.ColumnType> columnTypes, int col, Text text, Image showImage, TableLayout tableLayout, Component item, String s, TableLayout.LayoutConfig params) {

        if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_BLOB.getValue()) {
            if (resultSet.getBlob(col) == null) {
                text.setText("null");
            } else {
                bytes = resultSet.getBlob(col);
                showImage.setTag(bytes);
                text.setText("数据加载中..");
                if (click_pos == 0 || click_pos == -1) {
                    text.setText("[DATA]");
                } else if (click_pos == 1) {
                    try {
                        text.setText(new String(bytes, "utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                } else {
                    //LogUtil.error(TAG, click_pos + "" + "BYTE = " + getFormat(click_pos));
                    getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < 7; i++) {
                                byte[] newBytes = new byte[bytes.length / 7];
                                System.arraycopy(bytes, i * bytes.length / 7, newBytes, 0, bytes.length / 7);
                                if (click_pos == 3) {
                                    final Base64.Decoder decoder = Base64.getDecoder();
                                    final Base64.Encoder encoder = Base64.getEncoder();
                                    final String encodedText = encoder.encodeToString(newBytes);
                                    blob = encodedText;
                                } else if (click_pos == 2) {
                                    blob = bytesToHexString(newBytes);
                                }
                                count += blob;
                                if (i > 0) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, 1000);

                    getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            text.setText(blob);
                        }
                    }, 5000);
                }
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_FLOAT.getValue()) {
            if (new Float(resultSet.getFloat(col)).toString().isEmpty()) {
                text.setText("null");
            } else {
                text.setText(new Float(resultSet.getFloat(col)).toString());
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_INTEGER.getValue()) {
            if (new Integer(resultSet.getInt(col)).toString().isEmpty()) {
                text.setText("null");
            } else {
                text.setText(new Integer(resultSet.getInt(col)).toString());
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_NULL.getValue()) {
            if (s.contains("articles")) {
                //判断图片
                if (resultSet.getBlob(col) == null) {
                    text.setText("null");
                } else {
                    bytes = resultSet.getBlob(col);
                    showImage.setTag(bytes);
                    text.setText("数据加载中..");
                    if (click_pos == 0 || click_pos == -1) {
                        text.setText("[DATA]");
                    } else if (click_pos == 1) {
                        try {
                            text.setText(new String(bytes, "utf-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //LogUtil.error(TAG, click_pos + "" + "BYTE = " + getFormat(click_pos));
                        getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < 7; i++) {
                                    byte[] newBytes = new byte[bytes.length / 7];
                                    System.arraycopy(bytes, i * bytes.length / 7, newBytes, 0, bytes.length / 7);
                                    if (click_pos == 3) {
                                        final Base64.Decoder decoder = Base64.getDecoder();
                                        final Base64.Encoder encoder = Base64.getEncoder();
                                        final String encodedText = encoder.encodeToString(newBytes);
                                        blob = encodedText;
                                    } else if (click_pos == 2) {
                                        blob = bytesToHexString(newBytes);
                                    }
                                    count += blob;
                                    if (i > 0) {
                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }, 1000);

                        getUITaskDispatcher().delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                text.setText(blob);
                            }
                        }, 5000);
                    }
                }
            } else {
                text.setText(resultSet.getString(col));
            }
        } else {
            text.setText(resultSet.getString(col));
        }
        this.tableLayout.addComponent(item, params);

    }

    //显示一张图片
    private void show(Context context, PixelMapElement pixelMapElement, byte[] bytes) {
        DirectionalLayout directionalLayout = (DirectionalLayout)
                LayoutScatter.getInstance(context)
                        .parse(ResourceTable.Layout_template_ability_tab_popup, null, false);
        Text str_content = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_content);
        Image image = (Image) directionalLayout.findComponentById(ResourceTable.Id_img_show);
        Text imageSize = (Text) directionalLayout.findComponentById(ResourceTable.Id_img_size);
        str_content.setVisibility(Component.HIDE);
        image.setVisibility(Component.VISIBLE);
        image.setImageElement(pixelMapElement);
        imageSize.setVisibility(Component.VISIBLE);
        imageSize.setText(pixelMapElement.getWidth() + "x" + pixelMapElement.getHeight() + "  73KB");
        CommonDialog dialog = new CommonDialog(context);
        dialog.setContentCustomComponent(directionalLayout)
                .setAutoClosable(true)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setCornerRadius(30);
        Text str_cancel = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_cancel);
        str_cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.destroy();
            }
        });
        dialog.setSwipeToDismiss(true);
        dialog.setAutoClosable(true);
        Text str_copy = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_copy);
        str_copy.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyText(count);
                dialog.remove();
                new ToastDialog(context).setComponent(LayoutScatter.getInstance(context).parse(ResourceTable.Layout_toastdialog, null, false)).setOffset(200, 700).show();
            }
        });
        dialog.show();
    }


    // byte数组转换成十六进制输出
    public String bytesToHexString(byte[] bArr) {
        StringBuffer sb = new StringBuffer(bArr.length);
        String sTmp;

        for (int i = 0; i < bArr.length; i++) {
            sTmp = Integer.toHexString(0xFF & bArr[i]);
            if (sTmp.length() < 2)
                sb.append(0);
            sb.append(sTmp.toUpperCase());
        }

        return sb.toString();
    }

    //点击表格时弹窗
    private void show(Context context, String content) {
        DirectionalLayout directionalLayout = (DirectionalLayout)
                LayoutScatter.getInstance(context)
                        .parse(ResourceTable.Layout_template_ability_tab_popup, null, false);
        Text str_content = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_content);
        str_content.setText(content);
        str_content.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
        CommonDialog dialog = new CommonDialog(context);
        dialog.setContentCustomComponent(directionalLayout)
                .setAutoClosable(true)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setCornerRadius(30);
        Text str_cancel = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_cancel);
        str_cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.destroy();
            }
        });
        dialog.setSwipeToDismiss(true);
        dialog.setAutoClosable(true);
        Text str_copy = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_copy);
        str_copy.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyText(content);
                dialog.remove();
                new ToastDialog(context).setComponent(LayoutScatter.getInstance(context).parse(ResourceTable.Layout_toastdialog, null, false)).setOffset(200, 700).show();
            }
        });
        dialog.show();
    }

    /**
     * 功能描述：复制文本到剪切板
     */
    private void copyText(String s) {
        pasteboard = SystemPasteboard.getSystemPasteboard(this);
        PasteData pasteData = new PasteData();
        pasteData.addTextRecord(s);
        PacMap pacMap = new PacMap();
        pacMap.putString("ADDITION_KEY", "ADDITION_VALUE_OF_TEXT");
        pasteData.getProperty().setAdditions(pacMap);
        pasteData.getProperty().setTag("USER_TAG");
        pasteData.getProperty().setLocalOnly(true);
        pasteboard.setPasteData(pasteData);
        pasteData = null;
        /* showText.setText("copyText success");*/
    }


    @Override
    protected void onBackPressed() {
        textField.setText("");
        textField.setHeight(990);
        warningLayout.setVisibility(Component.HIDE);
        search_scrollView.setVisibility(Component.HIDE);
        tableLayout.removeAllComponents();
        InputPrompt.getINSTANCE().setResult("");
        InputPrompt.getINSTANCE().DestroyData();
        preferences.putBoolean("HOME", false);
        terminate();
        super.onBackPressed();
    }

    @Override
    protected void onBackground() {
        Bar.removeAllComponents();
        if (preferences.getBoolean("HOME", true)) {
            preferences.getBoolean("HOME", true);
        }
        super.onBackground();
    }
}
