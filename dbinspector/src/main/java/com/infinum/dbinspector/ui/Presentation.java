package com.infinum.dbinspector.ui;

import ohos.app.Context;

/**
 * 展示类,处理Context和使用的数据
 */

public class Presentation {
    private Presentation() {
    }

    private static Presentation INSTANCE = new Presentation();

    public static Presentation getInstance() {
        return INSTANCE;
    }

    public static class Constants {
        public static class Keys {
            public static final String DATABASE_PATH = "KEY_DATABASE_PATH";
            public static final String DATABASE_FILEPATH = "KEY_DATABASE_FILEPATH";
            public static final String DATABASE_NAME = "KEY_DATABASE_NAME";
            public static final String TABLE_NAME = "KEY_TABLE_NAME";
            public static final String DATABASE_EXTENSION = "KEY_DATABASE_EXTENSION";
            public static final String SCHEMA_NAME = "KEY_SCHEMA_NAME";
            public static final String SHOULD_REFRESH = "KEY_SHOULD_REFRESH";
        }

        public static class Limits {
            public static final int PAGE_SIZE = 100;
            public static final int INITIAL_PAGE = 1;
        }

        public static class Settings {
            public static final int LINES_LIMIT_MINIMUM = 1;
            public static final int LINES_LIMIT_MAXIMUM = 100;
            public static final String BLOB_DATA_PLACEHOLDER = "[ DATA ]";
        }
    }

   public static Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getApplication() {
        if (context != null) {
            return context.getApplicationContext();
        } else {
            throw new NullPointerException("Presentation context has not been initialized");
        }
    }

    private int count = 1;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     *
     * 待替换
     */

    /*fun modules(): List<Module> =
            Domain.modules().plus(
            listOf(
            viewModels()
            )
                    )

    private fun viewModels() = module {
        viewModel { DatabaseViewModel(get(), get(), get(), get(), get()) }
        viewModel { EditDatabaseViewModel(get(), get()) }

        viewModel { SettingsViewModel(get(), get(), get(), get(), get(), get(), get()) }

        viewModel { SchemaViewModel(get(), get()) }

        viewModel { TablesViewModel(get()) }
        viewModel { ViewsViewModel(get()) }
        viewModel { TriggersViewModel(get()) }

        viewModel { TableViewModel(get(), get(), get(), get(), get()) }
        viewModel { ViewViewModel(get(), get(), get(), get(), get()) }
        viewModel { TriggerViewModel(get(), get(), get(), get(), get()) }

        viewModel { PragmaViewModel(get(), get()) }

        viewModel { TableInfoViewModel(get()) }
        viewModel { ForeignKeysViewModel(get()) }
        viewModel { IndexViewModel(get()) }

        viewModel { EditViewModel(get(), get(), get(), get(), get(), get(), get()) }
    }
*/
}
