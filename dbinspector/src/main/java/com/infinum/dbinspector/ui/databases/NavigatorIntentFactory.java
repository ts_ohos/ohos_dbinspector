package com.infinum.dbinspector.ui.databases;

import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.schema.SchemaActivity;
import com.infinum.dbinspector.ui.settings.SettingsActivity;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.javax.xml.validation.Schema;

import java.util.HashSet;
import java.util.Set;

public class NavigatorIntentFactory {

    private AbilitySlice abilitySlice;


    public NavigatorIntentFactory(AbilitySlice abilitySlice) {
        this.abilitySlice = abilitySlice;
    }

    public static Set SUPPORTED_MIME_TYPE = new HashSet();

    static {
        SUPPORTED_MIME_TYPE.add(" application/*");
        SUPPORTED_MIME_TYPE.add("application/x-sqlite3");
        SUPPORTED_MIME_TYPE.add("application/vnd.sqlite3");
    }

    public void showSettings() {
        Intent intent1 = new Intent();
        abilitySlice.present(new SettingsActivity(), intent1);
    }

    public void showSchema(DatabaseDescriptor database) {
        Intent intent1 = new Intent();
        intent1.setParam(Presentation.Constants.Keys.DATABASE_PATH, database.getAbsolutePath());
        intent1.setParam(Presentation.Constants.Keys.DATABASE_NAME, database.getName());
        abilitySlice.present(new SchemaActivity(), intent1);
    }

    public void showShare(DatabaseDescriptor it) {

    }

    /* public Intent showImportChooser(){

     }*/

    /*public void showShare(DatabaseDescriptor database) {
        Intent intent1 = new Intent();
        intent1.setType("application/octet-stream");
        intent1.addFlags(Intent.FLAG_ABILITY_NEW_MISSION)
        abilitySlice.present();
    }*/


}
