package com.infinum.dbinspector.ui.databases;

import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.data.source.raw.OhosDatabasesSource;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.DatabaseRepository;
import com.infinum.dbinspector.domain.database.control.DatabaseControl;
import com.infinum.dbinspector.domain.database.control.converters.DatabaseConverter;
import com.infinum.dbinspector.domain.database.control.mappers.DatabaseDescriptorMapper;
import com.infinum.dbinspector.domain.database.interactors.CopyDatabaseInteractor;
import com.infinum.dbinspector.domain.database.interactors.GetDatabasesInteractor;
import com.infinum.dbinspector.domain.database.interactors.RemoveDatabaseInteractor;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.database.usecases.CopyDatabaseUseCase;
import com.infinum.dbinspector.domain.database.usecases.GetDatabasesUseCase;
import com.infinum.dbinspector.domain.database.usecases.RemoveDatabaseUseCase;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import com.infinum.dbinspector.ui.shared.base.BaseViewModel;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class DatabaseViewModel extends BaseViewModel {

    private static final String TAG = "DatabaseViewModel";

    private Context context;
    private UseCases.GetDatabases getDatabases;
    private UseCases.ImportDatabases importDatabases;
    private UseCases.RemoveDatabase removeDatabase;
    private UseCases.CopyDatabase copyDatabase;

    public DatabaseViewModel(Context context) {
        this.context = context;
    }

    private List<DatabaseDescriptor> databases = new ArrayList<>();


    public List<DatabaseDescriptor> getDatabases() {
        return databases;
    }


    //传输数据
    public void browse(String query) {

        LogUtil.error(TAG, databases + ":browse");

        DatabaseParameters.Get parmentersGet = new DatabaseParameters.Get(context.getApplicationContext(), query);

        OhosDatabasesSource ohosDatabasesSource = new OhosDatabasesSource();

        GetDatabasesInteractor getDatabasesInteractor = new GetDatabasesInteractor(ohosDatabasesSource);

        DatabaseDescriptorMapper databaseDescriptorMapper = new DatabaseDescriptorMapper();

        DatabaseConverter databaseConverter = new DatabaseConverter();

        DatabaseControl databaseControl = new DatabaseControl(databaseDescriptorMapper, databaseConverter);

        DatabaseRepository databaseRepository = new DatabaseRepository(getDatabasesInteractor, databaseControl);

        getDatabases = new GetDatabasesUseCase(databaseRepository);

        try {
            databases = getDatabases.invoke(parmentersGet);
            LogUtil.error(TAG, databases + "");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public void copy(DatabaseDescriptor it) {

        DatabaseParameters.Copy parmentersCommand = new DatabaseParameters.Copy(context.getApplicationContext(), it);

        OhosDatabasesSource ohosDatabasesSource = new OhosDatabasesSource();

        CopyDatabaseInteractor copyDatabaseInteractor = new CopyDatabaseInteractor(ohosDatabasesSource);

        DatabaseDescriptorMapper databaseDescriptorMapper = new DatabaseDescriptorMapper();

        DatabaseConverter databaseConverter = new DatabaseConverter();

        DatabaseControl databaseControl = new DatabaseControl(databaseDescriptorMapper, databaseConverter);

        DatabaseRepository databaseRepository = new DatabaseRepository(copyDatabaseInteractor, databaseControl);

        copyDatabase = new CopyDatabaseUseCase(databaseRepository);
        try {
            databases = copyDatabase.invoke(parmentersCommand);
            LogUtil.error(TAG, databases + "");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    //删除
    public void remove(DatabaseDescriptor it) {

        LogUtil.error(TAG, databases + "");

        DatabaseParameters.Command parmentersCommand = new DatabaseParameters.Command(context.getApplicationContext(), it);

        OhosDatabasesSource ohosDatabasesSource = new OhosDatabasesSource();

        RemoveDatabaseInteractor removeDatabaseInteractor = new RemoveDatabaseInteractor(ohosDatabasesSource);

        DatabaseDescriptorMapper databaseDescriptorMapper = new DatabaseDescriptorMapper();

        DatabaseConverter databaseConverter = new DatabaseConverter();

        DatabaseControl databaseControl = new DatabaseControl(databaseDescriptorMapper, databaseConverter);

        DatabaseRepository databaseRepository = new DatabaseRepository(removeDatabaseInteractor, databaseControl);

        removeDatabase = new RemoveDatabaseUseCase(databaseRepository);
        try {
            databases = removeDatabase.invoke(parmentersCommand);
            LogUtil.error(TAG, databases + "");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }


}
