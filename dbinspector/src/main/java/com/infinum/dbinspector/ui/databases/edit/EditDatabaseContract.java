package com.infinum.dbinspector.ui.databases.edit;

import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

public class EditDatabaseContract {

    private EditDatabaseContract() {
    }

    private static EditDatabaseContract INSTANCE = new EditDatabaseContract();

    public static EditDatabaseContract getINSTANCE(){
        return INSTANCE;
    }

    public void createIntent(AbilitySlice slice, Input input) {

        LogUtil.error("EditDatabaseContract createIntent",""+input.absolutePath+"input.getName() = "+input.getName());
        Intent intent = new Intent();
        intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, input.getAbsolutePath());
        intent.setParam(Presentation.Constants.Keys.DATABASE_FILEPATH, input.getParentPath());
        intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, input.getName());
        intent.setParam(Presentation.Constants.Keys.DATABASE_EXTENSION, input.getExtension());
        slice.presentForResult(new EditDatabaseActivity(), intent, 1);
    }


    public static class Input {
        String absolutePath;
        String parentPath;
        String name;
        String extension;

        public Input(String absolutePath, String parentPath, String name, String extension) {
            this.absolutePath = absolutePath;
            this.parentPath = parentPath;
            this.name = name;
            this.extension = extension;
        }

        public String getAbsolutePath() {
            return absolutePath;
        }

        public void setAbsolutePath(String absolutePath) {
            this.absolutePath = absolutePath;
        }

        public String getParentPath() {
            return parentPath;
        }

        public void setParentPath(String parentPath) {
            this.parentPath = parentPath;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }
    }

}
