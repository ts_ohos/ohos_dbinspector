package com.infinum.dbinspector.ui.databases;

import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;

public class DatabaseInteractions {
    Function<DatabaseDescriptor> onDelete;
    Function<DatabaseDescriptor> onEdit;
    Function<DatabaseDescriptor> onCopy;
    Function<DatabaseDescriptor> onShare;

    public DatabaseInteractions() {
    }


    public Function<DatabaseDescriptor> getOnDelete() {
        return onDelete;
    }

    public void setOnDelete(Function<DatabaseDescriptor> onDelete) {
        this.onDelete = onDelete;
    }

    public Function<DatabaseDescriptor> getOnEdit() {
        return onEdit;
    }

    public void setOnEdit(Function<DatabaseDescriptor> onEdit) {
        this.onEdit = onEdit;
    }

    public Function<DatabaseDescriptor> getOnCopy() {
        return onCopy;
    }

    public void setOnCopy(Function<DatabaseDescriptor> onCopy) {
        this.onCopy = onCopy;
    }

    public Function<DatabaseDescriptor> getOnShare() {
        return onShare;
    }

    public void setOnShare(Function<DatabaseDescriptor> onShare) {
        this.onShare = onShare;
    }
}
