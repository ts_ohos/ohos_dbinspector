package com.infinum.dbinspector.ui.databases;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.content.shared.ContentViewModel;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;


public class DatabaseViewHolder {

    private static final String TAG = "DatabaseViewHolder";
    private Context context;
    private Text path;
    private Text name;
    private Text version;
    private Image removeButton;
    private Image editButton;
    private Image copyButton;
    private Image shareButton;
    private Component content;

    private int count = 1;

    private DatabaseDescriptor item;
    private Function<DatabaseDescriptor> onClick;
    private DatabaseInteractions interactions;

    public Component bind(DatabaseDescriptor item, Function<DatabaseDescriptor> onClick, DatabaseInteractions interactions) {
        this.item = item;
        this.onClick = onClick;
        this.interactions = interactions;
        initView();
        ClickedListener();
        return content;
    }

    public void unbind() {
        removeButton.setClickedListener(null);
        editButton.setClickedListener(null);
        copyButton.setClickedListener(null);
        shareButton.setClickedListener(null);
        content.setClickedListener(null);
    }


    public DatabaseViewHolder() {
     /*   this.context = Presentation.context;*/
    }

    private void ClickedListener() {
        removeButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (item != null) {
                    interactions.getOnDelete().invoke(item);
                }
            }
        });
        editButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (item != null) {
                    interactions.getOnEdit().invoke(item);
                }
            }
        });
        copyButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (item != null) {
                    interactions.getOnCopy().invoke(item);
                }
            }
        });
        shareButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (item != null) {
                    interactions.getOnShare().invoke(item);
                }
            }
        });
        content.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (item != null) {
                    onClick.invoke(item);
                }
            }
        });
    }

    private void initView() {
        content =  LayoutScatter.getInstance(Presentation.getInstance().getApplication()).parse(ResourceTable.Layout_dbinspector_item_database, null, false);
        path = (Text) content.findComponentById(ResourceTable.Id_path);
        name = (Text) content.findComponentById(ResourceTable.Id_name);
        version = (Text) content.findComponentById(ResourceTable.Id_version);
        removeButton = (Image) content.findComponentById(ResourceTable.Id_removeButton);
        editButton = (Image) content.findComponentById(ResourceTable.Id_editButton);
        copyButton = (Image) content.findComponentById(ResourceTable.Id_copyButton);
        shareButton = (Image) content.findComponentById(ResourceTable.Id_shareButton);

        ContentViewModel contentViewModel = new ContentViewModel();
        contentViewModel.setDatabasePath(item.getAbsolutePath());
        contentViewModel.open();
        RdbStore rdbStore = contentViewModel.getRdbStore();

        name.setText(item.getName());
        version.setText(rdbStore.getVersion());
        path.setText(item.getParentPath());
    }

}
