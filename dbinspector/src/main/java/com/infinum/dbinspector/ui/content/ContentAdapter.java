package com.infinum.dbinspector.ui.content;

import com.infinum.dbinspector.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;


import java.util.List;

public class ContentAdapter extends BaseItemProvider {

    private List<TableLayout> mTableLayouts;

    private Context mContext;

    public ContentAdapter(Context context, List<TableLayout> tableLayouts) {
        mContext = context;
        mTableLayouts = tableLayouts;
    }

    @Override
    public int getCount() {
        return mTableLayouts.size();
    }

    @Override
    public Object getItem(int i) {
        return mTableLayouts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_dbinspector_item_table_info, null, false);
            if (component instanceof ComponentContainer) {
                DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT);
                ((ComponentContainer) component).addComponent(mTableLayouts.get(i), layoutConfig);
            }
        }
        return component;
    }

}
