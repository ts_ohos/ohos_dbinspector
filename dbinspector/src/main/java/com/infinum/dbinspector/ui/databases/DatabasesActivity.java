package com.infinum.dbinspector.ui.databases;


import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.ui.databases.edit.EditDatabaseContract;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import com.infinum.dbinspector.ui.shared.searchable.Searchable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.CommonDialog;


public class DatabasesActivity extends BaseActivity implements Searchable {

    private static final String TAG = "DatabasesActivity";

    //主界面放置 Bar
    private DirectionalLayout Bar;

    //具体的Bar页面
    private Component databaseBar;

    //搜索按钮
    private Image search;

    //图标按钮
    private Image icon;

    //设置按钮
    private Image setting;

    //输入框
    private TextField textField;

    //第一个显示的Layout
    private DependentLayout text_layout;

    //点击搜索出现的Layout
    private DependentLayout search_layout;

    //关闭按钮
    private Image close;

    //显示数据页面
    private StackLayout show;

    //搜索为空的页面
    private DirectionalLayout noFind;

    private Boolean isAdd = false;

    //导航工厂
    private NavigatorIntentFactory navigatorIntentFactory = new NavigatorIntentFactory(this);

    //操作控制
    private DatabaseInteractions databaseInteractions;

    //适配器
    private DatabasesAdapter databasesAdapter;

    //ListContainer
    private ListContainer listContainer;

    //ViewModel;
    private DatabaseViewModel viewModel;

    private AbilitySlice slice;

    private DatabaseInteractions setDatabaseInteractions() {
        databaseInteractions = new DatabaseInteractions();
        databaseInteractions.setOnDelete(new Function<DatabaseDescriptor>() {
            @Override
            public void invoke(DatabaseDescriptor it) {
                LogUtil.error(TAG, "onDelete");
                removeDatabase(it);
            }
        });
        databaseInteractions.setOnEdit(new Function<DatabaseDescriptor>() {
            @Override
            public void invoke(DatabaseDescriptor it) {
                LogUtil.error(TAG, "onEdit = " + it.getAbsolutePath() + ".." + it.getParentPath() + "。。" + it.getName() + ".." + it.getExtension());
                EditDatabaseContract.Input input = new EditDatabaseContract
                        .Input(it.getAbsolutePath(), it.getParentPath(), it.getName(), it.getExtension());
                EditDatabaseContract.getINSTANCE().createIntent(slice, input);
            }
        });
        databaseInteractions.setOnCopy(new Function<DatabaseDescriptor>() {
            @Override
            public void invoke(DatabaseDescriptor it) {
                viewModel.copy(it);
                databasesAdapter.add(viewModel.getDatabases());
            }
        });
        databaseInteractions.setOnShare(new Function<DatabaseDescriptor>() {
            @Override
            public void invoke(DatabaseDescriptor it) {
                LogUtil.error(TAG, "onShare");
                navigatorIntentFactory.showShare(it);
            }
        });
        return databaseInteractions;
    }

    //点击删除时跳出弹窗
    private void removeDatabase(DatabaseDescriptor database) {
        CommonDialog commonDialog = new CommonDialog(this);
        Component component = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_commondialog_layout, null, true);
        commonDialog.setContentCustomComponent(component);
        commonDialog.setSize(800, 400);
        commonDialog.siteRemovable(true);
        commonDialog.setAutoClosable(true);

        Text message = (Text) component.findComponentById(ResourceTable.Id_message);
        message.setText("Delete Database " + database.getName());
        Button cancel = (Button) component.findComponentById(ResourceTable.Id_cancel);
        Button ok = (Button) component.findComponentById(ResourceTable.Id_ok);

        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.remove();
            }
        });
        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                viewModel.remove(database);
                //LogUtil.error(TAG,viewModel.getDatabases().get(0).getName()+":"+viewModel.getDatabases().get(01).getName());
                //databasesAdapter = new DatabasesAdapter(viewModel.getDatabases());
                databasesAdapter.removeObject(database);
                commonDialog.remove();
                if (viewModel.getDatabases().size() == 0) {
                    show.addComponent(noFind);
                    listContainer.setVisibility(Component.HIDE);
                }
            }
        });
        commonDialog.show();
    }

    private Function<DatabaseDescriptor> setDatabaseDescriptor() {
        return new Function<DatabaseDescriptor>() {
            @Override
            public void invoke(DatabaseDescriptor databaseDescriptor) {
                navigatorIntentFactory.showSchema(databaseDescriptor);
            }
        };
    }


    /**
     * 控件声明
     */
    @Override
    public void clickLinstener() {

        textField.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int start, int before, int count) {
                LogUtil.error(TAG, "start:" + start + "before:" + before + "count:" + count);
                search(s);
                //表示查询到结果
                LogUtil.error("onTextUpdated", "String : " + s + "size: " + viewModel.getDatabases().size() + "");
                if (viewModel.getDatabases().size() > 0) {
                    //如果有为空指示
                    if (isAdd) {
                        show.removeComponent(noFind);
                        isAdd = false;
                        listContainer.setVisibility(Component.VISIBLE);
                    }
                    databasesAdapter.add(viewModel.getDatabases());
                } else if (!isAdd) {
                    show.addComponent(noFind);
                    listContainer.setVisibility(Component.HIDE);
                    isAdd = true;
                }

            }
        });

        search.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                text_layout.setVisibility(Component.HIDE);
                search_layout.setVisibility(Component.VISIBLE);
            }
        });
        setting.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                navigatorIntentFactory.showSettings();
            }
        });

        close.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                text_layout.setVisibility(Component.VISIBLE);
                search_layout.setVisibility(Component.HIDE);
                textField.clearFocus();
                textField.setText("");

                //如果数据库有值,如果noFind 页面移除掉
                if (viewModel.getDatabases().size() > 0) {
                    if (isAdd) {
                        show.removeComponent(noFind);
                        isAdd = false;
                        listContainer.setVisibility(Component.VISIBLE);
                    }
                }
            }
        });
        icon.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
               /* Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.infinum.dbinspector")
                        .withAbilityName("com.infinum.dbinspector.MainAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent, 0);*/
                terminateAbility();
            }
        });
    }

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_databases;
    }

    @Override
    public void initView() {
        slice = this;
        Bar = (DirectionalLayout) findComponentById(ResourceTable.Id_bar);
        show = (StackLayout) findComponentById(ResourceTable.Id_show);
        databaseBar = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_database_bar, null, false);
        search = (Image) databaseBar.findComponentById(ResourceTable.Id_search);
        icon = (Image) databaseBar.findComponentById(ResourceTable.Id_icon);
        setting = (Image) databaseBar.findComponentById(ResourceTable.Id_settings);
        text_layout = (DependentLayout) databaseBar.findComponentById(ResourceTable.Id_text_layout);
        search_layout = (DependentLayout) databaseBar.findComponentById(ResourceTable.Id_search_layout);
        textField = (TextField) databaseBar.findComponentById(ResourceTable.Id_textFiled);
        close = (Image) databaseBar.findComponentById(ResourceTable.Id_close);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);

        noFind = (DirectionalLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_layout_empty, null, false);
        Bar.addComponent(databaseBar);

        viewModel = new DatabaseViewModel(this);
        //DatabaseParameters.Get 赋值
        viewModel.browse("");
        LogUtil.error(TAG, "viewModel.getDatabases  0 =  " + viewModel.getDatabases().get(0).getAbsolutePath());
        if (viewModel.getDatabases().size() > 0) {
            //适配器中加入点击事件和数据集
            databasesAdapter = new DatabasesAdapter(setDatabaseInteractions(), setDatabaseDescriptor(), viewModel.getDatabases());
            //databasesAdapter.submitList(it);
            listContainer.setItemProvider(databasesAdapter);
        } else {
            show.addComponent(noFind);
            listContainer.setVisibility(Component.HIDE);
        }

        if (Bar.getChildCount() == 0) {
            Bar.addComponent(databaseBar);
        }

    }

    @Override
    public void onSearchOpened() {

    }

    @Override
    public void search(String query) {
        viewModel.browse(query);
    }

    @Override
    public String searchQuery() {
        return null;
    }

    @Override
    public void onSearchClosed() {

    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);
        LogUtil.error(TAG, "onResult");
        //更改名字Rename
        if (requestCode == 1) {
            // Process resultIntent here.
            String oldName = resultIntent.getStringParam("oldName");
            String newName = resultIntent.getStringParam("newName");
            LogUtil.error(TAG,"oldName =" +oldName+"newName=" +newName );
            if (!oldName.equals(newName)){
                databasesAdapter.update(oldName, newName);
            }
        }
    }

    @Override
    protected void onBackground() {
        Bar.removeAllComponents();
        super.onBackground();
    }
}
