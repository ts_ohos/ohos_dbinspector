package com.infinum.dbinspector.ui.schema.views;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.shared.models.Cell;
import com.infinum.dbinspector.ui.schema.shared.SchemaViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class SchemaViewAdapter extends RecycleItemProvider {

    private SchemaViewHolder schemaViewHolder;

    private List<Cell> list = new ArrayList<>();

    private Function<String> onClick;

    public SchemaViewAdapter(RdbStore rdbStore, Function<String> onClick) {
        ResultSet resultSet = rdbStore.querySql("select name from sqlite_master where type='view' order by name", null);
        while (resultSet.goToNextRow()) {
            String userName = resultSet.getString(0);
            list.add(new Cell(userName));
        }
        this.onClick = onClick;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        LogUtil.error("TAG", "view适配器调用");
        schemaViewHolder = new SchemaViewHolder();
        component = schemaViewHolder.bind(list.get(i), onClick);
        return component;
    }

    public int add(String s) {
        List<Cell> newList = new ArrayList<>();
        //遍历适配器数据
        for (int i = 0; i < list.size(); i++) {
            if (s.equals("")) {
                LogUtil.error("搜索为空", "+1");
                newList.add(list.get(i));
            } else {
                //搜索栏不会空,搜索到数据时就把数据显示出来
                if (list.get(i).getText().contains(s)) {
                    newList.add(list.get(i));
                }
            }
        }
        list = newList;
        notifyDataChanged();
        LogUtil.error("SchemaAdapter", list.size() + "");
        return list.size();
    }

    public Cell getList(int i) {
        return list.get(i);
    }
}
