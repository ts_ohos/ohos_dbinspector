package com.infinum.dbinspector.ui.shared.base;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * fraction 父类
 *
 */
public abstract class BaseFraction extends Fraction {

    protected Component mComponent;

    /**
     * 获取布局文件的id
     *
     * @return 布局文件的id
     */
    public abstract int getUIContent();

    /**
     * 初始化组件
     */
    public abstract void initComponent();


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // 将布局文件转换成组件对象
        mComponent = scatter.parse(getUIContent(), container, false);
        // 初始化组件
        initComponent();
        return mComponent;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * 获取string.json文件中定义的字符串
     *
     * @param resId
     * @return
     */
    public String getString(int resId) {
        try {
            return getFractionAbility().getResourceManager().getElement(resId).getString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取color.json文件中定义颜色值
     *
     * @param colorId
     * @return
     */
    public int getColor(int colorId) {
        try {
            return getFractionAbility().getResourceManager().getElement(colorId).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
