package com.infinum.dbinspector.ui.content.shared;

import com.infinum.dbinspector.data.source.memory.connection.OhosConnectionSource;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.connection.ConnectionRepository;
import com.infinum.dbinspector.domain.connection.control.ConnectionControl;
import com.infinum.dbinspector.domain.connection.control.converters.ConnectionConverter;
import com.infinum.dbinspector.domain.connection.control.mappers.ConnectionMapper;
import com.infinum.dbinspector.domain.connection.interactors.OpenConnectionInteractor;
import com.infinum.dbinspector.domain.connection.usecase.OpenConnectionUseCase;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.ui.shared.base.BaseViewModel;
import ohos.data.rdb.RdbStore;

public class ContentViewModel extends BaseViewModel {

    private UseCases.OpenConnection openConnection;
    private UseCases.CloseConnection closeConnection;

    private String databasePath;

    public String getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath = databasePath;
    }

    private RdbStore rdbStore;

    public RdbStore getRdbStore() {
        return rdbStore;
    }

    public void open() {

        ConnectionParameters connectionParameters = new ConnectionParameters(databasePath);

        OhosConnectionSource ohosConnectionSource = new OhosConnectionSource();

        OpenConnectionInteractor openConnectionInteractor = new OpenConnectionInteractor(ohosConnectionSource);

        ConnectionMapper connectionMapper = new ConnectionMapper();

        ConnectionConverter connectionConverter = new ConnectionConverter();

        ConnectionControl connectionControl = new ConnectionControl(connectionMapper, connectionConverter);

        ConnectionRepository connectionRepository = new ConnectionRepository(openConnectionInteractor, connectionControl);

        openConnection = new OpenConnectionUseCase(connectionRepository);

        try {
            //返回一个数据库
            rdbStore = openConnection.invoke(connectionParameters);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }
}
