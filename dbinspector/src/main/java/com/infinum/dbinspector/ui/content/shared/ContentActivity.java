package com.infinum.dbinspector.ui.content.shared;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.edit.EditActivity;
import com.infinum.dbinspector.ui.schema.pragma.PragmaActivity;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.miscservices.pasteboard.PasteData;
import ohos.miscservices.pasteboard.SystemPasteboard;
import ohos.utils.PacMap;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public abstract class ContentActivity extends BaseActivity {

    private final static String TAG = "ContentActivity";

    protected int drop;

    //标题
    private Text db_title;

    //message
    private Text db_message;

    //显示标题栏页面
    private DependentLayout showBar;

    //返回按钮
    private Image db_return;

    //删除按钮
    private Image db_clear;

    //sql语句查询按钮
    private Image db_edit;


    protected RdbStore rdbStore;

    protected String databaseName;
    protected String databasePath;
    protected String schemaName;
    private String tableName;


    private TableLayout tableInfoLayout;

    private String[] columnNames;

    private ContentViewModel contentViewModel;

    private Image db_pragm;

    //title 字段背景
    private ShapeElement shapeElement;

    //table 表格大小
    private TableLayout.LayoutConfig params;

    //存储数据
    private Preferences preferences;


    //格数限制
    private int stop = 200;
    private int limitStop;

    private boolean isLimit;
    private boolean start_click;
    private boolean middle_click;
    private boolean end_click;
    private int click_pos;
    private String blob;

    private SystemPasteboard pasteboard;

    private byte[] bytes;

    String count = "";

    @Override
    protected void onStart(Intent intent) {
        databaseName = intent.getStringParam(Presentation.Constants.Keys.DATABASE_NAME);
        schemaName = intent.getStringParam(Presentation.Constants.Keys.SCHEMA_NAME);
        databasePath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_PATH);
        tableName = intent.getStringParam(Presentation.Constants.Keys.TABLE_NAME);

        LogUtil.error(TAG, "databaseName = " + databaseName + "+ schemaName =" + schemaName + "+databasePath = " + databasePath);
        super.onStart(intent);
        contentViewModel = new ContentViewModel();
        contentViewModel.setDatabasePath(databasePath);
        contentViewModel.open();
        rdbStore = contentViewModel.getRdbStore();
        rdbStore.querySql("PRAGMA foreign_keys = 0", null);


        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        preferences = databaseHelper.getPreferences("setting_filter");
        isLimit = preferences.getBoolean("setting_ok", false);
        limitStop = preferences.getInt("stop", -1);
        start_click = preferences.getBoolean("start_click", true);
        middle_click = preferences.getBoolean("middle_click", false);
        end_click = preferences.getBoolean("end_click", false);

        click_pos = preferences.getInt("radio_click", -1);

    }


    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_content;
    }

    @Override
    public void initView() {
        if (getResultSet() == null) {
            terminate();
        }

        showBar = (DependentLayout) findComponentById(ResourceTable.Id_db_bar);
        DependentLayout db_bar = (DependentLayout) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_dbinspector_db_bar, null, false);
        showBar.addComponent(db_bar);
        db_title = (Text) findComponentById(ResourceTable.Id_db_title);
        db_message = (Text) findComponentById(ResourceTable.Id_db_message);
        db_return = (Image) findComponentById(ResourceTable.Id_db_return);
        db_clear = (Image) findComponentById(ResourceTable.Id_db_clear);
        db_pragm = (Image) findComponentById(ResourceTable.Id_db_pragma);
        db_edit = (Image) findComponentById(ResourceTable.Id_db_edit);
        tableInfoLayout = (TableLayout) findComponentById(ResourceTable.Id_table_info);
        setupUi();
    }


    private void setupUi() {
        db_title.setText(schemaName);
        db_message.setText(databaseName + "-> " + tableName);

        shapeElement = new ShapeElement(this, ResourceTable.Graphic_background_grey);
        if (isLimit) {
            if (start_click) {
                getTableInfo_start();
            } else if (middle_click) {
                getTableInfo_middle();
            } else if (end_click) {
                getTableInfo_end();
            }
        } else {
            getTableInfo();
        }
    }

    //sql 语句查询结果，设置tableLayout
    private ResultSet getResultSet() {
        ResultSet resultSet = null;
        params = new TableLayout.LayoutConfig();
        String error = "";
        try {

            if (schemaName.equals("Trigger")) {
                resultSet = rdbStore.querySql("select * from sqlite_master where type = 'trigger' and name like '%" + databaseName + "%'", null);
                params.width = 400;
                params.height = 800;
            } else if (schemaName.equals("Views")) {
                resultSet = rdbStore.querySql("select * from sqlite_master where type = 'views' and name like '%" + databaseName + "%'", null);
            } else {
                resultSet = rdbStore.querySql("select * from '" + tableName + "' LIMIT " + stop + "", null);
                params.width = 400;
                params.height = 300;
            }
        } catch (Throwable e) {
            error = e.getMessage();
        }
        if (error == "") {
            return resultSet;
        } else {
            return null;
        }
    }

    //正常显示每一个数据
    private void showTable(ResultSet resultSet, int ROWS, int COLS) {

        List<ResultSet.ColumnType> columnTypes = new ArrayList<>();

        resultSet.goToNextRow();
        for (int i = 0; i < columnNames.length; i++) {
            columnTypes.add(resultSet.getColumnTypeForIndex(i));
        }
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                Component item = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_table_info_item, null, false);
                Image showImage = (Image) item.findComponentById(ResourceTable.Id_image_id);
                Text text = (Text) item.findComponentById(ResourceTable.Id_item_info);
                showImage.setVisibility(Component.HIDE);
                text.setVisibility(Component.VISIBLE);
                if (row == 0) {
                    item.setBackground(shapeElement);
                    text.setText(columnNames[col]);
                    if (schemaName.equals("Trigger")) {
                        TableLayout.LayoutConfig newParam = new TableLayout.LayoutConfig(400, 300);
                        tableInfoLayout.addComponent(item, newParam);
                    } else tableInfoLayout.addComponent(item, params);
                } else {
                    showBlob(columnTypes, col, resultSet, text, tableInfoLayout, showImage, item);
                }
                item.setClickable(true);
                item.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (showImage.getTag() != null) {
                            Object object = showImage.getTag();
                            byte[] bytes = (byte[]) object;
                            InputStream inputStream = new ByteArrayInputStream(bytes);
                            ImageSource.SourceOptions scrOpts = new ImageSource.SourceOptions();
                            scrOpts.formatHint = "image/jpg";
                            ImageSource imageSource = ImageSource.create(inputStream, scrOpts);
                            // 设置图片参数
                            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                            PixelMapElement pixelMapElement = new PixelMapElement(pixelMap);
                            show(getContext(), pixelMapElement, bytes);
                        } else show(getContext(), text.getText());
                    }
                });
            }
            if (row != 0) {
                resultSet.goToNextRow();
            }
        }

    }

    private void showBlob(List<ResultSet.ColumnType> columnTypes, int col, ResultSet resultSet, Text text, TableLayout tableInfoLayout, Image showImage, Component item) {
        if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_BLOB.getValue()) {
            if (resultSet.getBlob(col) == null) {
                text.setText("null");
            } else {
                bytes = resultSet.getBlob(col);
                showImage.setTag(bytes);
                text.setText("数据加载中..");
                if (click_pos == 0 || click_pos == -1) {
                    text.setText("[DATA]");
                } else {
                    //LogUtil.error(TAG, click_pos + "" + "BYTE = " + getFormat(click_pos));
                    getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < 7; i++) {
                                try {
                                    byte[] newBytes = new byte[bytes.length / 7];
                                    System.arraycopy(bytes, i * bytes.length / 7, newBytes, 0, bytes.length / 7);
                                    if (click_pos == 3) {
                                        final Base64.Decoder decoder = Base64.getDecoder();
                                        final Base64.Encoder encoder = Base64.getEncoder();
                                        final String encodedText = encoder.encodeToString(newBytes);
                                        blob = encodedText;
                                    } else if (click_pos == 1) {
                                        blob = new String(newBytes, "UTF-8");
                                    } else if (click_pos == 2) {
                                        blob = bytesToHexString(newBytes);
                                    }
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                    LogUtil.error(TAG, " e  =" + e.toString());
                                }
                                count += blob;
                                if (i > 0) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, 1000);

                    getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            text.setText(blob);
                        }
                    }, 10000);
                }
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_FLOAT.getValue()) {
            if (new Float(resultSet.getFloat(col)).toString().isEmpty()) {
                text.setText("null");
            } else {
                text.setText(new Float(resultSet.getFloat(col)).toString());
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_INTEGER.getValue()) {
            if (new Integer(resultSet.getInt(col)).toString().isEmpty()) {
                text.setText("null");
            } else {
                text.setText(new Integer(resultSet.getInt(col)).toString());
            }
        } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_NULL.getValue()) {
            text.setText("null");
        } else {
            text.setText(resultSet.getString(col));
        }
        this.tableInfoLayout.addComponent(item, params);
    }

    //数据库删除操作之后，只显示最上方字段
    private void showDeleteTable(int ROWS, int COLS) {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                Component item1 = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_table_info_item, null, false);
                Text text1 = (Text) item1.findComponentById(ResourceTable.Id_item_info);
                text1.setText(columnNames[col]);
                item1.setBackground(shapeElement);
                tableInfoLayout.addComponent(item1, params);
                item1.setClickable(true);
                item1.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        show(getContext(), text1.getText());
                    }
                });
            }
        }
    }

    //显示某个之间的数据库数据
    private void showBetweenTable(ResultSet resultSet, int start, int end, int COLS) {
        List<ResultSet.ColumnType> columnTypes = new ArrayList<>();

        if (start == 0) {
            resultSet.goToNextRow();
        } else {
            resultSet.goTo(start);
        }
        for (int i = 0; i < columnNames.length; i++) {
            columnTypes.add(resultSet.getColumnTypeForIndex(i));
        }
        for (int row = start; row < end; row++) {
            for (int col = 0; col < COLS; col++) {
                Component item = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_table_info_item, null, false);
                Image showImage = (Image) item.findComponentById(ResourceTable.Id_image_id);
                Text text = (Text) item.findComponentById(ResourceTable.Id_item_info);
                showImage.setVisibility(Component.HIDE);
                text.setVisibility(Component.VISIBLE);
                if (row == start) {
                    item.setBackground(shapeElement);
                    text.setText(columnNames[col]);
                    if (schemaName.equals("Trigger")) {
                        TableLayout.LayoutConfig newParam = new TableLayout.LayoutConfig(400, 300);
                        tableInfoLayout.addComponent(item, newParam);
                    } else tableInfoLayout.addComponent(item, params);
                } else {
                    showBlob(columnTypes, col, resultSet, text, tableInfoLayout, showImage, item);
                }
                item.setClickable(true);
                item.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (showImage.getTag() != null) {
                            Object object = showImage.getTag();
                            byte[] bytes = (byte[]) object;
                            InputStream inputStream = new ByteArrayInputStream(bytes);
                            ImageSource.SourceOptions scrOpts = new ImageSource.SourceOptions();
                            scrOpts.formatHint = "image/jpg";
                            ImageSource imageSource = ImageSource.create(inputStream, scrOpts);
                            // 设置图片参数
                            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                            PixelMapElement pixelMapElement = new PixelMapElement(pixelMap);
                            show(getContext(), pixelMapElement, bytes);
                        } else show(getContext(), text.getText());
                    }
                });
            }
            if (row != 0) {
                resultSet.goToNextRow();
            }
        }
    }

    //正常状态下显示
    public void getTableInfo() {
        if (rdbStore.isOpen()) {

            LogUtil.error(TAG, "schemaName = " + schemaName);
            ResultSet resultSet = getResultSet();
            if (resultSet != null) {
                columnNames = resultSet.getAllColumnNames();
                LogUtil.error(TAG, "columNames len = " + columnNames.length);

                int ROWS = resultSet.getRowCount();
                int COLS = resultSet.getColumnCount();
                ROWS++;
                LogUtil.error(TAG, " rows = " + ROWS);
                tableInfoLayout.setRowCount(ROWS);
                tableInfoLayout.setColumnCount(COLS);

                //但ROWS == 1是表示数据库数据已经被删除 否则正常查询
                if (ROWS != 1) {
                    showTable(resultSet, ROWS, COLS);
                } else {
                    showDeleteTable(ROWS, COLS);
                }
            }
        }
    }

    //显示最前面的限定数量的数据
    private void getTableInfo_start() {
        if (rdbStore.isOpen()) {

            ResultSet resultSet = getResultSet();
            if (resultSet != null) {
                columnNames = resultSet.getAllColumnNames();
                LogUtil.error(TAG, "columNames len = " + columnNames.length);

                int ROWS = resultSet.getRowCount();
                int COLS = resultSet.getColumnCount();
                ROWS++;

                LogUtil.error(TAG, "limitStop = " + limitStop + "ROWS = " + ROWS);
                //限制大于Rows 正常显示
                if (limitStop >= ROWS) {
                    tableInfoLayout.setRowCount(ROWS);
                    tableInfoLayout.setColumnCount(COLS);

                    //但ROWS == 1是表示数据库数据已经被删除 否则正常查询
                    if (ROWS != 1) {
                        showTable(resultSet, ROWS, COLS);
                    } else {
                        showDeleteTable(ROWS, COLS);
                    }
                } else {
                    tableInfoLayout.setRowCount(limitStop);
                    tableInfoLayout.setColumnCount(COLS);

                    //但ROWS == 1是表示数据库数据已经被删除 否则正常查询
                    if (ROWS != 1) {
                        showTable(resultSet, limitStop + 1, COLS);
                    } else {
                        showDeleteTable(ROWS, COLS);
                    }
                }
            }
        }
    }

    //显示最后面的限定数量的数据
    private void getTableInfo_end() {
        if (rdbStore.isOpen()) {
            LogUtil.error(TAG, "schemaName = " + schemaName);
            ResultSet resultSet = getResultSet();
            if (resultSet != null) {
                columnNames = resultSet.getAllColumnNames();
                LogUtil.error(TAG, "columNames len = " + columnNames.length);

                int ROWS = resultSet.getRowCount();
                int COLS = resultSet.getColumnCount();
                ROWS++;

                LogUtil.error(TAG, "limitStop = " + limitStop + "ROWS = " + ROWS);
                if (limitStop >= ROWS) {
                    tableInfoLayout.setRowCount(ROWS);
                    tableInfoLayout.setColumnCount(COLS);
                    if (ROWS != 1) {
                        showTable(resultSet, ROWS, COLS);
                    } else {
                        showDeleteTable(ROWS, COLS);
                    }
                } else {
                    tableInfoLayout.setRowCount(limitStop + 1);
                    tableInfoLayout.setColumnCount(COLS);
                    if (ROWS != 1) {
                        showBetweenTable(resultSet, ROWS - limitStop - 1, ROWS, COLS);
                    } else {
                        showDeleteTable(ROWS, COLS);
                    }
                }
            }
        }
    }

    //显示中间的限定数量的数据
    private void getTableInfo_middle() {
        if (rdbStore.isOpen()) {
            ResultSet resultSet = getResultSet();
            if (resultSet != null) {
                columnNames = resultSet.getAllColumnNames();
                LogUtil.error(TAG, "columNames len = " + columnNames.length);

                int ROWS = resultSet.getRowCount();
                int COLS = resultSet.getColumnCount();
                ROWS++;
                LogUtil.error(TAG, "limitStop = " + limitStop + "ROWS = " + ROWS);
                //限制大于Rows 正常显示
                if (limitStop >= ROWS) {
                    tableInfoLayout.setRowCount(ROWS);
                    tableInfoLayout.setColumnCount(COLS);

                    //但ROWS == 1是表示数据库数据已经被删除 否则正常查询
                    if (ROWS != 1) {
                        showTable(resultSet, ROWS, COLS);
                    } else {
                        showDeleteTable(ROWS, COLS);
                    }
                } else if (ROWS == 1) {
                    tableInfoLayout.setRowCount(ROWS);
                    tableInfoLayout.setColumnCount(COLS);
                    showDeleteTable(ROWS, COLS);
                } else {
                    tableInfoLayout.setRowCount(limitStop + 1);
                    tableInfoLayout.setColumnCount(COLS);
                    showBetweenTable(resultSet, ROWS / 2 , ROWS / 2 + limitStop+1 , COLS);
                    }
                }
        }

    }

    //显示一张图片
    private void show(Context context, PixelMapElement pixelMapElement, byte[] bytes) {
        DirectionalLayout directionalLayout = (DirectionalLayout)
                LayoutScatter.getInstance(context)
                        .parse(ResourceTable.Layout_template_ability_tab_popup, null, false);
        Text str_content = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_content);
        Image image = (Image) directionalLayout.findComponentById(ResourceTable.Id_img_show);
        Text imageSize = (Text) directionalLayout.findComponentById(ResourceTable.Id_img_size);
        str_content.setVisibility(Component.HIDE);
        image.setVisibility(Component.VISIBLE);
        image.setImageElement(pixelMapElement);
        imageSize.setVisibility(Component.VISIBLE);
        imageSize.setText(pixelMapElement.getWidth() + "x" + pixelMapElement.getHeight() + "  73KB");
        CommonDialog dialog = new CommonDialog(context);
        dialog.setContentCustomComponent(directionalLayout)
                .setAutoClosable(true)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setCornerRadius(30);
        Text str_cancel = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_cancel);
        str_cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.destroy();
            }
        });
        dialog.setSwipeToDismiss(true);
        dialog.setAutoClosable(true);
        Text str_copy = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_copy);
        str_copy.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyText(count);
                dialog.remove();
                new ToastDialog(context).setComponent(LayoutScatter.getInstance(context).parse(ResourceTable.Layout_toastdialog, null, false)).setOffset(200, 700).show();
            }
        });
        dialog.show();
    }

    // byte数组转换成十六进制输出
    public String bytesToHexString(byte[] bArr) {
        StringBuffer sb = new StringBuffer(bArr.length);
        String sTmp;

        for (int i = 0; i < bArr.length; i++) {
            sTmp = Integer.toHexString(0xFF & bArr[i]);
            if (sTmp.length() < 2)
                sb.append(0);
            sb.append(sTmp.toUpperCase());
        }
        return sb.toString();
    }

    //点击表格时弹窗
    private void show(Context context, String content) {
        DirectionalLayout directionalLayout = (DirectionalLayout)
                LayoutScatter.getInstance(context)
                        .parse(ResourceTable.Layout_template_ability_tab_popup, null, false);
        Text str_content = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_content);
        Image image = (Image) directionalLayout.findComponentById(ResourceTable.Id_img_show);
        Text imageSize = (Text) directionalLayout.findComponentById(ResourceTable.Id_img_size);
        str_content.setVisibility(Component.VISIBLE);
        image.setVisibility(Component.HIDE);
        image.setVisibility(Component.HIDE);
        str_content.setText(content);
        str_content.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
        CommonDialog dialog = new CommonDialog(context);
        dialog.setContentCustomComponent(directionalLayout)
                .setAutoClosable(true)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.CENTER)
                .setCornerRadius(30);
        Text str_cancel = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_cancel);
        str_cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dialog.destroy();
            }
        });
        dialog.setSwipeToDismiss(true);
        dialog.setAutoClosable(true);
        Text str_copy = (Text) directionalLayout.findComponentById(ResourceTable.Id_text_copy);
        str_copy.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                copyText(content);
                dialog.remove();
                new ToastDialog(context).setComponent(LayoutScatter.getInstance(context).parse(ResourceTable.Layout_toastdialog, null, false)).setOffset(200, 700).show();
            }
        });
        dialog.show();
    }

    /**
     * 功能描述：复制文本到剪切板
     */
    private void copyText(String s) {
        pasteboard = SystemPasteboard.getSystemPasteboard(this);
        PasteData pasteData = new PasteData();
        pasteData.addTextRecord(s);
        PacMap pacMap = new PacMap();
        pacMap.putString("ADDITION_KEY", "ADDITION_VALUE_OF_TEXT");
        pasteData.getProperty().setAdditions(pacMap);
        pasteData.getProperty().setTag("USER_TAG");
        pasteData.getProperty().setLocalOnly(true);
        pasteboard.setPasteData(pasteData);
        pasteData = null;
        /* showText.setText("copyText success");*/
    }


    @Override
    public void clickLinstener() {
        db_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });
        db_clear.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                LogUtil.error(TAG, "LimitStop = " + limitStop + "start_click = " + start_click + "middle_click = " + middle_click + "end_click = " + end_click);
                try {
                    drop(tableName);
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        db_pragm.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setParam(Presentation.Constants.Keys.SCHEMA_NAME, schemaName);
                intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, databaseName);
                intent.setParam(Presentation.Constants.Keys.TABLE_NAME, tableName);
                intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, databasePath);
                present(new PragmaActivity(), intent);
            }
        });

        db_edit.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setParam(Presentation.Constants.Keys.DATABASE_NAME, databaseName);
                intent.setParam(Presentation.Constants.Keys.DATABASE_PATH, databasePath);
                present(new EditActivity(), intent);
            }
        });
    }


    //点击删除时跳出弹窗
    private void drop(String tableName) throws NotExistException, WrongTypeException, IOException {
        CommonDialog commonDialog = new CommonDialog(this);
        Component component = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_commondialog_layout, null, true);
        commonDialog.setContentCustomComponent(component);
        commonDialog.setSize(800, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        //设置点击返回键时删除对话框
        commonDialog.siteRemovable(true);
        //设置点击外部隐藏
        commonDialog.setAutoClosable(true);

        Text name = (Text) component.findComponentById(ResourceTable.Id_name);
        name.setText(ResourceTable.String_dbinspector_title_info);
        Text message = (Text) component.findComponentById(ResourceTable.Id_message);
        message.setText(getResourceManager().getElement(drop).getString() + " " + schemaName);
        Button cancel = (Button) component.findComponentById(ResourceTable.Id_cancel);
        Button ok = (Button) component.findComponentById(ResourceTable.Id_ok);

        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                commonDialog.remove();
            }
        });
        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                RawRdbPredicates rawRdbPredicates = new RawRdbPredicates("'" + tableName + "'");
                int count = rdbStore.delete(rawRdbPredicates);
                LogUtil.error(TAG, "删除行数= " + count);
                tableInfoLayout.removeAllComponents();
                terminate();
                commonDialog.remove();
                //修改改变所有copy的值的内容
            }
        });
        commonDialog.show();
    }

    @Override
    protected void onBackground() {
        showBar.removeAllComponents();
        tableInfoLayout.removeAllComponents();
        super.onBackground();
    }
}
