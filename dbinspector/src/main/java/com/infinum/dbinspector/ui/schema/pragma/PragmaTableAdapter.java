package com.infinum.dbinspector.ui.schema.pragma;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class PragmaTableAdapter extends PageSliderProvider {

    private Context mContext;

    private List<ResultSet> pages;

    private List<Component> newPages;

    public PragmaTableAdapter(ArrayList<ResultSet> pages, Context context) {
        this.pages = pages;
        mContext = context;
        newPages = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            if (i == 0) {
                ResultSet resultSet = pages.get(i);
                ComponentContainer root = (ComponentContainer) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_pragma_page_item,
                        null, false);
                TableLayout tableLayout = (TableLayout) root.findComponentById(ResourceTable.Id_param_list);
                createTableLayout(resultSet, tableLayout);
                newPages.add(root);
            } else if (i == 1) {
                ResultSet resultSet = pages.get(i);
                ComponentContainer root = (ComponentContainer) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_pragma_page_item_one,
                        null, false);
                TableLayout tableLayout = (TableLayout) root.findComponentById(ResourceTable.Id_param_list);
                createTableLayout(resultSet, tableLayout);
                newPages.add(root);
            } else if (i == 2) {
                ResultSet resultSet = pages.get(i);
                ComponentContainer root = (ComponentContainer) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_progma_page_item_two,
                        null, false);
                TableLayout tableLayout = (TableLayout) root.findComponentById(ResourceTable.Id_param_list);
                createTableLayout(resultSet, tableLayout);
                newPages.add(root);
            }
        }
    }

    @Override
    public int getCount() {
        return pages == null ? 0 : pages.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        componentContainer.addComponent(newPages.get(i));
        return componentContainer;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeAllComponents();
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }



    private String[] columnNames;
    //是否已经删除
    private int stop = 100;

    public void createTableLayout(ResultSet resultSet, TableLayout tableLayout) {
        if (resultSet != null) {
            columnNames = resultSet.getAllColumnNames();
            // LogUtil.error(TAG, "columNames len = " + columnNames.length);

            int ROWS = resultSet.getRowCount();
            int COLS = resultSet.getColumnCount();
            ROWS++;

            tableLayout.setRowCount(ROWS);
            tableLayout.setColumnCount(COLS);

            List<ResultSet.ColumnType> columnTypes = new ArrayList<>();

            TableLayout.LayoutConfig params = new TableLayout.LayoutConfig();
            params.width = 300;
            params.height = 400;
            LogUtil.error("Adatper", "ROWS = " + ROWS + " COLS = " + COLS);
            if (ROWS > 1) {
                resultSet.goToNextRow();
                for (int i = 0; i < columnNames.length; i++) {
                    if (i == stop) {
                        break;
                    }
                    columnTypes.add(resultSet.getColumnTypeForIndex(i));
                }

                for (int row = 0; row < ROWS; row++) {
                    if (row == stop) {
                        break;
                    }
                    for (int col = 0; col < COLS; col++) {
                        Component item = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_table_info_item_pragma, null, false);
                        Text text = (Text) item.findComponentById(ResourceTable.Id_item_info);
                        if (row == 0) {
                            text.setText(columnNames[col]);
                            item.setBackground(new ShapeElement(mContext, ResourceTable.Graphic_background_grey));
                            tableLayout.addComponent(item, params);
                        } else {
                            if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_BLOB.getValue()) {
                                if (resultSet.getBlob(col) == null) {
                                    text.setText("null");
                                } else {
                                    text.setText(resultSet.getBlob(col).toString());
                                }
                            } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_FLOAT.getValue()) {
                                text.setText(new Float(resultSet.getFloat(col)).toString());
                            } else if (columnTypes.get(col).getValue() == ResultSet.ColumnType.TYPE_INTEGER.getValue()) {
                                text.setText(new Integer(resultSet.getInt(col)).toString());
                            } else {
                                if (resultSet.getBlob(col) == null) {
                                    text.setText("null");
                                } else {
                                    text.setText(resultSet.getString(col));
                                }
                            }
                            tableLayout.addComponent(item, params);
                        }
                    }
                    if (row != 0) {
                        resultSet.goToNextRow();
                    }
                }
            } else {
                for (int col = 0; col < COLS; col++) {
                    Component item1 = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_table_info_item, null, false);
                    Text text1 = (Text) item1.findComponentById(ResourceTable.Id_item_info);
                    text1.setText(columnNames[col]);
                    tableLayout.addComponent(item1, params);
                }
            }
        }
    }


}
