package com.infinum.dbinspector.ui.schema.pragma;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.content.shared.ContentActivity;
import com.infinum.dbinspector.ui.content.shared.ContentViewModel;
import com.infinum.dbinspector.ui.schema.SchemaViewModel;
import com.infinum.dbinspector.ui.schema.shared.PageSliderAdapter;
import com.infinum.dbinspector.ui.schema.shared.ViewCreateHelper;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

public class PragmaActivity extends BaseActivity {

    //页面切换
    private PageSlider pageSlider;

    //标题栏所在页面
    private DependentLayout pragma_bar;

    //具体标题栏
    private DependentLayout pragmaBar;

    //标签页面
    private TabList tabList;

    private PragmaCreateHelper viewCreateHelper;

    private ContentViewModel contentViewModel;

    private String databaseName;
    private String databasePath;
    private String tableName;
    private String schemaName;

    private Text pragma_message;

    protected RdbStore rdbStore;

    private Image pragma_return;

    private static final String TAG = PragmaActivity.class.getSimpleName();

    @Override
    public void  initView() {
        pragma_bar = (DependentLayout) findComponentById(ResourceTable.Id_pragma_bar);
        pragmaBar = (DependentLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_pragma_bar, null, false);
        pragma_bar.addComponent(pragmaBar);

        pragma_return = (Image) pragmaBar.findComponentById(ResourceTable.Id_pragma_return);

        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list_pragma);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider_pragma);

        pragma_message = (Text) pragmaBar.findComponentById(ResourceTable.Id_pragma_bar_message);
        pragma_message.setText(databaseName + " -> " + tableName);

      //  pragma_message.setText("blob" + "  -> " + schemaName);
        String[] tabTitle = new String[0];
        try {
            tabTitle = getResourceManager().getElement(ResourceTable.Strarray_tab_list_pragma).getStringArray();
        }catch (IOException e) {
            e.printStackTrace();
        }catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        for (String title : tabTitle) {
            TabList.Tab tab = tabList.new Tab(getContext());
            tab.setText(title);
            tabList.addTab(tab);
        }
        LogUtil.error(TAG, "length = " + tabTitle.length);
        tabList.selectTabAt(0);
        tabList.setFixedMode(true);

        setupUi(databaseName, databasePath);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        databaseName = intent.getStringParam(Presentation.Constants.Keys.DATABASE_NAME);
        databasePath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_PATH);
        schemaName = intent.getStringParam(Presentation.Constants.Keys.SCHEMA_NAME);
        tableName = intent.getStringParam(Presentation.Constants.Keys.TABLE_NAME);

        contentViewModel = new ContentViewModel();
        contentViewModel.setDatabasePath(databasePath);
        contentViewModel.open();
        rdbStore = contentViewModel.getRdbStore();
    }

    @Override
    protected void onActive() {
        super.onActive();

    }
    private void setupUi(String databaseName, String databasePath) {
        pageSlider.setProvider(new PragmaTableAdapter(initPageSliderViewData(), getContext()));
        pageSlider.setCurrentPage(0);
        pageSlider.setReboundEffect(true);
        pageSlider.setCentralScrollMode(true);

      /*  tablesFraction = new TablesFraction(viewModel.getRdbStore());
        fractionManager = ((FractionAbility) getAbility()).getFractionManager();
        fractionManager.startFractionScheduler().add(ResourceTable.Id_schema_show, tablesFraction).submit();*/
    }

    private ArrayList<ResultSet> initPageSliderViewData() {
        int pageSize = tabList.getTabCount();
        ArrayList<ResultSet> pages = new ArrayList<>();
        viewCreateHelper = new PragmaCreateHelper(getContext());
        LogUtil.error(TAG, "pageSize1 = " + pageSize);

        for (int i = 0; i < pageSize; i++) {
            pages.add(viewCreateHelper.createResultSet(contentViewModel.getRdbStore(), tabList.getTabAt(i).getText(),databasePath, tableName));
        }


        LogUtil.error(TAG, "pageSiz2 = " + pages.size());

        return pages;
    }
    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_pragma;
    }

    @Override
    public void clickLinstener() {
        pragma_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onBackPressed();
            }
        });

        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                if (pageSlider.getCurrentPage() != tab.getPosition()) {
                    pageSlider.setCurrentPage(tab.getPosition());
                }
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                if (tabList.getSelectedTab().getPosition() != i) {
                    tabList.selectTabAt(i);
                }
            }
        });

    }


    @Override
    protected void onBackground() {
        super.onBackground();
        tabList.removeAllComponents();
    }
}
