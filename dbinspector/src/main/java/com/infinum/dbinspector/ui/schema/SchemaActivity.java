package com.infinum.dbinspector.ui.schema;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.Presentation;
import com.infinum.dbinspector.ui.edit.EditActivity;
import com.infinum.dbinspector.ui.schema.pragma.PragmaActivity;
import com.infinum.dbinspector.ui.schema.shared.PageSliderAdapter;
import com.infinum.dbinspector.ui.schema.shared.ViewCreateHelper;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import com.infinum.dbinspector.ui.shared.searchable.Searchable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

public class SchemaActivity extends BaseActivity implements Searchable {

    private static final String TAG = "SchemaActivity";

    //标题栏所在页面
    private DependentLayout schema_bar;

    //具体标题栏
    private DependentLayout schemaBar;

    //标签页面
    private TabList tabList;

    //搜索按钮
    private Image schema_search;

    //返回按钮
    private Image schema_return;

    //输入按钮
    private Image schema_edit;

    //显示Text页面
    private DependentLayout text_layout;

    //text_message
    private Text schema_message;

    //显示搜索页面
    private DependentLayout search_layout;

    //关闭搜索页面
    private Image close;

    //搜索页面的输入框
    private TextField textField;

    //搜索为空的页面
    private DirectionalLayout noFind;


    //页面是否有东西
    private Boolean isAdd = false;

    //页面切换
    private PageSlider pageSlider;

    private SchemaViewModel viewModel;

    private Image doSearch;

    private String databaseName;
    private String databasePath;

    private ViewCreateHelper viewCreateHelper;

    private DependentLayout showTabList;

    private DependentLayout showPageSlider;

    @Override
    protected void onStart(Intent intent) {
        databaseName = intent.getStringParam(Presentation.Constants.Keys.DATABASE_NAME);
        databasePath = intent.getStringParam(Presentation.Constants.Keys.DATABASE_PATH);
        super.onStart(intent);

        //tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);

    }

    private void refreshDatabases() {
        search(searchQuery());
    }

    private void setupUi(String databaseName, String databasePath) {
        viewModel = new SchemaViewModel();
        viewModel.setDatabasePath(databasePath);
        schema_message.setText(databaseName);
        viewModel.open();
        pageSlider.setProvider(new PageSliderAdapter(initPageSliderViewData()));
        pageSlider.setCurrentPage(0);
        pageSlider.callOnClick();
        pageSlider.invalidate();
        pageSlider.setReboundEffect(true);
        pageSlider.setCentralScrollMode(true);

      /*  tablesFraction = new TablesFraction(viewModel.getRdbStore());
        fractionManager = ((FractionAbility) getAbility()).getFractionManager();
        fractionManager.startFractionScheduler().add(ResourceTable.Id_schema_show, tablesFraction).submit();*/
    }

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_schema;
    }

    @Override
    public void initView() {
        //添加标题栏
        schema_bar = (DependentLayout) findComponentById(ResourceTable.Id_schema_bar);
        schemaBar = (DependentLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_schema_bar, null, false);
        schema_bar.addComponent(schemaBar);
        //pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        showPageSlider = (DependentLayout) findComponentById(ResourceTable.Id_show_pager);
        showTabList = (DependentLayout) findComponentById(ResourceTable.Id_show_tab_list);

        /**
         * 解决页面缓存问题，自定义TabList 和 PageSlider
         */
        tabList = new TabList(getContext());
        tabList.setTabTextSize(50);
        tabList.setOrientation(Component.HORIZONTAL);
        tabList.setTabTextColors(Color.getIntColor("#999999"), Color.getIntColor("#0072E3"));
        tabList.setSelectedTabIndicatorColor(Color.getIntColor("#0072E3"));
        tabList.setSelectedTabIndicatorHeight(2);
        tabList.setMinHeight(150);
        tabList.setTabTextAlignment(TextAlignment.CENTER);
        DependentLayout.LayoutConfig tabListLayoutConfig =
                new DependentLayout.LayoutConfig(DependentLayout.LayoutConfig.MATCH_PARENT,
                        DependentLayout.LayoutConfig.MATCH_CONTENT);
        DependentLayout.LayoutConfig pageSliderLayoutConfig =
                new DependentLayout.LayoutConfig(DependentLayout.LayoutConfig.MATCH_PARENT,
                        DependentLayout.LayoutConfig.MATCH_CONTENT);
        showTabList.addComponent(tabList, tabListLayoutConfig);
        pageSlider = new PageSlider(getContext());
        showPageSlider.addComponent(pageSlider, pageSliderLayoutConfig);
        schema_search = (Image) schemaBar.findComponentById(ResourceTable.Id_schema_search);
        schema_return = (Image) schemaBar.findComponentById(ResourceTable.Id_schema_return);
        text_layout = (DependentLayout) schemaBar.findComponentById(ResourceTable.Id_text_layout);
        search_layout = (DependentLayout) schemaBar.findComponentById(ResourceTable.Id_search_layout);
        doSearch = (Image) search_layout.findComponentById(ResourceTable.Id_search);
        close = (Image) schemaBar.findComponentById(ResourceTable.Id_close);
        textField = (TextField) schemaBar.findComponentById(ResourceTable.Id_textFiled);
        schema_message = (Text) schemaBar.findComponentById(ResourceTable.Id_schema_bar_message);
        schema_edit = (Image) schemaBar.findComponentById(ResourceTable.Id_schema_edit);

        noFind = (DirectionalLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_layout_empty, null, false);
        //  show = (DependentLayout) findComponentById(ResourceTable.Id_schema_show);


        String[] tabTitle = new String[0];
        try {
            tabTitle = getResourceManager().getElement(ResourceTable.Strarray_tab_list).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

        for (String title : tabTitle) {
            TabList.Tab tab = tabList.new Tab(getContext());
            tab.setText(title);
            tabList.addTab(tab);
        }
        tabList.selectTabAt(0);
        tabList.setFixedMode(true);
        LogUtil.error(TAG, "databaseName:" + databaseName + "databasePath:" + databasePath);
        setupUi(databaseName, databasePath);
        //refreshDatabases();
    }


    private ArrayList<Component> initPageSliderViewData() {
        int pageSize = tabList.getTabCount();
        ArrayList<Component> pages = new ArrayList<>();
        viewCreateHelper = new ViewCreateHelper(this);
        for (int i = 0; i < pageSize; i++) {
            pages.add(viewCreateHelper.createView(databaseName,viewModel.getRdbStore(), tabList.getTabAt(i).getText(),databasePath));
        }
        return pages;
    }

    @Override
    public void clickLinstener() {

        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                if (pageSlider.getCurrentPage() != tab.getPosition()) {
                    pageSlider.setCurrentPage(tab.getPosition());
                }
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
                // 当某个Tab从选中状态变为未选中状态时的回调
            }

            @Override
            public void onReselected(TabList.Tab tab) {
                // 当某个Tab已处于选中状态，再次被点击时的状态回调
            }
        });

        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                if (tabList.getSelectedTab().getPosition() != i) {
                    tabList.selectTabAt(i);
                }
            }
        });

        schema_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });

        schema_search.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtil.error("schema_search", "点击");
                text_layout.setVisibility(Component.HIDE);
                search_layout.setVisibility(Component.VISIBLE);
            }
        });

        schema_edit.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setParam(Presentation.Constants.Keys.DATABASE_NAME,databaseName);
                intent.setParam(Presentation.Constants.Keys.DATABASE_PATH,databasePath);
                present(new EditActivity(), intent);
            }
        });
        close.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                text_layout.setVisibility(Component.VISIBLE);
                int position = tabList.getSelectedTabIndex();
                viewCreateHelper.noFind(position);
                if (textField.equals("")) {
                    search_layout.setVisibility(Component.HIDE);
                    textField.clearFocus();
                } else {
                    textField.setText("");
                    textField.clearFocus();
                    search_layout.setVisibility(Component.HIDE);
                }
            }
        });

        textField.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int start, int before, int count) {
                LogUtil.error(TAG, "onTextUpdated: String " + s);
                search(s);
                int position = tabList.getSelectedTabIndex();
                viewCreateHelper.noFind(position);
            }
        });
    }


    @Override
    protected void onBackground() {
        //标题栏移除
        schema_bar.removeComponent(schemaBar);

        //tab标签移除
        tabList.removeAllComponents();

        //关闭数据库
        viewModel.close();
        showPageSlider.removeAllComponents();
        showTabList.removeAllComponents();

        super.onBackground();
    }


    @Override
    public void onSearchOpened() {

    }

    @Override
    public void search(String query) {
        viewCreateHelper.search(query);
    }

    @Override
    public String searchQuery() {
        String query;
        if (!textField.getText().equals("")) {
            query = textField.getText();
        } else {
            query = databaseName;
        }
        return query;
    }

    @Override
    public void onSearchClosed() {

    }
}
