/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.ui.settings;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.ui.edit.InputPrompt;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.*;

public class SettingsActivity extends BaseActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    //标题栏
    private DirectionalLayout setting_bar;

    private Component settingBar;
    //返回按钮
    private Image setting_return;

    //输入框
    private TextField tableNameInputLayout;

    //输入的数据
    private String editText = null;

    //输入框上小字
    private Text textFieldUp;

    //输入框添加
    private Image tick;

    //添加布局所在的
    private DirectionalLayout add_tableName;

    //点击打勾显示
    private Image click_ok;

    private Boolean isClikc = false;

    //显示的页面
    private DependentLayout setting_ProgressBar;

    //number
    private Text number;

    //setting_add;
    private Image setting_add;

    //setting_delete
    private Image setting_delete;

    private Slider slider;

    //数据值
    private int count;

    //三个按钮 start middle end
    private Button start;
    private Button middle;
    private Button end;

    private Context context;

    private Preferences preferences;

    private ShapeElement selectShapeElement;
    private ShapeElement noSelectShapeElement;

    //单选按钮
    private RadioContainer radioContainer;

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_dbinspector_activity_settings;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        context = this;
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        preferences = databaseHelper.getPreferences("setting_filter");

        //标题栏
        setting_bar = (DirectionalLayout) findComponentById(ResourceTable.Id_setting_bar);
        settingBar = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dbinspector_setting_bar, null, false);
        setting_bar.addComponent(settingBar);

        //add_table
        tableNameInputLayout = (TextField) findComponentById(ResourceTable.Id_tableNameInputLayout);
        textFieldUp = (Text) findComponentById(ResourceTable.Id_textFieldUp);
        tick = (Image) findComponentById(ResourceTable.Id_setting_tick);
        add_tableName = (DirectionalLayout) findComponentById(ResourceTable.Id_add_tableName);
        setting_return = (Image) findComponentById(ResourceTable.Id_setting_return);

        //点击打勾 限制
        click_ok = (Image) findComponentById(ResourceTable.Id_click_ok);
        setting_ProgressBar = (DependentLayout) findComponentById(ResourceTable.Id_setting_ProgressBar_layout);
        number = (Text) findComponentById(ResourceTable.Id_number);
        setting_add = (Image) findComponentById(ResourceTable.Id_setting_add);
        setting_delete = (Image) findComponentById(ResourceTable.Id_setting_delete);
        slider = (Slider) findComponentById(ResourceTable.Id_setting_slider);

        //三个按钮
        start = (Button) findComponentById(ResourceTable.Id_START);
        middle = (Button) findComponentById(ResourceTable.Id_MIDDLE);
        end = (Button) findComponentById(ResourceTable.Id_END);

        selectShapeElement = new ShapeElement(this, ResourceTable.Graphic_background_click_Button);
        noSelectShapeElement = new ShapeElement(this, ResourceTable.Graphic_background_linear_layout);

        radioContainer = (RadioContainer) findComponentById(ResourceTable.Id_RadioContainer);
        if (preferences.getInt("radio_click", -1) == -1) {
            radioContainer.mark(0);
        } else {
            radioContainer.mark(preferences.getInt("radio_click", -1));
        }

        buttonInitialize();
        //获取添加的需要过滤的字符
        getSearchFilter();
    }

    private void buttonInitialize() {
        start.setBackground(selectShapeElement);

        if (preferences.getBoolean("middle_click", false)) {
            middle.setBackground(selectShapeElement);
            start.setBackground(noSelectShapeElement);
        }
        if (preferences.getBoolean("end_click", false)) {
            end.setBackground(selectShapeElement);
            start.setBackground(noSelectShapeElement);
        }

    }

    @Override
    public void initView() {
        count = preferences.getInt("stop", 0);
    }

    @Override
    public void clickLinstener() {
        setting_return.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
            }
        });
        tableNameInputLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                tableNameInputLayout.setHint("");
                textFieldUp.setVisibility(Component.VISIBLE);
            }
        });

        tick.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                LogUtil.error("setting", tableNameInputLayout.getText());
                String edit = tableNameInputLayout.getText();
                if (!edit.isEmpty() && !search_filter.contains(edit)) {
                    /*if (edit.equals("")) {
                        if (!isAdd) {
                            isAdd = true;
                        } else return;
                    }*/
                    editText = edit;
                    creatLayout(editText);
                    tableNameInputLayout.setText("");
                    if (search_filter != null) {
                        search_filter.add(editText);
                    }
                }
            }
        });

        //显示最大显示的行数
        click_ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!isClikc) {
                    //显示
                    setting_ProgressBar.setVisibility(Component.VISIBLE);
                    number.setVisibility(Component.VISIBLE);
                    click_ok.setPixelMap(ResourceTable.Media_setting_ok);
                    isClikc = true;
                } else {
                    //隐藏
                    isClikc = false;
                    number.setVisibility(Component.HIDE);
                    setting_ProgressBar.setVisibility(Component.HIDE);
                    click_ok.setPixelMap(null);
                }
            }
        });

        setting_delete.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (count > 0) {
                    count--;
                }
                slider.setProgressValue(count);
                number.setText(count + "");
            }
        });

        setting_add.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (count < 100) {
                    count++;
                }
                slider.setProgressValue(count);
                number.setText(count + "");
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                count = i;
                number.setText(count + "");
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        start.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //选中状态
                component.setBackground(selectShapeElement);
                middle.setBackground(noSelectShapeElement);
                end.setBackground(noSelectShapeElement);

                preferences.putBoolean("start_click", true);
                preferences.putBoolean("middle_click", false);
                preferences.putBoolean("end_click", false);
            }
        });

        middle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //选中状态
                component.setBackground(selectShapeElement);
                start.setBackground(noSelectShapeElement);
                end.setBackground(noSelectShapeElement);
                preferences.putBoolean("middle_click", true);
                preferences.putBoolean("start_click", false);
                preferences.putBoolean("end_click", false);
            }
        });

        end.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //选中状态
                component.setBackground(selectShapeElement);
                start.setBackground(noSelectShapeElement);
                middle.setBackground(noSelectShapeElement);
                preferences.putBoolean("end_click", true);
                preferences.putBoolean("start_click", false);
                preferences.putBoolean("middle_click", false);
            }
        });

        radioContainer.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                preferences.putInt("radio_click", i);
            }
        });

    }

    /**
     * 创建过滤的数据Componet
     *
     * @param editText 过滤数据值
     */
    private void creatLayout(String editText) {

        DependentLayout dependentLayout = new DependentLayout(this);
        dependentLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        dependentLayout.setWidth(900);
        dependentLayout.setMarginTop(20);
        dependentLayout.setMarginBottom(30);


        Image image1 = new Image(this);
        image1.setPixelMap(ResourceTable.Media_setting_table);
        image1.setWidth(80);
        image1.setHeight(80);
        image1.setMarginLeft(50);

        Image image2 = new Image(this);
        image2.setPixelMap(ResourceTable.Media_remove);
        image2.setHeight(100);
        image2.setWidth(100);
        image2.setMarginLeft(860);
        image2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                search_filter.remove(editText);
                LogUtil.error(TAG, "search_filter = " + search_filter.toString());
                add_tableName.removeComponent(dependentLayout);
            }
        });

        dependentLayout.addComponent(image1);
        dependentLayout.addComponent(image2);

        Text text = new Text(this);
        text.setWidth(800);
        text.setTextSize(50);
        text.setText(editText);
        text.setMultipleLine(true);
        text.setMarginTop(10);
        text.setMarginLeft(150);
        dependentLayout.addComponent(text);

        Component component = new Component(this);
        component.setHeight(1);
        component.setWidth(1000);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(205, 201, 201));
        component.setBackground(shapeElement);
        component.setMarginLeft(50);
        component.setMarginTop(110);
        dependentLayout.addComponent(component);

        add_tableName.addComponent(dependentLayout);
    }


    private Set<String> search_filter;

    /**
     * 进行设置页面初始化设置根据Preferences
     */
    public void getSearchFilter() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        preferences = databaseHelper.getPreferences("setting_filter");
        search_filter = preferences.getStringSet("filter",new TreeSet<>());
        Object[] strs = search_filter.toArray();
        if (search_filter == null) {
            return;
        }
        //String[] strs = (String[]) search_filter.toArray();
        LogUtil.error("strs", " search = " + strs.length);
        if (add_tableName.getChildCount() == 0) {
            for (int i = 0; i < strs.length; i++) {
                creatLayout((String) strs[i]);
            }
        }

        number.setText(String.valueOf(preferences.getInt("stop", 100)));
        slider.setProgressValue(preferences.getInt("stop", 100));
        //判断是否以前点击过
        if (preferences.getBoolean("setting_ok", false)) {
            setting_ProgressBar.setVisibility(Component.VISIBLE);
            number.setVisibility(Component.VISIBLE);
            click_ok.setPixelMap(ResourceTable.Media_setting_ok);
            isClikc = true;
        } else {
            //隐藏
            isClikc = false;
            number.setVisibility(Component.HIDE);
            setting_ProgressBar.setVisibility(Component.HIDE);
            click_ok.setPixelMap(null);
        }
    }

    /**
     * 保存设置的值
     */
    public void setSearch_filter() {
        preferences.putStringSet("filter", search_filter);
        preferences.putInt("stop", Integer.parseInt(number.getText()));
        preferences.putBoolean("setting_ok", isClikc);
        preferences.flush();
    }

    @Override
    protected void onBackground() {
        setting_bar.removeComponent(settingBar);
        setSearch_filter();
        tableNameInputLayout.setHint("Add  table name");
        textFieldUp.setVisibility(Component.HIDE);
        tableNameInputLayout.setText("");
        super.onBackground();
    }
}
