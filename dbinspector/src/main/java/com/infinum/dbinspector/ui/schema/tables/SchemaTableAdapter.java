package com.infinum.dbinspector.ui.schema.tables;

import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.Utils.Function;
import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.shared.models.Cell;
import com.infinum.dbinspector.ui.schema.shared.SchemaViewHolder;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SchemaTableAdapter extends RecycleItemProvider {

    private SchemaViewHolder schemaViewHolder;

    private List<Cell> list = new ArrayList<>();

    private Function<String> onClick;

    private Context mContext;

    private String TAG = SchemaTableAdapter.class.getSimpleName();

    private List<Cell> copyList = new ArrayList<>();

    public SchemaTableAdapter(RdbStore rdbStore, Function<String> onClick, Context context) {
        mContext = context;
        getSearchFilter();

        String filter = "";
        if (search_filter != null && search_filter.size() > 0) {
            Object[] strs = search_filter.toArray();
            filter += "and ";
            for (int i = 0; i < strs.length; i++) {
                if (i == strs.length - 1) {
                    filter += "name not like '%" + strs[i] + "%'";
                } else {
                    filter += "name not like '%" + strs[i] + "%' and ";
                }
            }
            LogUtil.error(TAG, "filter = " + filter);
        }
        ResultSet resultSet = rdbStore.querySql("select name from sqlite_master where type='table'" + filter + " order by name", null);
        while (resultSet.goToNextRow()) {
            String userName = resultSet.getString(0);
            list.add(new Cell(userName));
        }
        copyList = list;
        this.onClick = onClick;

    }

    public void search(RdbStore rdbStore, String query) {
        if (query.equals("")) {
            list = copyList;
        } else {
            List<Cell> list1 = new ArrayList<>();
            for (int i = 0; i < copyList.size(); i++) {
                if (copyList.get(i).getText().contains(query)) {
                    list1.add(copyList.get(i));
                }
            }
            list = list1;
        }
        notifyDataChanged();
    }

    private Set<String> search_filter;

    public void getSearchFilter() {
        DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        Preferences preferences = databaseHelper.getPreferences("setting_filter");
        if (preferences != null) search_filter = preferences.getStringSet("filter", new HashSet<>());
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        LogUtil.error("TAG", "table适配器调用");
        schemaViewHolder = new SchemaViewHolder();
        component = schemaViewHolder.bind(list.get(i), onClick);
        return component;
    }

    public int add(String s) {
        List<Cell> newList = new ArrayList<>();
        //遍历适配器数据
        for (int i = 0; i < list.size(); i++) {
            if (s.equals("")) {
                LogUtil.error("搜索为空", "+1");
                newList.add(list.get(i));
            } else {
                //搜索栏不会空,搜索到数据时就把数据显示出来
                if (list.get(i).getText().contains(s)) {
                    newList.add(list.get(i));
                }
            }
        }
        list = newList;
        notifyDataChanged();
        LogUtil.error("SchemaAdapter", list.size() + "");
        return list.size();
    }

    public Cell getList(int i) {
        return list.get(i);
    }
}
