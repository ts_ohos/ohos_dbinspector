package com.infinum.dbinspector.domain.connection;

import com.infinum.dbinspector.domain.Control;
import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.Interactors;
import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import ohos.data.rdb.RdbStore;

public class ConnectionRepository implements Repositories.Connection {
    private Interactors.OpenConnection openInteractor;
    private Interactors.CloseConnection closeInteractor;
    private Control.Connection control;


    public ConnectionRepository(Interactors.OpenConnection openInteractor, Control.Connection control) {
        this.openInteractor = openInteractor;
        this.control = control;
    }

    public ConnectionRepository(Interactors.CloseConnection closeInteractor, Control.Connection control) {
        this.closeInteractor = closeInteractor;
        this.control = control;
    }


    @Override
    public DatabaseConnection open(ConnectionParameters input) {
        RdbStore rdbStore = openInteractor.invoke(control.getConverter().invoke(input));
        DatabaseConnection databaseConnection = control.getMapper().invoke(rdbStore);
        return databaseConnection;
    }

    @Override
    public Void close(ConnectionParameters input) {
        closeInteractor.invoke(control.getConverter().invoke(input));
        return null;
    }


}
