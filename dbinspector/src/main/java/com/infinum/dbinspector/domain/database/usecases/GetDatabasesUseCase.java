package com.infinum.dbinspector.domain.database.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;

import java.util.List;

public class GetDatabasesUseCase implements UseCases.GetDatabases {

    private Repositories.Database databaseRepository;
    private Repositories.Connection connectionRepository;
    private Repositories.Pragma pragmaRepository;

    public GetDatabasesUseCase(Repositories.Database databaseRepository) {
        this.databaseRepository = databaseRepository;
    }

    @Override
    public List<DatabaseDescriptor> invoke(DatabaseParameters.Get input) {
        return databaseRepository.getPage(input);
    }
}
