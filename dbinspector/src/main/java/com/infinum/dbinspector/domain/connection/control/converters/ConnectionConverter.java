package com.infinum.dbinspector.domain.connection.control.converters;

import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;

public class ConnectionConverter implements Converters.Connection {

    @Override
    public String invoke(ConnectionParameters parameters) {
        return parameters.getDatabasePath();
    }
}
