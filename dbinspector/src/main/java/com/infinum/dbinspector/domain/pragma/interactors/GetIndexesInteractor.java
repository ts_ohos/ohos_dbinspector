package com.infinum.dbinspector.domain.pragma.interactors;

import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.data.model.local.cursor.input.Query;
import com.infinum.dbinspector.data.model.local.cursor.output.QueryResult;
import com.infinum.dbinspector.domain.Interactors;

public class GetIndexesInteractor implements Interactors.GetIndexes {
    private Sources.Local.Pragma source;

    @Override
    public QueryResult invoke(Query input) {
        return source.getIndexes(input);
    }
}
