package com.infinum.dbinspector.domain.connection.usecase;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import ohos.data.rdb.RdbStore;

public class OpenConnectionUseCase implements UseCases.OpenConnection {

    private  Repositories.Connection connectionRepository;

    public OpenConnectionUseCase(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    @Override
    public RdbStore invoke(ConnectionParameters input) {
        return  connectionRepository.open(input).getDatabase();
    }
}
