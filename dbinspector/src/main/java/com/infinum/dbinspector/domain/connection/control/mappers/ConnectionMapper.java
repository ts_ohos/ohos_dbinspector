package com.infinum.dbinspector.domain.connection.control.mappers;

import com.infinum.dbinspector.domain.Mappers;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import ohos.data.rdb.RdbStore;

public class ConnectionMapper implements Mappers.Connection {

    @Override
    public DatabaseConnection invoke(RdbStore model) {
        return new DatabaseConnection(model);
    }
}
