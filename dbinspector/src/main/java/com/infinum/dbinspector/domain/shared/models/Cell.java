package com.infinum.dbinspector.domain.shared.models;

import com.infinum.dbinspector.domain.models.TruncateMode;
import com.infinum.dbinspector.domain.schema.shared.models.exceptions.ImageType;

import java.nio.ByteBuffer;

public class Cell {
    private String text = null;
    private byte[] data = null;
    private ImageType imageType = ImageType.UNSUPPORTED;
    private int linesShown = Integer.MAX_VALUE;
    private TruncateMode truncateMode = TruncateMode.END;

    public Cell() {
    }

    public Cell(String text) {
        this.text = text;
    }

    public Cell(String text, byte[] data, ImageType imageType, int linesShown, TruncateMode truncateMode) {
        this.text = text;
        this.data = data;
        this.imageType = imageType;
        this.linesShown = linesShown;
        this.truncateMode = truncateMode;
    }

    /*override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cell

        if (text != other.text) return false
        if (data != null) {
            if (other.data == null) return false
            if (!data.contentEquals(other.data)) return false
        } else if (other.data != null) return false
        if (imageType != other.imageType) return false
        if (linesShown != other.linesShown) return false
        if (truncateMode != other.truncateMode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = text?.hashCode() ?: 0
        result = 31 * result + (data?.contentHashCode() ?: 0)
        result = 31 * result + imageType.hashCode()
        result = 31 * result + linesShown
        result = 31 * result + truncateMode.hashCode()
        return result
    }*/

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public int getLinesShown() {
        return linesShown;
    }

    public void setLinesShown(int linesShown) {
        this.linesShown = linesShown;
    }

    public TruncateMode getTruncateMode() {
        return truncateMode;
    }

    public void setTruncateMode(TruncateMode truncateMode) {
        this.truncateMode = truncateMode;
    }
}
