package com.infinum.dbinspector.domain.shared.models.dsl.conditions;

import com.infinum.dbinspector.domain.shared.models.dsl.shared.CompositeCondition;

public class Or extends CompositeCondition {

    public Or(Void sqlOperator) {
        super("OR");
    }
}
