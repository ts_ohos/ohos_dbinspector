package com.infinum.dbinspector.domain.shared.models.dsl.conditions;

import com.infinum.dbinspector.domain.shared.models.dsl.shared.Condition;

public class Like extends Condition {
    private String column;
    private Object value;

    public Like(String column) {
        this.column = column;
    }

    public Like(String column, Object value) {
        this.column = column;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    {
        if (value != null && !(value instanceof Number) && !(value instanceof String)) {
            throw new IllegalArgumentException("Only null, number and string values can be used in the 'WHERE' clause");
        }
    }
    @Override
    protected void addCondition(Condition condition) {
        throw new IllegalStateException("Can't add a nested condition to 'like'");
    }

    @Override
    public String toString() {
        return this.column + " LIKE \"%%" + this.value + "%%\"";
    }
}
