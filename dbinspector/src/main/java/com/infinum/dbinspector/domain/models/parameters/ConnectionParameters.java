package com.infinum.dbinspector.domain.models.parameters;

import com.infinum.dbinspector.domain.shared.base.BaseParameters;

public class ConnectionParameters implements BaseParameters {
    private String databasePath;

    public ConnectionParameters(String databasePath) {
        this.databasePath = databasePath;
    }

    public String getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath = databasePath;
    }
}
