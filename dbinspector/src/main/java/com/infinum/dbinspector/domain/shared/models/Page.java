package com.infinum.dbinspector.domain.shared.models;

import java.util.List;

public class Page {

   /* val nextPage: Int? = null,*/
    private int nextPage ;
    private int beforeCount = 0;
    private int afterCount = 0;
    private List<Cell> cells;

    public Page(int nextPage, List<Cell> cells) {
        this.nextPage = nextPage;
        this.cells = cells;
    }

    public Page(int nextPage, int beforeCount, int afterCount, List<Cell> cells) {
        this.nextPage = nextPage;
        this.beforeCount = beforeCount;
        this.afterCount = afterCount;
        this.cells = cells;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getBeforeCount() {
        return beforeCount;
    }

    public void setBeforeCount(int beforeCount) {
        this.beforeCount = beforeCount;
    }

    public int getAfterCount() {
        return afterCount;
    }

    public void setAfterCount(int afterCount) {
        this.afterCount = afterCount;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }
}
