package com.infinum.dbinspector.domain.database;

import com.infinum.dbinspector.Utils.LogUtil;
import com.infinum.dbinspector.domain.Control;
import com.infinum.dbinspector.domain.Interactors;
import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import ohos.agp.components.ListContainer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DatabaseRepository implements Repositories.Database {
    private static final String TAG = "DatabaseRepository";

    private Interactors.GetDatabases getPageInteractor;
    private Interactors.ImportDatabases importInteractor;
    private Interactors.RemoveDatabase removeInteractor;
    private Interactors.RenameDatabase renameInteractor;
    private Interactors.CopyDatabase copyInteractor;
    private Control.Database control;

    private List<DatabaseDescriptor> list = new ArrayList<>();

    public DatabaseRepository(Interactors.GetDatabases getPageInteractor, Control.Database control) {
        this.getPageInteractor = getPageInteractor;
        this.control = control;
    }

    public DatabaseRepository(Interactors.RemoveDatabase removeInteractor, Control.Database control) {
        this.removeInteractor = removeInteractor;
        this.control = control;
    }

    public DatabaseRepository(Interactors.RenameDatabase renameInteractor, Control.Database control) {
        this.renameInteractor = renameInteractor;
        this.control = control;
    }

    public DatabaseRepository(Interactors.CopyDatabase copyInteractor, Control.Database control) {
        this.copyInteractor = copyInteractor;
        this.control = control;
    }

    @Override
    public List<DatabaseDescriptor> import1(DatabaseParameters.Import input) {
        return null;
    }

    @Override
    public List<DatabaseDescriptor> rename(DatabaseParameters.Rename input) {
        List<DatabaseDescriptor> list = new ArrayList<>();
        for (File file : renameInteractor.invoke(control.getConverter().rename(input))){
            list.add(control.getMapper().invoke(file));
            LogUtil.error(TAG,list.size()+"");
        }
        return list;
    }

    @Override
    public List<DatabaseDescriptor> remove(DatabaseParameters.Command input) {
        List<DatabaseDescriptor> list = new ArrayList<>();
        //转换为listFile
        for (File file : removeInteractor.invoke(control.getConverter().command(input))) {
           list.add(control.getMapper().invoke(file));
           LogUtil.error(TAG,list.size()+"");
        }
        return list;
    }

    @Override
    public List<DatabaseDescriptor> copy(DatabaseParameters.Copy input) {
        List<DatabaseDescriptor> list = new ArrayList<>();
        //转换为listFile
        for (File file : copyInteractor.invoke(control.getConverter().copy(input))) {
            list.add(control.getMapper().invoke(file));
            LogUtil.error(TAG,list.size()+"");
        }
        return list;
    }

    @Override
    public List<DatabaseDescriptor> getPage(DatabaseParameters.Get input) {

        //Operation
        for (File file : getPageInteractor.invoke(control.getConverter().get(input))) {
            list.add(control.getMapper().invoke(file));
        }
        /*for (File file : getPageInteractor.invoke(control.getConverter().get(input))){
              list.add((DatabaseDescriptor) control.getMapper().invoke(file));
        }*/
        LogUtil.error(TAG, list.size() + "");
        return list;
    }
}
