package com.infinum.dbinspector.domain.shared.models;

import com.infinum.dbinspector.ResourceTable;

public enum  Sort {
    ASCENDING("ASC", ResourceTable.Media_ASC),
    DESCENDING("DESC", ResourceTable.Media_DESC);

    private String rawValue;
    private int icon;

    private Sort(String rawValue, int icon) {
        this.rawValue = rawValue;
        this.icon = icon;
    }


    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
