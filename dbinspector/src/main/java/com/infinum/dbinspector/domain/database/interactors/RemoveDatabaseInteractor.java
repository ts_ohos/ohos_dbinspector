package com.infinum.dbinspector.domain.database.interactors;

import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.domain.Interactors;
import com.infinum.dbinspector.domain.database.models.Operation;

import java.io.File;
import java.util.List;

public class RemoveDatabaseInteractor implements Interactors.RemoveDatabase {
    private Sources.Raw source;

    public RemoveDatabaseInteractor(Sources.Raw source) {
        this.source = source;
    }

    @Override
    public List<File> invoke(Operation input) {
        return  source.removeDatabase(input);
    }
}
