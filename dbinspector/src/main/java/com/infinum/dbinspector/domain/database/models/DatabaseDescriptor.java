package com.infinum.dbinspector.domain.database.models;

public class DatabaseDescriptor {
    private Boolean exists;
    private String parentPath;
    private String name;
    private String extension = "";
    private String version = "";

    public DatabaseDescriptor(Boolean exists, String parentPath, String name) {
        this.exists = exists;
        this.parentPath = parentPath;
        this.name = name;
    }

    public DatabaseDescriptor(Boolean exists, String parentPath, String name, String extension) {
        this.exists = exists;
        this.parentPath = parentPath;
        this.name = name;
        this.extension = extension;
    }

    public DatabaseDescriptor(Boolean exists, String parentPath, String name, String extension, String version) {
        this.exists = exists;
        this.parentPath = parentPath;
        this.name = name;
        this.extension = extension;
        this.version = version;
    }

    private String absolutePath;

    public Boolean getExists() {
        return exists;
    }

    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAbsolutePath() {
        if (extension.isEmpty()) {
            return parentPath + "/" + name;
        } else {
            return parentPath + "/" + name + extension;
        }
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }
}
