package com.infinum.dbinspector.domain.shared.models.parameters;

import com.infinum.dbinspector.domain.shared.models.Sort;

public class SortParameters {
    private Sort sort = Sort.ASCENDING;

    public SortParameters() {
    }

    public SortParameters(Sort sort) {
        this.sort = sort;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
