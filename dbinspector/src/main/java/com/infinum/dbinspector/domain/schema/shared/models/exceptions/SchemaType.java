package com.infinum.dbinspector.domain.schema.shared.models.exceptions;

import com.infinum.dbinspector.ResourceTable;

public enum  SchemaType {
    TABLES(ResourceTable.String_dbinspector_schema_tables),
    VIEWS(ResourceTable.String_dbinspector_schema_views),
    TRIGGERS(ResourceTable.String_dbinspector_schema_triggers);

    private int nameRes;

    SchemaType(int nameRes) {
        this.nameRes = nameRes;
    }
}
