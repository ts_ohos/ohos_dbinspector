package com.infinum.dbinspector.domain.pragma.models;


import com.infinum.dbinspector.ResourceTable;

public enum PragmaType {
    TABLE_INFO(ResourceTable.String_dbinspector_pragma_table_info),
    FOREIGN_KEY(ResourceTable.String_dbinspector_pragma_foreign_key),
    INDEX(ResourceTable.String_dbinspector_pragma_index);

    int nameRes;

    PragmaType(int nameRes) {
        this.nameRes = nameRes;
    }
}
