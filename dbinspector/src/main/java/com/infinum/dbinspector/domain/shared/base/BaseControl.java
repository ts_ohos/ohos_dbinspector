package com.infinum.dbinspector.domain.shared.base;

import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.Mappers;

import java.io.File;
import java.util.Map;

public interface BaseControl<Mapper, Converter>{
    Mapper getMapper();
    Converter getConverter();

}
