package com.infinum.dbinspector.domain.shared.models.dsl;


public class pragma {

    private String pragmaName;
    private String pragmaValue;

    public String getPragmaName() {
        return pragmaName;
    }

    public void setPragmaName(String pragmaName) {
        this.pragmaName = pragmaName;
    }

    public String getPragmaValue() {
        return pragmaValue;
    }

    public void setPragmaValue(String pragmaValue) {
        this.pragmaValue = pragmaValue;
    }
}
