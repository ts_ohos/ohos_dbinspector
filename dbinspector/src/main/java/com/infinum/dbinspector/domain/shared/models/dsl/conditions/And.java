package com.infinum.dbinspector.domain.shared.models.dsl.conditions;

import com.infinum.dbinspector.domain.shared.models.dsl.shared.CompositeCondition;

import java.util.function.Function;

public class And extends CompositeCondition {

    public And(Void sqlOperator) {
        super("AND");
    }
}
