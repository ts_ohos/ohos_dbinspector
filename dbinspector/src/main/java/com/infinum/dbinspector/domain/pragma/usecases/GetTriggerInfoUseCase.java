package com.infinum.dbinspector.domain.pragma.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;

public class GetTriggerInfoUseCase implements UseCases.GetTriggerInfo {
    private Repositories.Pragma pragmaRepository;

    public GetTriggerInfoUseCase(Repositories.Pragma pragmaRepository) {
        this.pragmaRepository = pragmaRepository;
    }

    public Repositories.Pragma getPragmaRepository() {
        return pragmaRepository;
    }

    public void setPragmaRepository(Repositories.Pragma pragmaRepository) {
        this.pragmaRepository = pragmaRepository;
    }

    @Override
    public Page invoke(PragmaParameters.Info input) throws CloneNotSupportedException {
        Void Unit = null;
       return pragmaRepository.getTriggerInfo(Unit);
    }
}
