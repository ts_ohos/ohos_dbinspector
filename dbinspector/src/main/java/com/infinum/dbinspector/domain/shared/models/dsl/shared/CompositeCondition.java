package com.infinum.dbinspector.domain.shared.models.dsl.shared;

import java.util.ArrayList;
import java.util.List;

public class CompositeCondition extends Condition {

    private String sqlOperator;

    public CompositeCondition(String sqlOperator) {
        this.sqlOperator = sqlOperator;
    }

    public String getSqlOperator() {
        return sqlOperator;
    }

    public void setSqlOperator(String sqlOperator) {
        this.sqlOperator = sqlOperator;
    }

    private List<Condition> conditions = new ArrayList<>();

    @Override
    protected void addCondition(Condition condition) {
        conditions.add(condition);
    }

    @Override
    public String toString() {
        List<String> count = new ArrayList<>();
       if (conditions.size() == 1){
         return  conditions.get(0).toString();
       }else {

         for (int i = 0; i<conditions.size();i++){
              count.add(i, "("+conditions.get(i)+")"+sqlOperator);
         }
          return count.toString();
       }
    }
}
