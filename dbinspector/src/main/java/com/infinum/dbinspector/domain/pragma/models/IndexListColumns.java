package com.infinum.dbinspector.domain.pragma.models;

public enum IndexListColumns {
    SEQ,
    NAME,
    UNIQUE;
}
