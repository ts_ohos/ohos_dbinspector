package com.infinum.dbinspector.domain;

import ohos.bundle.ModuleInfo;

import java.util.List;

public class Domain {
    private Domain() {
    }

    private static Domain INSTANCE = new Domain();

    public static Domain getInstance() {
        return INSTANCE;
    }

    public static class Constants {
        public static class Limits {
            public static final int PAGE_SIZE = 100;
            public static final int INITIAL_PAGE = 1;
        }

        public static class Settings {
            public static final int LINES_LIMIT_MAXIMUM = 100;
        }
    }

    public static class Qualifiers {
        public static String TABLES = "domain.qualifiers.tables";
        public static String VIEW = "domain.qualifiers.views";
        public static String TRIGGERS = "domain.qualifiers.triggers";
    }

}
