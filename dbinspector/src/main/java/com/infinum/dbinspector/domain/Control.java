package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.domain.shared.base.BaseControl;

public interface Control {
    interface Database extends BaseControl<Mappers.DatabaseDescriptorModel, Converters.Database> {
        
    }

    interface Connection extends BaseControl<Mappers.Connection, Converters.Connection> {
    }

    interface Schema extends BaseControl<Mappers.Schema, Converters.Schema> {
    }

    interface Pragma extends BaseControl<Mappers.Pragma, Converters.Pragma> {
    }

    interface RawQuery extends BaseControl<Mappers.RawQuery, Converters.RawQuery> {
    }

   /* interface Settings extends BaseControl<Mappers.SettingsModel, Converters.Settings> {
    }*/

}
