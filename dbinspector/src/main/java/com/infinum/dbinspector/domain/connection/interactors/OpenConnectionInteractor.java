package com.infinum.dbinspector.domain.connection.interactors;

import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.domain.Interactors;
import ohos.data.rdb.RdbStore;

public class OpenConnectionInteractor implements Interactors.OpenConnection {

    private Sources.Memory source;

    public OpenConnectionInteractor(Sources.Memory source) {
        this.source = source;
    }

    @Override
    public RdbStore invoke(String input) {
        return  source.openConnection(input);
    }
}
