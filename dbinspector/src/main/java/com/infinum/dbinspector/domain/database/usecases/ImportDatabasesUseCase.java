package com.infinum.dbinspector.domain.database.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;

import java.util.List;

public class ImportDatabasesUseCase implements UseCases.ImportDatabases {

    private Repositories.Database  databaseRepository;

    @Override
    public List<DatabaseDescriptor> invoke(DatabaseParameters.Import input) {
        return databaseRepository.import1(input);
    }
}
