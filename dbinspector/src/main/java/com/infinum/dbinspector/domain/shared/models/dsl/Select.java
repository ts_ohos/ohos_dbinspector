package com.infinum.dbinspector.domain.shared.models.dsl;

import com.infinum.dbinspector.domain.shared.models.Sort;
import com.infinum.dbinspector.domain.shared.models.dsl.conditions.And;
import com.infinum.dbinspector.domain.shared.models.dsl.shared.Condition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Select {

    private List<String> columns = new ArrayList<>();
    private String table;
    private Condition condition;
    private List<String> orderByColumns = new ArrayList<>();
    private Sort orderByDirection = Sort.ASCENDING;
    private int limit;

    /**
     * get set方法
     *
     * @return
     */
    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public List<String> getOrderByColumns() {
        return orderByColumns;
    }

    public void setOrderByColumns(List<String> orderByColumns) {
        this.orderByColumns = orderByColumns;
    }

    public Sort getOrderByDirection() {
        return orderByDirection;
    }

    public void setOrderByDirection(Sort orderByDirection) {
        this.orderByDirection = orderByDirection;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }


    /**
     * 具体操作
     *
     * @param column
     */
    public void columns(String column) {
        columns.add(column);
    }


    public void from(String table) {
        setTable(table);
    }

    public void where(Condition condition) {
        setCondition(condition);
    }

    public void orderBy(Sort sort, String columns) {
        this.orderByDirection = sort;
        this.orderByColumns = Collections.singletonList(columns);
    }


    @Override
    public String toString() {
        String queryColumns = null;
        if (columns.isEmpty()) {
            queryColumns = "*";
        } else {
            for (int i = 0; i < columns.size(); i++) {
                String old = columns.get(i);
                columns.set(i, old + ", ");
            }
            queryColumns = columns.toString();
        }
        String condition = "WHER " + getCondition();

        String orderBy = null;
        if (!orderByColumns.isEmpty()) {
            for (int i = 0; i < orderByColumns.size(); i++) {
                String old = orderByColumns.get(i);
                orderByColumns.set(i, "ORDER BY " + old + ", ");
            }
            orderBy = orderByColumns.toString() + orderByDirection.getRawValue();
        } else {
            orderBy = "";
        }

        String limit = "LIMIT " + getLimit();

        return "SELECT " + queryColumns + "From " + getTable() + " " + condition + " " + orderBy + " " + limit;
    }
}
