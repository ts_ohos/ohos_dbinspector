package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.data.model.local.cursor.input.Query;
import com.infinum.dbinspector.data.model.local.cursor.output.QueryResult;
import com.infinum.dbinspector.data.model.local.proto.input.SettingsTask;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.database.models.Operation;
import com.infinum.dbinspector.domain.shared.base.BaseInteractor;
import ohos.data.rdb.RdbStore;


import java.io.File;
import java.util.List;

public interface Interactors {

    // region Database
    interface GetDatabases extends BaseInteractor<Operation, List<File>> {
    }

    interface ImportDatabases extends BaseInteractor<Operation, List<File>> {
    }

    interface RemoveDatabase extends BaseInteractor<Operation, List<File>> {
    }

    interface RenameDatabase extends BaseInteractor<Operation, List<File>> {
    }

    interface CopyDatabase extends BaseInteractor<Operation, List<File>> {
    }
    // endregion

    // region Connection
    interface OpenConnection extends BaseInteractor<String, RdbStore> {
    }

    interface CloseConnection extends BaseInteractor<String, Void> {
    }
    // endregion

    //region Settings
  /*  interface GetSettings extends BaseInteractor<SettingsTask, SettingsEntity> {
    }
*/
    interface SaveLinesLimit extends BaseInteractor<SettingsTask, Void> {
    }

    interface SaveLinesCount extends BaseInteractor<SettingsTask, Void> {
    }

    interface SaveTruncateMode extends BaseInteractor<SettingsTask, Void> {
    }

    interface SaveBlobPreviewMode extends BaseInteractor<SettingsTask, Void> {
    }

    interface SaveIgnoredTableName extends BaseInteractor<SettingsTask, Void> {
    }

    interface RemoveIgnoredTableName extends BaseInteractor<SettingsTask, Void> {
    }
    // endregion

    // region Schema
    interface GetTables extends BaseInteractor<Query, QueryResult> {
    }

    interface GetTableByName extends BaseInteractor<Query, QueryResult> {

    }

    interface DropTableContentByName extends BaseInteractor<Query, QueryResult> {

    }

    interface GetTriggers extends BaseInteractor<Query, QueryResult> {

    }

    interface GetTriggerByName extends BaseInteractor<Query, QueryResult> {

    }

    interface DropTriggerByName extends BaseInteractor<Query, QueryResult> {

    }

    interface GetViews extends BaseInteractor<Query, QueryResult> {

    }

    interface GetViewByName extends BaseInteractor<Query, QueryResult> {

    }

    interface DropViewByName extends BaseInteractor<Query, QueryResult> {

    }

    interface GetRawQueryHeaders extends BaseInteractor<Query, QueryResult> {
    }

    interface GetRawQuery extends BaseInteractor<Query, QueryResult> {
    }

    interface GetAffectedRows extends BaseInteractor<Query, QueryResult> {
    }
    // endregion

    // region Pragma
    interface GetUserVersion extends BaseInteractor<Query, QueryResult> {
    }

    interface GetTableInfo extends BaseInteractor<Query, QueryResult> {
    }

    interface GetForeignKeys extends BaseInteractor<Query, QueryResult> {
    }

    interface GetIndexes extends BaseInteractor<Query, QueryResult> {
    }

}
