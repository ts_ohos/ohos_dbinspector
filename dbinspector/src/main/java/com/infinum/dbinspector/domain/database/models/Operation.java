package com.infinum.dbinspector.domain.database.models;

import ohos.app.Context;
import ohos.utils.net.Uri;

import java.util.Collections;
import java.util.List;

public class Operation {
    private Context context;
    private DatabaseDescriptor databaseDescriptor =  null;
    private List<Uri> importUri = Collections.emptyList();
    private String argument = null;
    private List<Integer> list ;

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public Operation(Context context, String argument) {
        this.context = context;
        this.argument = argument;
    }

    public Operation(Context context, List<Uri> importUri) {
        this.context = context;
        this.importUri = importUri;
    }

    public Operation(Context context, DatabaseDescriptor databaseDescriptor) {
        this.context = context;
        this.databaseDescriptor = databaseDescriptor;
    }

    public Operation(Context context, DatabaseDescriptor databaseDescriptor, List<Integer> list) {
        this.context = context;
        this.databaseDescriptor = databaseDescriptor;
        this.list = list;
    }

    public Operation(Context context, DatabaseDescriptor databaseDescriptor, String argument) {
        this.context = context;
        this.databaseDescriptor = databaseDescriptor;
        this.argument = argument;
    }

    public Operation(Context context, DatabaseDescriptor databaseDescriptor, List<Uri> importUri, String argument) {
        this.context = context;
        this.databaseDescriptor = databaseDescriptor;
        this.importUri = importUri;
        this.argument = argument;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public DatabaseDescriptor getDatabaseDescriptor() {
        return databaseDescriptor;
    }

    public void setDatabaseDescriptor(DatabaseDescriptor databaseDescriptor) {
        this.databaseDescriptor = databaseDescriptor;
    }

    public List<Uri> getImportUri() {
        return importUri;
    }

    public void setImportUri(List<Uri> importUri) {
        this.importUri = importUri;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }
}
