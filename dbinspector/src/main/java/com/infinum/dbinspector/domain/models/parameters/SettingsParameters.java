package com.infinum.dbinspector.domain.models.parameters;

import com.infinum.dbinspector.domain.models.BlobPreviewMode;
import com.infinum.dbinspector.domain.models.TruncateMode;
import com.infinum.dbinspector.domain.shared.base.BaseParameters;

public class SettingsParameters implements BaseParameters {
    public class Get extends SettingsParameters {
    }

    public class LinesLimit extends SettingsParameters {
        private Boolean isEnable;

        public LinesLimit(Boolean isEnable) {
            this.isEnable = isEnable;
        }

        public Boolean getEnable() {
            return isEnable;
        }

        public void setEnable(Boolean enable) {
            isEnable = enable;
        }
    }

    public class LinesCount extends SettingsParameters {
        private int count;

        public LinesCount(int count) {
            this.count = count;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }

    public class Truncate extends SettingsParameters {
        private TruncateMode mode;

        public Truncate(TruncateMode mode) {
            this.mode = mode;
        }

        public TruncateMode getMode() {
            return mode;
        }

        public void setMode(TruncateMode mode) {
            this.mode = mode;
        }
    }

    public class BlobPreview extends SettingsParameters {
        private BlobPreviewMode mode;

        public BlobPreview(BlobPreviewMode mode) {
            this.mode = mode;
        }

        public BlobPreviewMode getMode() {
            return mode;
        }

        public void setMode(BlobPreviewMode mode) {
            this.mode = mode;
        }
    }

    public class IgnoredTableName extends SettingsParameters {
        private String name;

        public IgnoredTableName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


}
