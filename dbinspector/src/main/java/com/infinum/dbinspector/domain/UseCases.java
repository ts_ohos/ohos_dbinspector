package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import com.infinum.dbinspector.domain.models.parameters.SettingsParameters;
import com.infinum.dbinspector.domain.settings.models.Settings;
import com.infinum.dbinspector.domain.shared.base.BaseUseCase;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.ContentParameters;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;
import ohos.data.rdb.RdbStore;

import java.util.List;

public interface UseCases {
    //region database
    interface GetDatabases extends BaseUseCase<DatabaseParameters.Get, List<DatabaseDescriptor>> {
    }

    interface ImportDatabases extends BaseUseCase<DatabaseParameters.Import, List<DatabaseDescriptor>> {
    }

    interface RemoveDatabase extends BaseUseCase<DatabaseParameters.Command, List<DatabaseDescriptor>> {
    }

    interface RenameDatabase extends BaseUseCase<DatabaseParameters.Rename, List<DatabaseDescriptor>> {
    }

    interface CopyDatabase extends BaseUseCase<DatabaseParameters.Copy, List<DatabaseDescriptor>> {
    }
    // endregion

    // region Connection
    interface OpenConnection extends BaseUseCase<ConnectionParameters, RdbStore> {
    }

    interface CloseConnection extends BaseUseCase<ConnectionParameters, Void> {
    }

    //region Settings
    interface GetSettings extends BaseUseCase<SettingsParameters.Get, Settings> {
    }

    interface SaveIgnoredTableName extends BaseUseCase<SettingsParameters.IgnoredTableName, Void> {
    }

    interface RemoveIgnoredTableName extends BaseUseCase<SettingsParameters.IgnoredTableName, Void> {
    }

    interface ToggleLinesLimit extends BaseUseCase<SettingsParameters.LinesLimit, Void> {
    }

    interface SaveLinesCount extends BaseUseCase<SettingsParameters.LinesCount, Void> {
    }

    interface SaveTruncateMode extends BaseUseCase<SettingsParameters.Truncate, Void> {
    }

    interface SaveBlobPreviewMode extends BaseUseCase<SettingsParameters.BlobPreview, Void> {
    }
    // endregion

    // region Schema
    interface GetTables extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetViews extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetTriggers extends BaseUseCase<ContentParameters, Page> {
    }
    // endregion

    // region Content
    interface GetTable extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetView extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetTrigger extends BaseUseCase<ContentParameters, Page> {
    }

    interface DropTableContent extends BaseUseCase<ContentParameters, Page> {
    }

    interface DropView extends BaseUseCase<ContentParameters, Page> {
    }

    interface DropTrigger extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetRawQueryHeaders extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetRawQuery extends BaseUseCase<ContentParameters, Page> {
    }

    interface GetAffectedRows extends BaseUseCase<ContentParameters, Page> {
    }
    // endregion

    // region Pragma
    interface GetTableInfo extends BaseUseCase<PragmaParameters.Info, Page> {
    }

    interface GetTriggerInfo extends BaseUseCase<PragmaParameters.Info, Page> {
    }

    interface GetTablePragma extends BaseUseCase<PragmaParameters.Info, Page> {
    }

    interface GetForeignKeys extends BaseUseCase<PragmaParameters.ForeignKeys, Page> {
    }

    interface GetIndexes extends BaseUseCase<PragmaParameters.Indexes, Page> {
    }

}
