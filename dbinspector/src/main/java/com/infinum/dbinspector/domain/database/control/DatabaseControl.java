package com.infinum.dbinspector.domain.database.control;

import com.infinum.dbinspector.domain.Control;
import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.Mappers;

import java.io.File;

public class DatabaseControl implements Control.Database {
    private Mappers.DatabaseDescriptorModel mapper;
    private Converters.Database converter;

    public DatabaseControl(Mappers.DatabaseDescriptorModel mapper, Converters.Database converter) {
        this.mapper = mapper;
        this.converter = converter;
    }

    @Override
    public Mappers.DatabaseDescriptorModel getMapper() {
        return mapper;
    }

    @Override
    public Converters.Database getConverter() {
        return converter;
    }


}
