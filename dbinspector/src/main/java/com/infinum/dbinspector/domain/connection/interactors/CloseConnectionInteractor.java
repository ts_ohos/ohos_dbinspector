package com.infinum.dbinspector.domain.connection.interactors;

import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.domain.Interactors;

public class CloseConnectionInteractor implements Interactors.CloseConnection {
    private Sources.Memory source;

    public CloseConnectionInteractor(Sources.Memory source) {
        this.source = source;
    }

    public Sources.Memory getSource() {
        return source;
    }

    public void setSource(Sources.Memory source) {
        this.source = source;
    }

    @Override
    public Void invoke(String input) {
        source.closeConnection(input);
        return null;
    }
}
