package com.infinum.dbinspector.domain.shared.base;

public interface BaseMapper<Input, Output> {
    Output invoke(Input model);
}
