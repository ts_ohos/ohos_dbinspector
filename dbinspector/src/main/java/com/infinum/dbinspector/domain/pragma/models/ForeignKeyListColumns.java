package com.infinum.dbinspector.domain.pragma.models;

public enum ForeignKeyListColumns {
    ID,
    SEQ,
    TABLE,
    FROM,
    TO,
    ON_UPDATE,
    ON_DELETE,
    MATCH;

}
