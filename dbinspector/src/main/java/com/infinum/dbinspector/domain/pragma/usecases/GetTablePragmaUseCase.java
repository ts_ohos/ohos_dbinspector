package com.infinum.dbinspector.domain.pragma.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;

public class GetTablePragmaUseCase implements UseCases.GetTablePragma {
    private Repositories.Connection connectionRepository;
    private Repositories.Pragma pragmaRepository;

    public GetTablePragmaUseCase(Repositories.Connection connectionRepository, Repositories.Pragma pragmaRepository) {
        this.connectionRepository = connectionRepository;
        this.pragmaRepository = pragmaRepository;
    }

    public Repositories.Connection getConnectionRepository() {
        return connectionRepository;
    }

    public void setConnectionRepository(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    public Repositories.Pragma getPragmaRepository() {
        return pragmaRepository;
    }

    public void setPragmaRepository(Repositories.Pragma pragmaRepository) {
        this.pragmaRepository = pragmaRepository;
    }

    @Override
    public Page invoke(PragmaParameters.Info input) throws CloneNotSupportedException {
        DatabaseConnection connection = connectionRepository.open(new ConnectionParameters(input.getDatabasePath()));
        PragmaParameters.Info input1 = (PragmaParameters.Info) input.clone();
        input1.setDatabase(connection.getDatabase());
        return pragmaRepository.getTableInfo(input1);
    }
}
