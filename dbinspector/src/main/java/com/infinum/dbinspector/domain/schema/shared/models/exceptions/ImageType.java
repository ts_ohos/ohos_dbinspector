package com.infinum.dbinspector.domain.schema.shared.models.exceptions;

public enum ImageType {
    UNSUPPORTED("", ""),
    JPG("FFD8FFE0", ".jpg"),
    PNG("89504E470D0A1A0A", ".png"),
    BMP("424D", ".bmp");

    private String header;
    private String suffix;

    ImageType(String header, String suffix) {
        this.header = header;
        this.suffix = suffix;
    }

    private ImageType invoke(String hexData){
        if (hexData.startsWith( JPG.header)){
            return JPG;
        }else if (hexData.startsWith(PNG.header)){
            return PNG;
        }else if (hexData.startsWith(BMP.header)){
            return BMP;
        }else {
            return UNSUPPORTED;
        }
    }


}
