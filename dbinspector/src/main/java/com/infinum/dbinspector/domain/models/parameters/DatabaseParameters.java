package com.infinum.dbinspector.domain.models.parameters;

import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.shared.base.BaseParameters;
import ohos.app.Context;
import ohos.utils.net.Uri;

import java.util.List;

public class DatabaseParameters implements BaseParameters {

    public static class Get extends DatabaseParameters {
        private Context context;
        private String argument;

        public Get(Context context, String argument) {
            this.context = context;
            this.argument = argument;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public String getArgument() {
            return argument;
        }

        public void setArgument(String argument) {
            this.argument = argument;
        }
    }

    public static class Import extends DatabaseParameters {
        private Context context;
        private List<Uri> importUris;

        public Import(Context context, List<Uri> importUris) {
            this.context = context;
            this.importUris = importUris;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public List<Uri> getImportUris() {
            return importUris;
        }

        public void setImportUris(List<Uri> importUris) {
            this.importUris = importUris;
        }
    }

    public static class Command extends DatabaseParameters {
        private Context context;
        private DatabaseDescriptor databaseDescriptor;

        public Command(Context context, DatabaseDescriptor databaseDescriptor) {
            this.context = context;
            this.databaseDescriptor = databaseDescriptor;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public DatabaseDescriptor getDatabaseDescriptor() {
            return databaseDescriptor;
        }

        public void setDatabaseDescriptor(DatabaseDescriptor databaseDescriptor) {
            this.databaseDescriptor = databaseDescriptor;
        }
    }

    public static class Rename extends DatabaseParameters {
        private Context context;
        private DatabaseDescriptor databaseDescriptor;
        private String argument;

        public Rename(Context context, DatabaseDescriptor databaseDescriptor, String argument) {
            this.context = context;
            this.databaseDescriptor = databaseDescriptor;
            this.argument = argument;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public DatabaseDescriptor getDatabaseDescriptor() {
            return databaseDescriptor;
        }

        public void setDatabaseDescriptor(DatabaseDescriptor databaseDescriptor) {
            this.databaseDescriptor = databaseDescriptor;
        }

        public String getArgument() {
            return argument;
        }

        public void setArgument(String argument) {
            this.argument = argument;
        }
    }

    public static class Copy extends DatabaseParameters {
        private Context context;
        private DatabaseDescriptor databaseDescriptor;

        public Copy(Context context, DatabaseDescriptor databaseDescriptor) {
            this.context = context;
            this.databaseDescriptor = databaseDescriptor;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public DatabaseDescriptor getDatabaseDescriptor() {
            return databaseDescriptor;
        }

        public void setDatabaseDescriptor(DatabaseDescriptor databaseDescriptor) {
            this.databaseDescriptor = databaseDescriptor;
        }

    }
}
