package com.infinum.dbinspector.domain.shared.base;

public interface BaseUseCase< InputModel, OutputModel> {
     OutputModel invoke(InputModel input) throws CloneNotSupportedException;
}


