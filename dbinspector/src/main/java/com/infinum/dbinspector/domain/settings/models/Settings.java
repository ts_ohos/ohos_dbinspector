package com.infinum.dbinspector.domain.settings.models;

import com.infinum.dbinspector.domain.models.BlobPreviewMode;
import com.infinum.dbinspector.domain.models.TruncateMode;

import java.util.Collections;
import java.util.List;

public class Settings {
    private Boolean linesLimitEnabled = false;
    private int linesCount = 100;
    private TruncateMode truncateMode = TruncateMode.END;
    private BlobPreviewMode blobPreviewMode = BlobPreviewMode.PLACEHOLDER;
    private List<String> ignoredTableNames = Collections.emptyList();

    public Settings() {
    }


    public Boolean getLinesLimitEnabled() {
        return linesLimitEnabled;
    }

    public void setLinesLimitEnabled(Boolean linesLimitEnabled) {
        this.linesLimitEnabled = linesLimitEnabled;
    }

    public int getLinesCount() {
        return linesCount;
    }

    public void setLinesCount(int linesCount) {
        this.linesCount = linesCount;
    }

    public TruncateMode getTruncateMode() {
        return truncateMode;
    }

    public void setTruncateMode(TruncateMode truncateMode) {
        this.truncateMode = truncateMode;
    }

    public BlobPreviewMode getBlobPreviewMode() {
        return blobPreviewMode;
    }

    public void setBlobPreviewMode(BlobPreviewMode blobPreviewMode) {
        this.blobPreviewMode = blobPreviewMode;
    }

    public List<String> getIgnoredTableNames() {
        return ignoredTableNames;
    }

    public void setIgnoredTableNames(List<String> ignoredTableNames) {
        this.ignoredTableNames = ignoredTableNames;
    }
}
