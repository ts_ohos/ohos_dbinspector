package com.infinum.dbinspector.domain.shared.models;

import com.infinum.dbinspector.domain.shared.models.dsl.Select;
import com.infinum.dbinspector.domain.shared.models.dsl.pragma;
import com.infinum.dbinspector.domain.shared.models.dsl.shared.Condition;

public class Statements {
    public static Sort sort = Sort.ASCENDING;

    private Statements() {
    }

    private static final Statements INSTANCE = new Statements();

    public static Statements getInStance() {
        return INSTANCE;
    }

    public static class Pragma {

        public static pragma userVersion() {
            pragma pragma = new pragma();
            pragma.setPragmaName("user_version");
            return pragma;
        }

        public static pragma tableInfo(String name) {
            pragma pragma = new pragma();
            pragma.setPragmaName("table_info");
            pragma.setPragmaValue(name);
            return pragma;
        }

        public static pragma foreignKeys(String name) {
            pragma pragma = new pragma();
            pragma.setPragmaName("foreign_key_list");
            pragma.setPragmaName(name);
            return pragma;
        }

        public static pragma indexes(String name) {
            pragma pragma = new pragma();
            pragma.setPragmaName("index_list");
            pragma.setPragmaValue(name);
            return pragma;
        }

    }

    public static class Schema {

        public static String tables(String query) {
            Select select = new Select();
            select.columns("name");
            select.from("sqlite_master");
            select.where(new Condition() {
                @Override
                protected void addCondition(Condition condition) {
                    condition.eq("type", "table");
                    condition.like("name", query);
                }
            });
            select.orderBy(sort, "name");
            return select.toString();
        }

        public static String views(String query) {
            Select select = new Select();
            select.columns("name");
            select.from("sqlite_master");
            select.where(new Condition() {
                @Override
                protected void addCondition(Condition condition) {
                    condition.eq("type", "view");
                    condition.like("name", query);
                }
            });
            select.orderBy(sort, "name");

            return select.toString();
        }

        public static String triggers(String query) {
            Select select = new Select();
            select.columns("name");
            select.from("sqlite_master");
            select.where(new Condition() {
                @Override
                protected void addCondition(Condition condition) {
                    condition.eq("type", "trigger");
                    condition.like("name", query);
                }
            });
            select.orderBy(sort, "name");

            return select.toString();
        }
    }


}
