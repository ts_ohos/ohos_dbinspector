package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.data.model.local.cursor.input.Order;
import com.infinum.dbinspector.data.model.local.proto.input.SettingsTask;
import com.infinum.dbinspector.domain.database.models.Operation;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import com.infinum.dbinspector.domain.models.parameters.SettingsParameters;
import com.infinum.dbinspector.domain.shared.base.BaseConverter;
import com.infinum.dbinspector.domain.shared.base.BaseParameters;
import com.infinum.dbinspector.domain.shared.models.parameters.ContentParameters;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;
import com.infinum.dbinspector.domain.shared.models.parameters.SortParameters;
import ohos.data.distributed.common.Query;

public interface Converters {
    interface Database {
        Operation get(DatabaseParameters.Get parameters);

        Operation import1(DatabaseParameters.Import parameters);

        Operation rename(DatabaseParameters.Rename parameters);

        Operation command(DatabaseParameters.Command parameters);

        Operation copy(DatabaseParameters.Copy parameters);
    }

    interface Connection extends BaseConverter<ConnectionParameters, String> {
    }

    interface Sort extends BaseConverter<SortParameters, Order> {
    }

    interface Schema {
        Query getPage(ContentParameters parameters);

        Query getByName(ContentParameters parameters);

        Query dropByName(ContentParameters parameters);
    }

    interface Pragma {
        Query version(PragmaParameters.Version parameters);

        Query info(PragmaParameters.Info parameters);

        Query foreignKeys(PragmaParameters.ForeignKeys parameters);

        Query indexes(PragmaParameters.Indexes parameters);
    }

    interface RawQuery extends BaseConverter<ContentParameters, Query> {
    }

  /*  interface BlobPreview extends BaseConverter<SettingsParameters.BlobPreview, SettingsEntity.BlobPreviewMode> {
    }

    interface Truncate extends BaseConverter<SettingsParameters.Truncate, SettingsEntity.TruncateMode> {
    }*/

    interface Settings {
        SettingsTask get(SettingsParameters.Get parameters);

        SettingsTask linesLimit(SettingsParameters.LinesLimit parameters);

        SettingsTask linesCount(SettingsParameters.LinesCount parameters);

        SettingsTask truncateMode(SettingsParameters.Truncate parameters);

        SettingsTask blobPreviewMode(SettingsParameters.BlobPreview parameters);

        SettingsTask ignoredTableName(SettingsParameters.IgnoredTableName parameters);
    }

}
