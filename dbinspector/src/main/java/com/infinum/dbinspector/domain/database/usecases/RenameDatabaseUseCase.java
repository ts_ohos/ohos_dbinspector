package com.infinum.dbinspector.domain.database.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;

import java.util.List;

public class RenameDatabaseUseCase implements UseCases.RenameDatabase {

    private Repositories.Database  databaseRepository;

    public RenameDatabaseUseCase(Repositories.Database databaseRepository) {
        this.databaseRepository = databaseRepository;
    }

    @Override
    public List<DatabaseDescriptor> invoke(DatabaseParameters.Rename input) {
        return databaseRepository.rename(input);
    }
}
