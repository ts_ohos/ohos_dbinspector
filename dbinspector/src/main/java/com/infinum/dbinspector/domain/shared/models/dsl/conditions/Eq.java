package com.infinum.dbinspector.domain.shared.models.dsl.conditions;

import com.infinum.dbinspector.domain.shared.models.dsl.shared.Condition;

public class Eq extends Condition {
    private String column;
    private Object value;


    {
        if (value != null && !(value instanceof Number) && !(value instanceof String)) {
            throw new IllegalArgumentException("Only null, number and string values can be used in the 'WHERE' clause");
        }
    }

    public Eq(String column) {
        this.column = column;
    }

    public Eq(String column, Object value) {
        this.column = column;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    protected void addCondition(Condition condition) {
        throw new IllegalStateException("Can't add a nested condition to 'eq'");
    }

    @Override
    public String toString() {
        if (value == null){
            return column+"is NULL";
        }else if (value instanceof String){
           return column+"='"+value+'\'';
        }else {
            return column+"="+value;
        }
    }
}
