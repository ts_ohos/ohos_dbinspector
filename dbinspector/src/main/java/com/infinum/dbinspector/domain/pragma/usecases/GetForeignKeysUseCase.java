package com.infinum.dbinspector.domain.pragma.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;

public class GetForeignKeysUseCase implements UseCases.GetForeignKeys {
    private Repositories.Connection connectionRepository;
    private Repositories.Pragma pragmaRepository;

    public GetForeignKeysUseCase(Repositories.Connection connectionRepository, Repositories.Pragma pragmaRepository) {
        this.connectionRepository = connectionRepository;
        this.pragmaRepository = pragmaRepository;
    }

    public Repositories.Connection getConnectionRepository() {
        return connectionRepository;
    }

    public void setConnectionRepository(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    public Repositories.Pragma getPragmaRepository() {
        return pragmaRepository;
    }

    public void setPragmaRepository(Repositories.Pragma pragmaRepository) {
        this.pragmaRepository = pragmaRepository;
    }

    @Override
    public Page invoke(PragmaParameters.ForeignKeys input) throws CloneNotSupportedException {
        DatabaseConnection connection = connectionRepository.open(new ConnectionParameters(input.getDatabasePath()));
        PragmaParameters.ForeignKeys input1 = (PragmaParameters.ForeignKeys) input.clone();
        input1.setDatabase(connection.getDatabase());
        return pragmaRepository.getForeignKeys(input1);
    }
}
