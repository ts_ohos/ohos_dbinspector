package com.infinum.dbinspector.domain.pragma.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;

public class GetIndexesUseCase implements UseCases.GetIndexes {

    private Repositories.Connection connectionRepository;
    private Repositories.Pragma pragmaRepository;

    public GetIndexesUseCase(Repositories.Connection connectionRepository, Repositories.Pragma pragmaRepository) {
        this.connectionRepository = connectionRepository;
        this.pragmaRepository = pragmaRepository;
    }

    public Repositories.Connection getConnectionRepository() {
        return connectionRepository;
    }

    public void setConnectionRepository(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    public Repositories.Pragma getPragmaRepository() {
        return pragmaRepository;
    }

    public void setPragmaRepository(Repositories.Pragma pragmaRepository) {
        this.pragmaRepository = pragmaRepository;
    }

    @Override
    public Page invoke(PragmaParameters.Indexes input) throws CloneNotSupportedException {
        DatabaseConnection connection = connectionRepository.open(new ConnectionParameters(input.getDatabasePath()));
        PragmaParameters.Indexes input1 = (PragmaParameters.Indexes) input.clone();
        input1.setDatabase(connection.getDatabase());
        return pragmaRepository.getIndexes(input1);
    }
}
