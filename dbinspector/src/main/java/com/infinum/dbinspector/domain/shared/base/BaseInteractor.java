package com.infinum.dbinspector.domain.shared.base;

public interface BaseInteractor<InputModel,OutputModel> {
    OutputModel invoke(InputModel input);
}
