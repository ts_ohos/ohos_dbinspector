package com.infinum.dbinspector.domain.shared.base;

public interface BaseRepository<InputModel, OutputModel> {
    OutputModel getPage(InputModel input);
}
