package com.infinum.dbinspector.domain.connection.models;

import ohos.data.rdb.RdbStore;

public class DatabaseConnection {
    private RdbStore database;

    public DatabaseConnection(RdbStore database) {
        this.database = database;
    }

    public RdbStore getDatabase() {
        return database;
    }

    public void setDatabase(RdbStore database) {
        this.database = database;
    }
}
