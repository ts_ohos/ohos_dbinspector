package com.infinum.dbinspector.domain.pragma.models;

public enum TableInfoColumns {
    CID,
    NAME,
    TYPE,
    NOTNULL,
    DFLT,
    PK;
}
