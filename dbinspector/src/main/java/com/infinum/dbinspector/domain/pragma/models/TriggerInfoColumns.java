package com.infinum.dbinspector.domain.pragma.models;

public enum TriggerInfoColumns {
    NAME,
    SQL;
}
