package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.data.model.local.cursor.output.Field;
import com.infinum.dbinspector.data.model.local.cursor.output.QueryResult;
import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.settings.models.Settings;
import com.infinum.dbinspector.domain.shared.base.BaseMapper;
import com.infinum.dbinspector.domain.shared.models.Cell;
import com.infinum.dbinspector.domain.shared.models.Page;
import ohos.data.rdb.RdbStore;

import java.io.File;

public class Mappers {
    public interface Connection extends BaseMapper<RdbStore, DatabaseConnection> {
    }

    public interface DatabaseDescriptorModel extends BaseMapper<File, DatabaseDescriptor> {
    }

    public interface CellModel extends BaseMapper<Field, Cell> {
    }

    public interface Schema extends BaseMapper<QueryResult, Page> {
    }

    public interface Pragma extends BaseMapper<QueryResult, Page> {

       /* ((String) -> CellModel) transformToHeader();
        ((Field) -> CellModel) transformToCell();*/
    }

    public interface RawQuery extends BaseMapper<QueryResult, Page> {
    }
/*
    public interface TruncateMode extends BaseMapper<SettingsEntity.TruncateMode, TruncateMode> {
    }

    public interface BlobPreviewMode extends BaseMapper<SettingsEntity.BlobPreviewMode, BlobPreviewMode> {
    }

    public interface SettingsModel extends BaseMapper<SettingsEntity, Settings>{}*/

}
