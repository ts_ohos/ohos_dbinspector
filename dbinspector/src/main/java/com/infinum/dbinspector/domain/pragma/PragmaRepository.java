package com.infinum.dbinspector.domain.pragma;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;

public class PragmaRepository implements Repositories.Pragma {
    @Override
    public Page getUserVersion(PragmaParameters.Version input) {
        return null;
    }

    @Override
    public Page getTableInfo(PragmaParameters.Info input) {
        return null;
    }

    @Override
    public Page getTriggerInfo(Void input) {
        return null;
    }

    @Override
    public Page getForeignKeys(PragmaParameters.ForeignKeys input) {
        return null;
    }

    @Override
    public Page getIndexes(PragmaParameters.Indexes input) {
        return null;
    }
}
