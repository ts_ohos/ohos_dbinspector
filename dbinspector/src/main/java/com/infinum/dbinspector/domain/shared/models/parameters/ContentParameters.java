package com.infinum.dbinspector.domain.shared.models.parameters;

import com.infinum.dbinspector.domain.Domain;
import com.infinum.dbinspector.domain.shared.models.Sort;
import ohos.data.rdb.RdbStore;

public class ContentParameters {
    private String databasePath = "";
    private RdbStore database;
    private String statement;
    private SortParameters sort = new SortParameters(Sort.ASCENDING);
    private int pageSize = Domain.Constants.Limits.PAGE_SIZE;
    private int page = Domain.Constants.Limits.INITIAL_PAGE;

    public ContentParameters(RdbStore database, String statement) {
        this.database = database;
        this.statement = statement;
    }

    public ContentParameters(String databasePath, RdbStore database, String statement, SortParameters sort, int pageSize, int page) {
        this.databasePath = databasePath;
        this.database = database;
        this.statement = statement;
        this.sort = sort;
        this.pageSize = pageSize;
        this.page = page;
    }

    public String getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath = databasePath;
    }

    public RdbStore getDatabase() {
        return database;
    }

    public void setDatabase(RdbStore database) {
        this.database = database;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public SortParameters getSort() {
        return sort;
    }

    public void setSort(SortParameters sort) {
        this.sort = sort;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
