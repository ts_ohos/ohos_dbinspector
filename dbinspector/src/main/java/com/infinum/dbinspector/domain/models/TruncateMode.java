package com.infinum.dbinspector.domain.models;

public enum  TruncateMode {
    START,
    MIDDLE,
    END
}
