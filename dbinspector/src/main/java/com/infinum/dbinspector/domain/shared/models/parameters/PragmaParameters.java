package com.infinum.dbinspector.domain.shared.models.parameters;

import com.infinum.dbinspector.domain.Domain;
import com.infinum.dbinspector.domain.shared.base.BaseParameters;
import ohos.data.rdb.RdbStore;

public class PragmaParameters implements BaseParameters    {

    public class Version extends PragmaParameters {
        private String databasePath;
        private RdbStore database = null;
        private String statement;

        public Version(String databasePath, String statement) {
            this.databasePath = databasePath;
            this.statement = statement;
        }

        public Version(String databasePath, RdbStore database, String statement) {
            this.databasePath = databasePath;
            this.database = database;
            this.statement = statement;
        }

        public String getDatabasePath() {
            return databasePath;
        }

        public void setDatabasePath(String databasePath) {
            this.databasePath = databasePath;
        }

        public RdbStore getDatabase() {
            return database;
        }

        public void setDatabase(RdbStore database) {
            this.database = database;
        }

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }
    }

    public class Info extends PragmaParameters implements Cloneable {
        private String databasePath;
        private RdbStore database = null;
        private String statement;
        private SortParameters sort = new SortParameters();
        private int page = Domain.Constants.Limits.INITIAL_PAGE;
        private int pageSize = Domain.Constants.Limits.PAGE_SIZE;

        public Info(String databasePath, RdbStore database, String statement, SortParameters sort, int page, int pageSize) {
            this.databasePath = databasePath;
            this.database = database;
            this.statement = statement;
            this.sort = sort;
            this.page = page;
            this.pageSize = pageSize;
        }

        public Info(String databasePath, String statement) {
            this.databasePath = databasePath;
            this.statement = statement;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public String getDatabasePath() {
            return databasePath;
        }

        public void setDatabasePath(String databasePath) {
            this.databasePath = databasePath;
        }

        public RdbStore getDatabase() {
            return database;
        }

        public void setDatabase(RdbStore database) {
            this.database = database;
        }

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }

        public SortParameters getSort() {
            return sort;
        }

        public void setSort(SortParameters sort) {
            this.sort = sort;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }
    }

    public class ForeignKeys extends PragmaParameters implements Cloneable {
        private String databasePath;
        private RdbStore database = null;
        private String statement;
        private SortParameters sort = new SortParameters();
        private int page = Domain.Constants.Limits.INITIAL_PAGE;
        private int pageSize = Domain.Constants.Limits.PAGE_SIZE;

        public ForeignKeys(RdbStore database) {
            this.database = database;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public ForeignKeys(String databasePath, RdbStore database, String statement, SortParameters sort, int page, int pageSize) {
            this.databasePath = databasePath;
            this.database = database;
            this.statement = statement;
            this.sort = sort;
            this.page = page;
            this.pageSize = pageSize;
        }

        public ForeignKeys(String databasePath, String statement) {
            this.databasePath = databasePath;
            this.statement = statement;
        }

        public String getDatabasePath() {
            return databasePath;
        }

        public void setDatabasePath(String databasePath) {
            this.databasePath = databasePath;
        }

        public RdbStore getDatabase() {
            return database;
        }

        public void setDatabase(RdbStore database) {
            this.database = database;
        }

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }

        public SortParameters getSort() {
            return sort;
        }

        public void setSort(SortParameters sort) {
            this.sort = sort;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }
    }

    public class Indexes extends PragmaParameters implements Cloneable {
        private String databasePath;
        private RdbStore database = null;
        private String statement;
        private SortParameters sort = new SortParameters();
        private int page = Domain.Constants.Limits.INITIAL_PAGE;
        private int pageSize = Domain.Constants.Limits.PAGE_SIZE;

        public Indexes(String databasePath, RdbStore database, String statement, SortParameters sort, int page, int pageSize) {
            this.databasePath = databasePath;
            this.database = database;
            this.statement = statement;
            this.sort = sort;
            this.page = page;
            this.pageSize = pageSize;
        }

        public Indexes(String databasePath, String statement) {
            this.databasePath = databasePath;
            this.statement = statement;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public String getDatabasePath() {
            return databasePath;
        }

        public void setDatabasePath(String databasePath) {
            this.databasePath = databasePath;
        }

        public RdbStore getDatabase() {
            return database;
        }

        public void setDatabase(RdbStore database) {
            this.database = database;
        }

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }

        public SortParameters getSort() {
            return sort;
        }

        public void setSort(SortParameters sort) {
            this.sort = sort;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }
    }
}
