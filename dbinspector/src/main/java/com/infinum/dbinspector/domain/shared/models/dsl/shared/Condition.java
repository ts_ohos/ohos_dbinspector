package com.infinum.dbinspector.domain.shared.models.dsl.shared;

import com.infinum.dbinspector.domain.shared.models.dsl.conditions.And;
import com.infinum.dbinspector.domain.shared.models.dsl.conditions.Eq;
import com.infinum.dbinspector.domain.shared.models.dsl.conditions.Like;
import com.infinum.dbinspector.domain.shared.models.dsl.conditions.Or;

import java.util.function.Function;

public abstract class Condition {

    protected abstract void addCondition(Condition condition);

    public final void and(Void T) {
        addCondition(new And(T));
    }

    public final void or(Void T) {
        addCondition(new Or(T));
    }

    public final void eq(String T, Object value) {
        addCondition(new Eq(T, value));
    }

    public final void like(String T, Object value) {
        addCondition(new Like(T, value));
    }

}
