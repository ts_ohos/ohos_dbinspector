package com.infinum.dbinspector.domain.database.interactors;

import com.infinum.dbinspector.data.Sources;
import com.infinum.dbinspector.domain.Interactors;
import com.infinum.dbinspector.domain.database.models.Operation;

import java.io.File;
import java.util.List;

public class CopyDatabaseInteractor implements Interactors.CopyDatabase {
    private  Sources.Raw source;

    public CopyDatabaseInteractor(Sources.Raw source) {
        this.source = source;
    }

    @Override
    public List<File> invoke(Operation input) {
        return source.copyDatabase(input);
    }
}
