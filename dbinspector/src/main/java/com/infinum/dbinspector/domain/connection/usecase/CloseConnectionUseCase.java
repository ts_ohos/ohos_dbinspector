package com.infinum.dbinspector.domain.connection.usecase;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;

public class CloseConnectionUseCase implements UseCases.CloseConnection {

    private Repositories.Connection connectionRepository;

    public CloseConnectionUseCase(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    public Repositories.Connection getConnectionRepository() {
        return connectionRepository;
    }

    public void setConnectionRepository(Repositories.Connection connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    @Override
    public Void invoke(ConnectionParameters input) {
        return connectionRepository.close(input);
    }
}
