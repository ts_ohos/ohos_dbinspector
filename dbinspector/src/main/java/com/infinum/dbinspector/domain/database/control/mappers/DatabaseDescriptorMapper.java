package com.infinum.dbinspector.domain.database.control.mappers;

import com.infinum.dbinspector.domain.Mappers;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;

import java.io.File;

public class DatabaseDescriptorMapper implements Mappers.DatabaseDescriptorModel {

    @Override
    public DatabaseDescriptor invoke(File model) {
        return new DatabaseDescriptor(
                model.exists(),
                model.getParentFile().getPath(),
                model.getName().substring(0,model.getName().lastIndexOf(".")),
                model.getName().substring(model.getName().lastIndexOf("."))
                );
    }
}
