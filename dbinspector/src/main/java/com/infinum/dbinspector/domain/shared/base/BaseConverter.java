package com.infinum.dbinspector.domain.shared.base;

public interface BaseConverter<Input, Output> {
    Output invoke(Input parameters) ;
}
