package com.infinum.dbinspector.domain.database.usecases;

import com.infinum.dbinspector.domain.Repositories;
import com.infinum.dbinspector.domain.UseCases;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;

import java.util.List;

public class RemoveDatabaseUseCase implements UseCases.RemoveDatabase {

    private Repositories.Database  databaseRepository;

    public RemoveDatabaseUseCase(Repositories.Database databaseRepository) {
        this.databaseRepository = databaseRepository;
    }

    @Override
    public List<DatabaseDescriptor> invoke(DatabaseParameters.Command input) {
        return databaseRepository.remove(input);
    }
}
