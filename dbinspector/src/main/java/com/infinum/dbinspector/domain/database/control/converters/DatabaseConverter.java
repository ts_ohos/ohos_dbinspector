package com.infinum.dbinspector.domain.database.control.converters;

import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.database.models.Operation;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;

public class DatabaseConverter implements Converters.Database {
    @Override
    public Operation get(DatabaseParameters.Get parameters) {
        return new Operation(parameters.getContext(), parameters.getArgument());
    }

    @Override
    public Operation import1(DatabaseParameters.Import parameters) {
        return new Operation(parameters.getContext(), parameters.getImportUris());
    }

    @Override
    public Operation rename(DatabaseParameters.Rename parameters) {
        return new Operation(parameters.getContext(), parameters.getDatabaseDescriptor(), parameters.getArgument());
    }

    @Override
    public Operation command(DatabaseParameters.Command parameters) {
        return new Operation(parameters.getContext(), parameters.getDatabaseDescriptor());
    }

    @Override
    public Operation copy(DatabaseParameters.Copy parameters) {
        return new Operation(parameters.getContext(),parameters.getDatabaseDescriptor());
    }
}
