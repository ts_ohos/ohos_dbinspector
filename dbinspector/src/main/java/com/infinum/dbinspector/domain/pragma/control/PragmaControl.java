package com.infinum.dbinspector.domain.pragma.control;

import com.infinum.dbinspector.domain.Control;
import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.Mappers;

import java.io.File;

public class PragmaControl implements Control.Pragma {

    private Mappers.Pragma mapper;
    private Converters.Pragma converter;

    public PragmaControl(Mappers.Pragma mapper, Converters.Pragma converter) {
        this.mapper = mapper;
        this.converter = converter;
    }

    @Override
    public Mappers.Pragma getMapper() {
        return null;
    }

    @Override
    public Converters.Pragma getConverter() {
        return converter;
    }


}
