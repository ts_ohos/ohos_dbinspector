package com.infinum.dbinspector.domain.pragma.control.converters;

import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;
import ohos.data.distributed.common.Query;

public class PragmaConverter implements Converters.Pragma {
    private Converters.Sort sortConverter;
    @Override
    public Query version(PragmaParameters.Version parameters) {
        return null;
    }

    @Override
    public Query info(PragmaParameters.Info parameters) {
        return null;
    }

    @Override
    public Query foreignKeys(PragmaParameters.ForeignKeys parameters) {
        return null;
    }

    @Override
    public Query indexes(PragmaParameters.Indexes parameters) {
        return null;
    }
}
