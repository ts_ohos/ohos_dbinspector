package com.infinum.dbinspector.domain.connection.control;

import com.infinum.dbinspector.domain.Control;
import com.infinum.dbinspector.domain.Converters;
import com.infinum.dbinspector.domain.Mappers;

import java.io.File;

public class ConnectionControl implements Control.Connection {
    private Mappers.Connection mapper;
    private Converters.Connection converter;

    public ConnectionControl(Mappers.Connection mapper, Converters.Connection converter) {
        this.mapper = mapper;
        this.converter = converter;
    }

    public Mappers.Connection getMapper() {
        return mapper;
    }

    @Override
    public Converters.Connection getConverter() {
        return converter;
    }

    public void setMapper(Mappers.Connection mapper) {
        this.mapper = mapper;
    }

}
