package com.infinum.dbinspector.domain;

import com.infinum.dbinspector.domain.connection.models.DatabaseConnection;
import com.infinum.dbinspector.domain.database.models.DatabaseDescriptor;
import com.infinum.dbinspector.domain.database.models.Operation;
import com.infinum.dbinspector.domain.models.parameters.ConnectionParameters;
import com.infinum.dbinspector.domain.models.parameters.DatabaseParameters;
import com.infinum.dbinspector.domain.models.parameters.SettingsParameters;
import com.infinum.dbinspector.domain.shared.base.BaseRepository;
import com.infinum.dbinspector.domain.shared.models.Page;
import com.infinum.dbinspector.domain.shared.models.parameters.ContentParameters;
import com.infinum.dbinspector.domain.shared.models.parameters.PragmaParameters;
import com.infinum.dbinspector.domain.settings.models.Settings ;
import ohos.agp.render.Paint;

import java.util.List;

public interface Repositories {
    interface Database extends BaseRepository<DatabaseParameters.Get, List<DatabaseDescriptor>> {

        List<DatabaseDescriptor> import1(DatabaseParameters.Import input);

        List<DatabaseDescriptor> rename(DatabaseParameters.Rename input);

        List<DatabaseDescriptor> remove(DatabaseParameters.Command input);

        List<DatabaseDescriptor> copy(DatabaseParameters.Copy input);
    }

    interface Connection {
        DatabaseConnection open(ConnectionParameters input);

        Void close(ConnectionParameters input);
    }

    interface Settings extends BaseRepository<SettingsParameters.Get, Settings> {
        Void saveLinesLimit(SettingsParameters.LinesLimit input);

        Void saveLinesCount(SettingsParameters.LinesCount input);

        Void saveTruncateMode(SettingsParameters.Truncate input);

        Void saveBlobPreview(SettingsParameters.BlobPreview input);

        Void saveIgnoredTableName(SettingsParameters.IgnoredTableName input);

        Void removeIgnoredTableName(SettingsParameters.IgnoredTableName input);
    }

    interface Schema extends BaseRepository<ContentParameters, Page> {
        Page getByName(ContentParameters input);

        Page dropByName(ContentParameters input);
    }

    interface Pragma {
        Page getUserVersion(PragmaParameters.Version input);

        Page getTableInfo(PragmaParameters.Info input);

        Page getTriggerInfo(Void input);

        Page getForeignKeys(PragmaParameters.ForeignKeys input);

        Page getIndexes(PragmaParameters.Indexes input);
    }

    interface RawQuery extends BaseRepository<ContentParameters, Page> {
        Page getHeaders(ContentParameters input);

        Page getAffectedRows(ContentParameters input);
    }

}
