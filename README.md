# android_dbinspector  
本项目是基于开源项目dbinspector进行harmonyos化的移植和开发的，可以通过项目标签以及点击[android_dbinspector](https://github.com/infinum/android_dbinspector) 追踪到原项目版本

移植版本：5.3.5

#### 介绍  
主要功能：展示手机内数据库文件提供列表展示，针对数据库进行复制，重命名，删除操作。  
点击数据库显示出数据表，可以进行搜索操作，和执行sql语句查询操作，能够显示出数据表的所有数值，数据表删除操作，数据表结构信息查询。  
针对数据库的提供设置方法，能够进行数据表的过滤操作等。
#### 安装教程

方式一：

通过library生成har包,添加har包到集成的libs文件夹内
在entry的gradle内添加如下代码
```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```
方式二:
Mavn库 下载使用
```
buildscript {
    repositories {
        ...
        mavenCentral()
    }
 allprojects {
     repositories {
         ...
         mavenCentral()
     }
 }
```
```
  implementation 'com.gitee.ts_ohos:ohos_dbinspector_dbinspector:1.0.0'
```
#### 使用说明  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/162041_58416c1a_8562620.png "屏幕截图.png")  
对外提供一个跳转接口，用户需要调用跳转方法，跳转到数据库展示页面,首先需要把数据库文件放到rawfile资源文件夹，  
调用AssetsDatabaseProvider.names()方法存放数据库名称，最后调用AssetsDatabaseProvider.copy()方法，把资源文件存放到  
data/data/包名/files//databases/db目录下。数据库展示出来就是点击进行操作。

```
 assetsDatabaseProvider = new AssetsDatabaseProvider(this);
 assetsDatabaseProvider.names(new String[]{"blog.db","chinook.db","northwind.sqlite"});

  Run.setClickedListener(new Component.ClickedListener() {
             @Override
             public void onClick(Component component) {
                 assetsDatabaseProvider.copy();
                 DbInspector.getInstance().show();
             }
         });
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/162002_a8f0b7a3_8562620.png "屏幕截图.png")  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/195459_dcc816c0_8562620.png "屏幕截图.png")  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/195450_c54eb8ab_8562620.png "屏幕截图.png")  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/195751_293077bb_8562620.png "屏幕截图.png")  
#### 版本迭代
v1.0.0 基于原项目最新版本，初次提交。
#### 项目源文件许可证  

Copyright 2020 Infinum

Licensed under the Apache License, Version 2.0 (the "License");

you may not use this file except in compliance with the License.

You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software

distributed under the License is distributed on an "AS IS" BASIS,

WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and

limitations under the License.
