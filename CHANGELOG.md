# dbinspector
## 项目移植说明
-----

#### 已实现功能：
- 数据库显示页面
- 数据库搜索功能
- 设置功能
- 数据库的删除、复制、重命名操作功能实现
- 点击数据库显示出所有的数据表
- 数据表名进行搜索
- 数据表页面的sql语句输入查询结果页面
- 删除数据表数据完成
- 获取数据表信息完成

#### 部分功能差异
- 悬浮按钮实现，跳转到系统文件夹失败(鸿蒙不支持跳转)
- 数据库表的分享功能未实现，鸿蒙不支持