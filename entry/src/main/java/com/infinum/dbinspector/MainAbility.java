package com.infinum.dbinspector;

import com.infinum.dbinspector.slice.MainAbilitySlice;
import com.infinum.dbinspector.ui.Presentation;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        Presentation.getInstance().setContext(this);
    }
}
