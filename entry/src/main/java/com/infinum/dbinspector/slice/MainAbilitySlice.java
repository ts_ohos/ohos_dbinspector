/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infinum.dbinspector.slice;

import com.infinum.dbinspector.DbInspector;
import com.infinum.dbinspector.ResourceTable;
import com.infinum.dbinspector.data.RawfileDatabaseProvider;
import com.infinum.dbinspector.ui.shared.base.BaseActivity;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbilitySlice extends BaseActivity {

    //Run 按钮
    private Button Run;

    private RawfileDatabaseProvider rawfileDatabaseProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        rawfileDatabaseProvider = new RawfileDatabaseProvider(this);
        rawfileDatabaseProvider.names(new String[]{"blog.db", "chinook.db", "northwind.sqlite"});
    }

    @Override
    public void setLayoutId() {
        layoutId = ResourceTable.Layout_ability_main;
    }

    @Override
    public void initView() {
        Run = (Button) findComponentById(ResourceTable.Id_show);
    }

    @Override
    public void clickLinstener() {
        Run.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                rawfileDatabaseProvider.copy();
                DbInspector.getInstance().show();
            }
        });

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
