package com.infinum.dbinspector.slice;

import com.infinum.dbinspector.data.DatabaseProvider;

public class MainViewModel {
    private DatabaseProvider databaseProvider;

    public MainViewModel(DatabaseProvider databaseProvider) {
        this.databaseProvider = databaseProvider;
    }

    public void copy(){
        databaseProvider.copy();
    }
}
